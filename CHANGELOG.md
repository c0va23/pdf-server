# 0.12.5
- Disable parcel cache in earthly. Fix rebuilding of tester in docker image.
- Split compositions and templates in menu
- Add templates search to menu
- Reduce tester bundle size (by update parcel)
- Update watchexec to latest version
- Fix default chromium arguments to support newest version

# 0.12.4
- Fix example preview in tester. Disable sandbox on iframe.

# 0.12.3
- Fix preview in tester. Disable sandbox on iframe.

# 0.12.2
- Revert build image optimizations. No functional changes.

# 0.12.1
- Configure parent based sampling
- Add trace sampling, configurable by `TRACING_SAMPLE_RATE` envvar
- Skip status endpoint tracing by default. To enable status tracing, set
  `TRACING_KEEP_STATUS_TRACE` envvar to `true`
- Remove skipping for tracing http and router spans.
  Full skip status endpoint trace replace this behavior
- Update all packages to actual versions

# 0.12.0
- Update go to 1.22
- Update Alpine to 3.19
- Update all packages to actual versions
- Fix start chromium on Alpine 3.19

# 0.11.5
- Update earthly to 0.8.7
- Add remote-http templates. See remote-http example to explain.

# 0.11.4
- Add retry attempts to render template
- Add timeouts to borrow/repay renderer from/to pool
- Invalidate renderer on error

# 0.11.3
- Add preview render plan tab to template tester
- Add endpoint `POST /templates/:template_name/preview/render-plan` to debug composition plan.

# 0.11.2
- Add value type of condition evolution result in yaml composition.

# 0.11.1
- Composition merge PDF parallel to rendering.

# 0.11.0-beta.2
- Document configuration.
- Allow filter templates and examples by name into validate command.
- Remove targetId from CDT span (it was very frequent)
- JS composition write logs to common logger

# 0.11.0-beta.1
- Add JavaScript composition plan builders. See `examples/compositions/js-example/composition.js` to explain.
- **INTERNAL** Enable CI on refactor/* branches
- **INTERNAL** refactor YAML composition loader. Allow new syntax for compositions.

# 0.10.0-beta.12
- **INTERNAL** refactor store loaders
- Allow recursive templates directory with `--templates-recursive` flag (env `TEMPLATES_RECURSIVE`).
- **INTERNAL** Optimize rendering composite templates with one template
- **INTERNAL** configure CI caching via earthly satellite

# 0.10.0-beta.11
- Add `templates[].transform.jq` to `composition.yaml`. See `examples/compositions/only_static/composition.yaml` to explain.
- **INTERNAL** Update to node20
- **INTERNAL** Update parcel to 2.10.0
- **INTERNAL** Update earthly to 0.7.20
- **INTERNAL** Update golang to 1.21
- **INTERNAL** Configure depguard

# 0.10.0-beta.10
- Allow trigger PDF printing via callback from template page to internal server (see `callback` example)
- Add `embed_text` and `embed_base64` helpers to go templates and handlebars (see `embed_assets` example)
- Fix "Not Found" errors into internal server logs
- **INTERNAL** fast base64 decoding of PDF payload returned from browser

# 0.10.0-beta.9
- **INTERNAL** Update node 18.x and npm to 9.x.
- **INTERNAL** Remove CDP pool span
- Fix CDP pool config bindings

# 0.10.0-beta.8
- Fix caching of assets
- Composite template tracing
- Detailed tracing of html2pdf template rendering
- Configure tracing propagators via OTEL_PROPAGATORS envvar.
- Passive status request log to debug

# 0.10.0-beta.7
- Templates in composition rendered in parallel

# 0.10.0-beta.6
- `#equal` handlebars helper support else branch

# 0.10.0-beta.5
- Set `Content-Length` headers

# 0.10.0-beta.4
- Add condition to composite templates

# 0.10.0-beta.3
- Add raw template (read and return PDF file directly)
- Add helper `markdown`

# 0.10.0-beta.2
- Fix examples and schema on composite template

# 0.10.0-beta.1
- Add composition templates. See `--composition-templates` flag.

# 0.9.2
- Support sub-dir into partials
- Add "Refresh" button to example
- Expand current template menu item pn page reloading
- Independent scroll on render panels

# 0.9.1
- Add `format_time` helper

# 0.9.0
- Support waitLifecycleEvent configuration via template params.

# 0.9.0-beta.2
- Support handlebars template engine, with support partials.
  Partials can be defined in `partials/` dir.
- **INTERNAL** skip test if browser not found
- Backend return internal error
- Add copyable curl command to tester
- Keep only one log per loaded template
- Capture all browser logs
- Downgrade to alpine 3.16
- Reload examples and schema
- Add settings to app bar
- Allow resize inputs and preview panel
- Allow hide inputs and preview panel
- Move examples selector to bottom of page
- Clean selected example when template is switched
- Allow build pre-release (alpha, beta, rc and etc) versions

# 0.9.0-beta.1
- Support partials for golang templates. Partials can be defined in `partials/` dir.
  Markdown partials can be defined with `.md` extension.
- Change partial extension from .md.mustache to .mustache.md
- Mustache template read partials from `partials/` dir instead of `includes/`
- **INTERNAL** Fix all browser warnings on tester
- **INTERNAL** Update tester to react 18
- Add example selector to input panel in tester
- Add reloading templates by passing `--templates-reload-mode` flags.
  Supported values:
  - `none` - legacy behavior. Templates not reload.
  - `always` - templates reload on each request.
- Allow run http server listener with address reuse option (`--server-reuse-addr` flag).
- Render partials with extension '.md.mustache' as markdown
- Cache mustache partials.
  Now partials load only once on startup, and not reload on each request.
- **BREAKING** load mustache partials into `includes/` dir
- **INTERNAL** Update earthly to 0.6.30
- **INTERNAL** Update go to 1.20

# 0.8.0
- **INTERNAL** Remove grid from status page
- Show version into tester UI
- Print version on server startup
- Show error into status page
- Add default chromium arguments required for headless arm64 version
- Add arm64 image (multi-arch)
- **INTERNAL** Update earthly to 0.6.22
- **BREAKING** Disable building of firefox image
- **INTERNAL** Upgrade golang to 1.18
- **INTERNAL** Update base image to alpine 3.16
- **INTERNAL** Not generate mocks on release

# 0.7.2
- Swap (fix) firefox and chromium images.
- Update pool defaults. Now pdf-server start with one idle browser.
- Add status page to Tester UI.
- Define `GET /_status` into `GET /swagger.json`.
- Log link to debug tools after start.
- Add redirect from root (`/`) to static path.
- Add static dir config and bind flags with envvars (see `server --help`).
- **INTERNAL** Update golangci-lint to 1.48.0 and fix new warnings.
- **INTERNAL** Migrate from golint to revive and fix new warnings.

# 0.7.1
- Re-Enable testOnReturn. Invalidate broken render objects.
- Fix "Create page" logging level
- Add lorem ipsum example
- Not start with template without body

# 0.7.0
- Not validate render before return to pool.
- Fix `--version` into images.
- **BRAKING** Fix typo into argument `--render-pool-time-between-eviction`
    (ENV: `RENDER_POOL_TIME_BETWEEN_EVICTION`)
- Add tracing. See `--tracing-*` and `--otel-collector-*` flags.
- Add render tester browser app (`GET /static/tester/`).
- Add swagger API doc page `/static/swagger.html`.
- Add simple `GET /spec/swagger.json` spec endpoint.
  Known issues:
  - not supported json schemas with `$ref` and `$defs` keywords.
- **BRAKING**: Add suffix `-chromium` to chromium based image tags. New tag examples:
  - `latest` -> `latest-chromium`.
  - `v0.7.0` -> `v0.7.0-chromium`.
- Provide firefox based images (image tag have suffix `-firefox`).
  Firefox tag examples:
  - `latest-firefox`.
  - `v0.7.0-firefox`.
- Migrate image build to earthly.
- **BRAKING**: Remove ONBUILD from produced image.
  Now user templates should be add to image explicitly.
  Now minimal Dockerfile look like:
    ```Dockerfile
    FROM registry.gitlab.com/c0va23/pdf-server:latest-chromium

    ADD templates templates
    ```
- Add render timeout, configurable by `render-timeout` argument or
  by `RENDER_TIMEOUT` environment variable.
- Add argument `browser-command` (ENV: `BROWSER_COMMAND`) as new canonical form
  of `chromium-command` (ENV: `CHROMIUM_COMMAND`) argument.
- Add argument `browser-args` (ENV: `BROWSER_ARGS`) as new canonical form of
  `chromium-args` (ENV: `CHROMIUM_ARG`) argument.
- Allow log into JSON format (with `LOG_FORMAT=json` environment variable).
- Status endpoint (`GET /_status`) now get version with CDP client.
  Status return 500, when render pool not ready.
- Add page pool factory. Enabled by `FACTORY_TYPE=page`
- **BRAKING**: Remove `POST /render/:template_name` endpoint
- **BRAKING**: Rename environment variable `CHROMIUM__ARGS` to `CHROMIUM_ARGS`
- Migrate to golang 1.17
- Migrate to alpine 3.15
- Build with Earthly

# 0.6.3
- Log request duration in seconds (float)

# 0.6.2
- Allow configure RPC buffer size via `--factory-rpc-buffer-size`

# 0.6.1
- Fix close page connection
- Up default render-pool-time-between-eviction to 1 hour
- Up default factory-max-usage-count to 1000
- Wrap container process into tini for prevent zombie process
- Add delay before get page attempts. Can be configured via `--factory-get-page-retry-delay`
- Fix CMD into Dockerfile

# 0.6.0
- Fixed graceful shutdown
- Log request_id
- Migrate to golang 1.13
- Fixed race when waiting for page lifecycle event.
- Added argument `--factory-max-usage-count` (`$FACTORY_MAX_USAGE_COUNT`) to configure the maximum number of uses pages from the pool.
- Added argument `--factory-max-get-page-attempts` (`$FACTORY_MAX_GET_PAGE_ATTEMPTS`) to configure the number of attempts to get the default page "about".
- Now pages are always deleted from the pool, not after n seconds of idle time.
- Added argument `--render-pool-time-between-eviction` (`$RENDER_POOL_TIME_BETWEEN_EVICTION`) to configure the frequency of checking the idle pages.
- A page pool creates new browser instances instead of pages in a single browser.
- Expanded the standard list of chromium startup arguments.
- Migrate from github.com/chromedp/chromedp to github.com/mafredri/cdp.
- Add argument `--wait-lifecycle-event`. This argument allows you to specify the
  page event that will be expected before printing to PDF is called.
- Refactor chromerender package.
- Add one year caching for assets.
- Log time with millisecond.

# v0.5.1
- Fix chrome page create
- Fix defaults for render pool

# v0.5.0
- Add versioning

# 2019-08-23
- Add command `validate`. Its command parse all templates and render all
  examples. Command `validate` useful for test templates on CI/CD.

# 2019-08-22
- Add template examples. Examples can added into `examples/` into template
  directory and can by rendered by
  `GET /templates/:template_name/examples/:example_name`.

# 2019-08-21
- Rename render API endpoint to `POST /templates/:template_name/render`
  (from `POST /render/:template_name`)
- Add header and footer templates. Now template directory can contains
  `(header|footer)\.(html|tmpl|mustache)` files. Its templates rendered with
  main template data and replace appropriate values into params JSON.
  Its templates parsed only if params value `pdf.displayHeaderFooter` is equal
  `true`.

# 2019-08-20
- Assets server return `Content-Type` header with value based on asset
  extension.
