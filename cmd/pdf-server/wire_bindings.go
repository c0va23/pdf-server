package main

import (
	"math/rand"

	"github.com/google/wire"
	pool "github.com/jolestar/go-commons-pool/v2"
	"github.com/sirupsen/logrus"

	"gitlab.com/c0va23/pdf-server/app/config"
	"gitlab.com/c0va23/pdf-server/app/log"
	"gitlab.com/c0va23/pdf-server/app/pdfrender"
	"gitlab.com/c0va23/pdf-server/app/pdfrender/cdp"
	pdfRenderServer "gitlab.com/c0va23/pdf-server/app/pdfrender/server"
	"gitlab.com/c0va23/pdf-server/app/server"
	"gitlab.com/c0va23/pdf-server/app/services"
	"gitlab.com/c0va23/pdf-server/app/templates"
	"gitlab.com/c0va23/pdf-server/app/templates/fileloader"
	"gitlab.com/c0va23/pdf-server/app/templates/fileloader/helpers"
	"gitlab.com/c0va23/pdf-server/app/templates/fileloader/remote"
	"gitlab.com/c0va23/pdf-server/app/templates/futures"
	traceUtils "gitlab.com/c0va23/pdf-server/app/utils/trace_utils"
)

//nolint:deadcode,unused,varcheck,gochecknoglobals
var wireSet = wire.NewSet(
	log.NewLoggerFromConfig,

	fileloader.NewMarkdownPartialPreprocessor,
	fileloader.NewOrderedPartialProcessors,

	wire.Bind(new(helpers.MarkdownProcessor), new(*fileloader.MarkdownPartialPreprocessor)),
	helpers.NewMarkdownProcessor,

	wire.Bind(new(fileloader.MarkdownHelpers), new(*helpers.MarkdownHelpers)),

	ProviderScheduler,

	fileloader.NewDirPartialsLoader,
	fileloader.DefaultFileTemplateInstanceLoader,
	fileloader.NewFileSchemaLoader,
	fileloader.NewFileTemplateLoader,
	fileloader.NewFileParamsLoader,
	fileloader.NewFileAssetsLoader,
	fileloader.NewFileExamplesLoader,
	fileloader.NewCompositionLoaderBuilder,
	fileloader.NewRawTemplateLoader,
	remote.NewHTTPTemplateLoader,
	fileloader.DefaultOrderedTemplateLoaders,
	fileloader.DefaultOrderedTemplateLoaderBuilders,
	fileloader.NewStoreLoader,

	fileloader.LoadStore,

	templates.EmptyLazyStore,
	wire.Bind(new(templates.StoreProvider), new(*templates.LazyStore)),
	wire.Bind(new(templates.StoreConsumer), new(*templates.LazyStore)),

	pdfRenderServer.NewMapStore,
	pdfRenderServer.NewHandler,
	pdfRenderServer.ProvideServer,

	cdp.NewCmdRunnerBuilder,
	cdp.ProvideFactory,
	cdp.ProvidePoolBorrower,
	cdp.NewRender,

	services.NewBasePDFRenderService,
	services.NewSimpleValidateService,
	services.NewDirectStatsService,
	services.NewTemplateSpecService,

	server.NewHandlers,
	server.NewEchoRouter,
	server.ProvideHTTPServer,

	wire.FieldsOf(
		new(config.GlobalConfig),
		"RenderPool",
		"CdpRunner",
		"CdpRender",
		"Log",
		"Templates",
		"Factory",
		"Tracing",
	),
	wire.FieldsOf(new(server.HTTPServerConfig), "Static"),

	NewRandSource,

	traceUtils.ProvideTracerProvider,
	traceUtils.ProvideTracePropagator,

	wire.Bind(new(fileloader.HTMLTemplateLoader), new(*fileloader.FileTemplateInstanceLoader)),
	wire.Bind(new(fileloader.PartialsLoader), new(*fileloader.DirPartialsLoader)),
	wire.Bind(new(templates.SchemaLoader), new(*fileloader.FileSchemaLoader)),
	wire.Bind(new(fileloader.ParamsLoader), new(*fileloader.FileParamsLoader)),
	wire.Bind(new(templates.TemplateLoader), new(*fileloader.FileTemplateLoader)),
	wire.Bind(new(fileloader.AssetsLoader), new(*fileloader.FileAssetsLoader)),
	wire.Bind(new(templates.ExamplesLoader), new(*fileloader.FileExamplesLoader)),
	wire.Bind(new(cdp.RunnerBuilder), new(*cdp.CmdRunnerBuilder)),
	wire.Bind(new(cdp.TypedFactory), new(*cdp.Factory)),
	wire.Bind(new(pdfRenderServer.Store), new(*pdfRenderServer.MapStore)),
	wire.Bind(new(pdfRenderServer.Server), new(*pdfRenderServer.EchoServer)),
	wire.Bind(new(cdp.Borrower), new(*cdp.PoolBorrower)),
	wire.Bind(new(pool.PooledObjectFactory), new(*cdp.Factory)),
	wire.Bind(new(pdfrender.PDFRenderer), new(*cdp.Render)),
	wire.Bind(new(log.Logger), new(*logrus.Logger)),
	wire.Bind(new(services.PDFRenderService), new(*services.BasePDFRenderService)),
	wire.Bind(new(services.StatsService), new(*services.DirectStatsService)),
	wire.Bind(new(services.SpecService), new(*services.TemplateSpecService)),
	wire.Bind(new(server.HandlersInterface), new(*server.Handlers)),
	wire.Bind(new(server.Router), new(*server.EchoRouter)),
	wire.Bind(new(services.ValidateService), new(*services.SimpleValidateService)),
)

// NewRandSource return not random source.
func NewRandSource() rand.Source {
	return rand.NewSource(1)
}

// ProviderScheduler for template render.
func ProviderScheduler(
	poolConfig cdp.PoolConfig,
) (futures.Scheduler[templates.RenderResult], func()) {
	return futures.ProvidePoolScheduler[templates.RenderResult](futures.PoolSchedulerConfig{
		Workers: poolConfig.MaxTotal,
	})
}
