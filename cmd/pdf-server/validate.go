package main

import (
	"fmt"

	cliV2 "github.com/urfave/cli/v2"

	"gitlab.com/c0va23/pdf-server/app/config"
	"gitlab.com/c0va23/pdf-server/app/log"
)

// BuildValidateCommand parse templates and render all examples.
func BuildValidateCommand(globalConfig *config.GlobalConfig) *cliV2.Command {
	return &cliV2.Command{
		Name:  "validate",
		Usage: "Render all examples of all templates",
		ArgsUsage: "If arguments are provided, they will be used to filter templates with example. " +
			"Format is <template name>/<example name>. " +
			"Example: `validate template1/example1 template2/example2`",
		Action: func(cliCtx *cliV2.Context) (err error) {
			ctx := cliCtx.Context

			logger := log.LoggerFromContext(ctx, "validate")

			validateService, closer, err := BuildValidateService(ctx, *globalConfig)
			if nil != err {
				return fmt.Errorf("init validate service: %w", err)
			}

			defer closer()

			filter := cliCtx.Args().Slice()

			if err := validateService.Validate(ctx, filter); err != nil {
				return fmt.Errorf("validate: %w", err)
			}

			logger.Info("Validation successful")

			return nil
		},
	}
}
