console.log("load composition")

// `require` can be used to import scripts. Only require/exports syntax is supported.
const staticTasksBuilder = require('./static.js')

/*
`exports` should contains `buildPlan` function.`
`buildPlan` function will be called on render those composition.
`buildPlan` function  should returns array of objects named `render tasks`.
Every `render tasks` object should contains field `templateName`.
Also field `data` may be included to pass data to template.
*/
exports = {
  buildPlan: function(data) {
    console.log("build plan")

    if (typeof data.value !== 'string') {
      console.error("value should be string")
    }

    const tasks = []

    tasks.push(...staticTasksBuilder.buildStaticTasks())

    if (data.iterations?.length > 0) {
      tasks.push({
        templateName: 'lorem-ipsum',
        data: data,
      })
    }

    tasks.push({
      templateName: 'mustache_template',
      data: data.embedded,
    })

    return tasks
  }
}
