console.log("load static")

exports.buildStaticTasks = function() {
  console.warn("execute build static tasks")

  return [
    { templateName: 'static_template' },
    { templateName: 'simple_raw' },
  ]
}
