# PDF server examples

## Build image and start

```bash
earthly +chromium-image --PDF_SERVER_IMAGE=pdf-server:latest-chromium

cd examples

tree
# .
# ├── Dockerfile
# ├── Earthfile
# ├── README.md
# └── templates
#     ├── assets_template
#     │   ├── assets
#     │   │   ├── image.svg
#     │   │   └── styles.css
#     │   └── template.html
#     ├── examples_template
#     │   ├── examples
#     │   │   ├── alice.json
#     │   │   └── bob.json
#     │   └── template.mustache
#     ├── footer_template
#     │   ├── footer.html
#     │   ├── params.json
#     │   └── template.html
#     ├── go_template
#     │   └── template.tmpl
#     ├── mustache_template
#     │   └── template.mustache
#     ├── params_template
#     │   ├── params.json
#     │   └── template.html
#     ├── schema_template
#     │   ├── schema.json
#     │   └── template.tmpl
#     └── static_template
#         └── template.html

docker build --build-arg PDF_SERVER_IMAGE=pdf-server:latest-chromium -t pdf-server-examples:chromium .
# Sending build context to Docker daemon  30.21kB
# Step 1/3 : ARG PDF_SERVER_IMAGE=registry.gitlab.com/c0va23/pdf-server:latest-chromium
# Step 2/3 : FROM ${PDF_SERVER_IMAGE}
#  ---> d30d3157d908
# Step 3/3 : ADD templates templates
#  ---> fc9e5f7ae060
# Successfully built fc9e5f7ae060
# Successfully tagged pdf-server-examples:latest

docker run --rm -it -p 9999:9999 pdf-server-examples:latest
# time="2022-08-07T11:05:17.977+0300" level=info msg="Global config" config="&{info text {2 0 2 1h0m0s} {20 10µs 1000 8096 runner} {org.chromium.Chromium [--headless --hide-scrollbars --mute-audio --no-default-browser-check --no-first-run --no-sandbox --safebrowsing-disable-auto-update --metrics-recording-only --disable-background-networking --disable-background-timer-throttling --disable-breakpad --disable-extensions --disable-file-system --disable-databases --disable-default-apps --disable-dev-shm-usage --disable-gpu --disable-ipc-flooding-protection --disable-sync --disable-logging --disable-notifications --disable-permissions-api --disable-popup-blocking --disable-prompt-on-repost --disable-renderer-backgrounding --disable-translate]} {load 30s} {./examples/templates/} {otel pdf-server {grpc {localhost:4317} {localhost:4318}} 15s false}}" logger=server
# time="2022-08-07T11:05:17.977+0300" level=info msg="Server config" config="{:9999 10s {./app/server/static/ false}}" logger=server
# time="2022-08-07T11:05:17.977+0300" level=info msg="Template instance loaded" logger=templates template_dir=examples/templates/assets_template template_loader=instance template_name=assets_template template_part=template template_path=template.html template_type=static
# time="2022-08-07T11:05:17.978+0300" level=info msg="Assets loaded" logger=templates template_dir=examples/templates/assets_template template_loader=assets template_name=assets_template
# time="2022-08-07T11:05:17.978+0300" level=info msg="Template loaded" logger=templates template_dir=examples/templates/assets_template template_name=assets_template
# time="2022-08-07T11:05:17.978+0300" level=info msg="Template instance loaded" logger=templates template_dir=examples/templates/examples_template template_loader=instance template_name=examples_template template_part=template template_path=template.mustache template_type=mustache
# time="2022-08-07T11:05:17.978+0300" level=info msg="Example loaded" logger=templates template_dir=examples/templates/examples_template template_example=alice template_loader=examples template_name=examples_template
# time="2022-08-07T11:05:17.979+0300" level=info msg="Example loaded" logger=templates template_dir=examples/templates/examples_template template_example=bob template_loader=examples template_name=examples_template
# time="2022-08-07T11:05:17.979+0300" level=info msg="Template loaded" logger=templates template_dir=examples/templates/examples_template template_name=examples_template
# time="2022-08-07T11:05:17.979+0300" level=info msg="Template instance loaded" logger=templates template_dir=examples/templates/footer_template template_loader=instance template_name=footer_template template_part=template template_path=template.html template_type=static
# time="2022-08-07T11:05:17.979+0300" level=info msg="Template instance loaded" logger=templates template_dir=examples/templates/footer_template template_loader=instance template_name=footer_template template_part=footer template_path=footer.html template_type=static
# time="2022-08-07T11:05:17.979+0300" level=info msg="Params loaded" logger=templates template_dir=examples/templates/footer_template template_loader=params template_name=footer_template
# time="2022-08-07T11:05:17.979+0300" level=info msg="Template loaded" logger=templates template_dir=examples/templates/footer_template template_name=footer_template
# time="2022-08-07T11:05:17.980+0300" level=info msg="Template instance loaded" logger=templates template_dir=examples/templates/go_template template_loader=instance template_name=go_template template_part=template template_path=template.tmpl template_type=go_template
# time="2022-08-07T11:05:17.980+0300" level=info msg="Template loaded" logger=templates template_dir=examples/templates/go_template template_name=go_template
# time="2022-08-07T11:05:17.980+0300" level=info msg="Template instance loaded" logger=templates template_dir=examples/templates/lorem-ipsum template_loader=instance template_name=lorem-ipsum template_part=template template_path=template.mustache template_type=mustache
# time="2022-08-07T11:05:17.981+0300" level=info msg="Example loaded" logger=templates template_dir=examples/templates/lorem-ipsum template_example=10 template_loader=examples template_name=lorem-ipsum
# time="2022-08-07T11:05:17.981+0300" level=info msg="Example loaded" logger=templates template_dir=examples/templates/lorem-ipsum template_example=100 template_loader=examples template_name=lorem-ipsum
# time="2022-08-07T11:05:17.982+0300" level=info msg="Example loaded" logger=templates template_dir=examples/templates/lorem-ipsum template_example=1000 template_loader=examples template_name=lorem-ipsum
# time="2022-08-07T11:05:17.982+0300" level=info msg="Template loaded" logger=templates template_dir=examples/templates/lorem-ipsum template_name=lorem-ipsum
# time="2022-08-07T11:05:17.982+0300" level=info msg="Template instance loaded" logger=templates template_dir=examples/templates/mustache_template template_loader=instance template_name=mustache_template template_part=template template_path=template.mustache template_type=mustache
# time="2022-08-07T11:05:17.983+0300" level=info msg="Template loaded" logger=templates template_dir=examples/templates/mustache_template template_name=mustache_template
# time="2022-08-07T11:05:17.983+0300" level=info msg="Template instance loaded" logger=templates template_dir=examples/templates/params_template template_loader=instance template_name=params_template template_part=template template_path=template.html template_type=static
# time="2022-08-07T11:05:17.983+0300" level=info msg="Params loaded" logger=templates template_dir=examples/templates/params_template template_loader=params template_name=params_template
# time="2022-08-07T11:05:17.983+0300" level=info msg="Template loaded" logger=templates template_dir=examples/templates/params_template template_name=params_template
# time="2022-08-07T11:05:17.984+0300" level=info msg="Template instance loaded" logger=templates template_dir=examples/templates/schema_template template_loader=instance template_name=schema_template template_part=template template_path=template.tmpl template_type=go_template
# time="2022-08-07T11:05:17.984+0300" level=info msg="Schema loaded" logger=templates template_dir=examples/templates/schema_template template_loader=schema template_name=schema_template
# time="2022-08-07T11:05:17.984+0300" level=info msg="Template loaded" logger=templates template_dir=examples/templates/schema_template template_name=schema_template
# time="2022-08-07T11:05:17.984+0300" level=info msg="Template instance loaded" logger=templates template_dir=examples/templates/static_template template_loader=instance template_name=static_template template_part=template template_path=template.html template_type=static
# time="2022-08-07T11:05:17.985+0300" level=info msg="Template loaded" logger=templates template_dir=examples/templates/static_template template_name=static_template
# time="2022-08-07T11:05:17.985+0300" level=info msg="Create runner factory"
# time="2022-08-07T11:05:17.985+0300" level=info msg="Start internal server" logger=browser-render-server
# time="2022-08-07T11:05:17.986+0300" level=info msg="Internal server start listen" listen_addr="127.0.0.1:37425" logger=browser-render-server
# time="2022-08-07T11:05:17.986+0300" level=info msg="Start HTTP server" listen_addr="[::]:9999" logger=http-server
# time="2022-08-07T11:05:17.986+0300" level=info msg="Debug tools (Tester UI, Swagger UI and etc.): http://[::]:9999/" logger=http-server
```

Open link in last log line (http://[::]:9999/ in example) and goto tester.

Or open new terminal.

## Check
```bash
curl -v -X POST -H 'Content-Type: application/json' -d '{"value": 123}' http://localhost:9999/templates/mustache_template/render > mustache.pdf
# Note: Unnecessary use of -X or --request, POST is already inferred.
# *   Trying 127.0.0.1:9999...
#   % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
#                                  Dload  Upload   Total   Spent    Left  Speed
#   0     0    0     0    0     0      0      0 --:--:-- --:--:-- --:--:--     0* Connected to localhost (127.0.0.1) port 9999 (#0)
# > POST /templates/mustache_template/render HTTP/1.1
# > Host: localhost:9999
# > User-Agent: curl/7.82.0
# > Accept: */*
# > Content-Type: application/json
# > Content-Length: 14
# >
# } [14 bytes data]
# * Mark bundle as not supporting multiuse
# < HTTP/1.1 200 OK
# < Content-Type: application/pdf
# < Date: Thu, 31 Mar 2022 07:41:03 GMT
# < Transfer-Encoding: chunked
# <
# { [3981 bytes data]
# 100  5547    0  5533  100    14  23108     58 --:--:-- --:--:-- --:--:-- 23209
# * Connection #0 to host localhost left intact

# On linux
xdg-open mustache.pdf

# On OS X
open mustache.pdf
```
