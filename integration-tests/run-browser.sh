#!/bin/sh

set -xe

export BROWSER=${1:-chromium}
echo BROWSER=${BROWSER}

export EXPECTED_BROWSER=${2:-chrome}
echo EXPECTED_BROWSER=${EXPECTED_BROWSER}

FACTORY_TYPES=${3:-runner page}

BUILD_ID=$(uuidgen)

export EXAMPLES_IMAGE=pdf-server:${BUILD_ID}-${BROWSER}
echo "EXAMPLES_IMAGE=${EXAMPLES_IMAGE}"

export ROBOT_IMAGE=pdf-server-robot:${BUILD_ID}

clean() {
    if docker compose down --rmi local --remove-orphans; then
        echo "Docker compose down successful"
    fi

    if docker image rm "${EXAMPLES_IMAGE}"; then
        echo "Image ${EXAMPLES_IMAGE} removed"
    fi

    if docker image rm "${ROBOT_IMAGE}"; then
        echo "Image ${ROBOT_IMAGE} removed"
    fi
}

trap clean EXIT

earthly --output ../examples/+${BROWSER}-image-push --EXAMPLES_IMAGE=${EXAMPLES_IMAGE}

earthly --output +robot-image-push --ROBOT_IMAGE=${ROBOT_IMAGE}

for factory_type in ${FACTORY_TYPES}; do
    echo "Run integration tests on ${BROWSER} with ${factory_type} factory type"

    export FACTORY_TYPE="${factory_type}"

    LOG_LEVEL=info docker compose run --rm --no-TTY pdf-server validate

    if ! docker compose run --rm --no-TTY tests; then
        docker compose logs
        exit 1
    fi
done
