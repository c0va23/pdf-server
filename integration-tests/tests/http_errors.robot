*** Settings ***
Library  RequestsLibrary
Resource  variables.resource

*** Test Cases ***

Example Not Found
    ${exampleResponse}=  GET
    ...  ${PDF_SERVER_BASE_URL}/templates/examples_template/examples/not_exists
    ...  expected_status=404

Example Template Not Found
    ${exampleResponse}=  GET
    ...  ${PDF_SERVER_BASE_URL}/templates/not_exists/examples/bob
    ...  expected_status=404

Render Template Not Found
    &{requestPayload}=  Create dictionary

    POST
    ...  ${PDF_SERVER_BASE_URL}/templates/not_exists/render
    ...  expected_status=404
    ...  json=${requestPayload}

Render With Bad Request
    POST
    ...  ${PDF_SERVER_BASE_URL}/templates/static_template/render
    ...  expected_status=400
    ...  json=invalidJson
