*** Variables ***
${EXPECTED_BROWSER} =  %{EXPECTED_BROWSER=chrome}


*** Settings ***
Library  RequestsLibrary
Resource  variables.resource

*** Test Cases ***

Status should be success
    GET  ${PDF_SERVER_BASE_URL}/_status  expected_status=200

Status response pool stats
    ${response} =  GET  ${PDF_SERVER_BASE_URL}/_status  expected_status=200

    Should Be Equal As Numbers  ${response.json()['pool_stats']['active']}  0  msg=active
    Should Be Equal As Numbers  ${response.json()['pool_stats']['idle']}  1  msg=idle
    Should Be Equal As Numbers  ${response.json()['pool_stats']['destroyed']}  0  msg=destroyed
    Should Be Equal As Numbers  ${response.json()['pool_stats']['invalidated']}  0  msg=invalidated

Status response render type is ${EXPECTED_RENDER_POOL_TYPE}
    ${response} =  GET  ${PDF_SERVER_BASE_URL}/_status  expected_status=200
    Should Be Equal  ${response.json()['render_pool_type']}  ${EXPECTED_RENDER_POOL_TYPE}  msg=render_pool_type

Status response with metadata
    ${response} =  GET  ${PDF_SERVER_BASE_URL}/_status  expected_status=200
    Should Contain  ${response.json()['metadata']['browser']}  ${EXPECTED_BROWSER}  msg=browser  ignore_case=True
