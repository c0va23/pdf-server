*** Settings ***
Library  RequestsLibrary
Resource  variables.resource


*** Keywords ***

Get Example
    [Arguments]  ${exampleName}
    ${exampleResponse}=  GET
    ...  ${PDF_SERVER_BASE_URL}/templates/examples_template/examples/${exampleName}
    ...  expected_status=200
    Should Be Equal  ${exampleResponse.headers['Content-Type']}  application/pdf
    Should Match Regexp  ${exampleResponse.text}  ${PDF_MAGIC_REGEX}


*** Test Cases ***

Example bob
    Get Example  exampleName=bob

Example alice
    Get Example  exampleName=alice
