*** Settings ***
Library  RequestsLibrary
Library  String
Resource  variables.resource


*** Keywords ***

Response Should be PDF
    [Arguments]  ${templateResponse}
    Should Be Equal  ${templateResponse.headers['Content-Type']}  application/pdf
    Should Match Regexp  ${templateResponse.text}  ${PDF_MAGIC_REGEX}

Escape URL Path
    [Arguments]  ${url_path}

    ${escaped_url_path}=  Replace String  ${url_path}  /  %2F

    RETURN  ${escaped_url_path}


*** Test Cases ***

Success Mustache Template
    &{requestPayload}=  Create dictionary  value=example
    ${templateResponse}=  POST
    ...  ${PDF_SERVER_BASE_URL}/templates/mustache_template/render
    ...  expected_status=200
    ...  json=${requestPayload}
    Response Should be PDF  ${templateResponse}

Bad Request on Mustache Template
    &{requestPayload}=  Create dictionary  invalidKey=example
    ${templateResponse}=  POST
    ...  ${PDF_SERVER_BASE_URL}/templates/mustache_template/render
    ...  expected_status=500
    ...  json=${requestPayload}

Success Go Template
    &{requestPayload}=  Create dictionary  value=example
    ${templateResponse}=  POST
    ...  ${PDF_SERVER_BASE_URL}/templates/go_template/render
    ...  expected_status=200
    ...  json=${requestPayload}
    Response Should be PDF  ${templateResponse}

Success Handlebars Template
    @{requestItems}=  Create List  First Item  Second Item  Third Item
    &{requestPayload}=  Create dictionary  value=example  items=${requestItems}

    ${templateResponse}=  POST
    ...  ${PDF_SERVER_BASE_URL}/templates/handlebars_partials/render
    ...  expected_status=200
    ...  json=${requestPayload}

    Response Should be PDF  ${templateResponse}

Success Composite Template
    @{iterations}=  Create List  ${1}  ${2}  ${3}  ${4}  ${5}
    &{embedded}=  Create Dictionary  value  Embedded
    &{requestPayload}=  Create dictionary  value=example  iterations=@{iterations}  embedded=&{embedded}
    ${templateResponse}=  POST
    ...  ${PDF_SERVER_BASE_URL}/templates/composition:only_static/render
    ...  expected_status=200
    ...  json=${requestPayload}
    Response Should be PDF  ${templateResponse}


Success Composite Template With Recursive Templates
    &{requestPayload}=  Create dictionary  value=Recursive value
    ${templateResponse}=  POST
    ...  ${PDF_SERVER_BASE_URL}/templates/composition:recursive/render
    ...  expected_status=200
    ...  json=${requestPayload}
    Response Should be PDF  ${templateResponse}


Success Template Within First Level of Recursive Templates
    &{requestPayload}=  Create dictionary  value=Recursive value
    ${raw_template_name}=  Set Variable  recursive/level-1
    ${template_name}=  Escape URL Path  ${raw_template_name}
    ${templateResponse}=  POST
    ...  ${PDF_SERVER_BASE_URL}/templates/${template_name}/render
    ...  expected_status=200
    ...  json=${requestPayload}
    Response Should be PDF  ${templateResponse}


Success Template Within Second Level of Recursive Templates
    &{requestPayload}=  Create dictionary  value=Recursive value
    ${raw_template_name}=  Set Variable  recursive/sub-level-1/level-2
    ${template_name}=  Escape URL Path  ${raw_template_name}
    ${templateResponse}=  POST
    ...  ${PDF_SERVER_BASE_URL}/templates/${template_name}/render
    ...  expected_status=200
    ...  json=${requestPayload}
    Response Should be PDF  ${templateResponse}


Success Template Within Third Level of Recursive Templates
    &{requestPayload}=  Create dictionary  value=Recursive value
    ${raw_template_name}=  Set Variable  recursive/sub-level-1/sub-level-2/level-3
    ${template_name}=  Escape URL Path  ${raw_template_name}
    ${templateResponse}=  POST
    ...  ${PDF_SERVER_BASE_URL}/templates/${template_name}/render
    ...  expected_status=200
    ...  json=${requestPayload}
    Response Should be PDF  ${templateResponse}


Success Composite JavaScript Template
    @{iterations}=  Create List  ${1}  ${2}  ${3}  ${4}  ${5}
    &{embedded}=  Create Dictionary  value  Embedded
    &{requestPayload}=  Create dictionary  value=example  iterations=@{iterations}  embedded=&{embedded}

    ${template_name}=  Set Variable  composition:js-example
    ${templateResponse}=  POST
    ...  ${PDF_SERVER_BASE_URL}/templates/${template_name}/render
    ...  expected_status=200
    ...  json=${requestPayload}
    Response Should be PDF  ${templateResponse}
