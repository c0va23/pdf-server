*** Settings ***
Library  RequestsLibrary
Resource  variables.resource


*** Keywords ***

Response Should be PDF
    [Arguments]  ${templateResponse}
    Should Be Equal  ${templateResponse.headers['Content-Type']}  application/pdf
    Should Match Regexp  ${templateResponse.text}  ${PDF_MAGIC_REGEX}


*** Test Cases ***

Success Schema Request
    &{requestPayload}=  Create dictionary  value=example
    ${templateResponse}=  POST
    ...  ${PDF_SERVER_BASE_URL}/templates/schema_template/render
    ...  expected_status=200
    ...  json=${requestPayload}
    Response Should be PDF  ${templateResponse}

Unprocessable Entity on Schema Template
    &{requestPayload}=  Create dictionary  invalidKey=example
    ${templateResponse}=  POST
    ...  ${PDF_SERVER_BASE_URL}/templates/schema_template/render
    ...  expected_status=422
    ...  json=${requestPayload}
