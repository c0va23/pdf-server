
#!/bin/sh

set -xe

./run-browser.sh "chromium" "chrome" "runner page"

# Firefox not support page factory (on CDP not implemented POST /json/new)
./run-browser.sh "firefox" "firefox" "runner"
