VERSION 0.8

ARG --global ALPINE_VERSION=3.17

robot-image:
	ARG PYTHON_VERSION=3.10

	FROM python:${PYTHON_VERSION}-alpine${ALPINE_VERSION}

	RUN apk add --no-cache \
		docker \
		jq

	WORKDIR /tests

	COPY requirements.txt .
	RUN python -m pip install --upgrade pip \
		&& pip --version \
		&& pip install -r requirements.txt

	ARG USEROS
	ARG USERARCH

	RUN export COMPOSE_VERSION=2.17.3 && \
		export COMPOSE_ARCH=$(test ${USERARCH} = amd64 && echo 'x86_64' || echo ${USERARCH}) && \
		export COMPOSE_URL=https://github.com/docker/compose/releases/download/v${COMPOSE_VERSION}/docker-compose-${USEROS}-${COMPOSE_ARCH} && \
		export COMPOSE_PATH=/usr/bin/docker-compose && \
		env | grep COMPOSE && \
		wget --quiet -O  "${COMPOSE_PATH}" "${COMPOSE_URL}" && \
		chmod +x "${COMPOSE_PATH}" && \
		docker-compose version

	SAVE IMAGE --cache-hint

robot-image-push:
	FROM +robot-image

	ARG ROBOT_IMAGE=pdf-server-robot:latest

	SAVE IMAGE --push ${ROBOT_IMAGE}

test-image:
	FROM +robot-image

	COPY --dir compose.yaml \
		tests.sh \
		lib \
		tests \
		.

	SAVE IMAGE --cache-hint

all-tests:
	FROM alpine:${ALPINE_VERSION}

	# Force cache
	WAIT
		BUILD +test-image
	END

	COPY +chromium-tests/reports ./reports/chromium

	# Firefox temporary disabled
	# COPY +firefox-tests/reports ./reports/firefox

	SAVE ARTIFACT ./reports . AS LOCAL ./reports

chromium-tests:
	FROM alpine:${ALPINE_VERSION}

	BUILD ../examples+chromium-image

	# Double from one image, brake caching

	COPY (+test/reports \
		--IMAGE=../examples+chromium-image \
		--EXPECTED_BROWSER=chrome \
		--FACTORY_TYPE=runner) \
		./runner

	SAVE ARTIFACT ./runner ./reports/

	COPY (+test/reports \
		--IMAGE=../examples+chromium-image \
		--EXPECTED_BROWSER=chrome \
		--FACTORY_TYPE=page) \
		./page

	SAVE ARTIFACT ./page ./reports/


firefox-tests:
	FROM alpine:${ALPINE_VERSION}

	BUILD ../examples+firefox-image

	COPY (+test/reports \
		--IMAGE=../examples+firefox-image \
		--EXPECTED_BROWSER=firefox \
		--FACTORY_TYPE=runner) \
		./runner

	# Firefox not support page factory type

	SAVE ARTIFACT ./runner ./reports/

test:
	FROM +test-image

	ARG IMAGE=../examples+chromium-image
	ARG EXPECTED_BROWSER=chrome
	ARG FACTORY_TYPE=runner

	ENV FACTORY_TYPE=${FACTORY_TYPE}

	WITH DOCKER \
			--compose compose.yaml \
			--service pdf-server \
			--load pdf-server:latest=${IMAGE}
		RUN EXPECTED_BROWSER=${EXPECTED_BROWSER} \
			&& sh ./tests.sh
	END

	SAVE ARTIFACT ./reports .
