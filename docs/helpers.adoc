= Helpers

Handlebars and Go templates support helpers. These are functions that can be called from within templates.

See helper usage into examples:

* link:../examples/helpers_handlebars[Handlebars example]
* link:../examples/helpers_go_template[Go template example]


== Helper `format_time`

Helper `format_time` format time in iso8601 representation with strftime formatting pattern.

For supported strftime formatting patterns see
https://pkg.go.dev/github.com/lestrrat-go/strftime#readme-supported-conversion-specifications.

Handlebars template usage example:
[source,handlebars]
----
{{format_time "%Y-%m-%dT%H:%M:%S%z" from_time}}

{{format_time "%A %d %B, %y" "2014-01-01T00:00:00" }}
----

Go template usage example:
[source,go]
----
Positional dynamic argument:
{{format_time "%Y-%m-%dT%H:%M:%S%z" .from_time}}

Dynamic pipe:
{{.from_time | format_time "%Y-%m-%dT%H:%M:%S%z"}}

Positional Static argument:
{{format_time "%A %d %B, %y" "2014-01-01T00:00:00" }}

Static pipe:
{{ "2014-01-01T00:00:00" | format_time "%A %d %B, %y"  }}
----


== Helper `markdown`

Helper `markdown` converts markdown to html. Return safe html.


See supported markdown extensions see
https://github.com/gomarkdown/markdown

Handlebars template usage example:
[source,handlebars]
----
{{markdown "# Hello World"}}
----

Go template usage example:
[source,go]
----
{{markdown "# Hello World"}}

{{.markdown | markdown}}
----
