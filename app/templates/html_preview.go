package templates

import (
	"context"
	"errors"
)

// HTMLPreviewer provide previews on HTML2PDF templates.
type HTMLPreviewer interface {
	RenderBodyHTML(context.Context, Data) (string, error)
	RenderHeaderHTML(context.Context, Data) (*string, error)
	RenderFooterHTML(context.Context, Data) (*string, error)
}

// NoTemplateError error returned by HtmlPreviewer if template not exist.
type NoTemplateError struct{}

// NewNoTemplateError constructor.
func NewNoTemplateError() *NoTemplateError {
	return new(NoTemplateError)
}

// Error interface implementation.
func (*NoTemplateError) Error() string {
	return "no template"
}

// Is interface implementation.
func (*NoTemplateError) Is(err error) bool {
	if noTemplateError := new(NoTemplateError); errors.As(err, &noTemplateError) {
		return true
	}

	return false
}
