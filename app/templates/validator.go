package templates

import (
	"bytes"
	"context"
	"errors"
	"fmt"
	"io"

	jsonSchemaV5 "github.com/santhosh-tekuri/jsonschema/v5"

	"gitlab.com/c0va23/pdf-server/app/log"
)

// Validator validate render data.
type Validator interface {
	ValidateData(ctx context.Context, data Data) error
	MarshalJSON() ([]byte, error)
}

// InvalidDataSchemaError returned by Validator when provided data is not valid.
type InvalidDataSchemaError struct {
	cause error
}

// NewInvalidDataSchemaError construct new InvalidDataSchemaError.
func NewInvalidDataSchemaError(cause error) *InvalidDataSchemaError {
	return &InvalidDataSchemaError{
		cause: cause,
	}
}

// Error return error message.
func (err *InvalidDataSchemaError) Error() string {
	return fmt.Sprintf("invalid data schema: %s", err.cause)
}

// Unwrap return original error.
func (err *InvalidDataSchemaError) Unwrap() error {
	return err.cause
}

// Is used by errors.Is.
func (*InvalidDataSchemaError) Is(otherError error) bool {
	if castedError := new(InvalidDataSchemaError); errors.As(otherError, &castedError) {
		return true
	}

	return false
}

// JSONSchemaValidator validate data with json schema.
type JSONSchemaValidator struct {
	rawSchema  []byte
	jsonSchema *jsonSchemaV5.Schema
}

// NewJSONSchemaValidator construct new JSONSchemaValidator.
func NewJSONSchemaValidator(rawSchema []byte) (*JSONSchemaValidator, error) {
	schemaReader := io.NopCloser(bytes.NewReader(rawSchema))

	schemaCompiler := jsonSchemaV5.NewCompiler()
	if err := schemaCompiler.AddResource("schema.json", schemaReader); err != nil {
		return nil, fmt.Errorf("add schema: %w", err)
	}

	jsonSchema, err := schemaCompiler.Compile("schema.json")
	if err != nil {
		return nil, fmt.Errorf("compile schema: %w", err)
	}

	return &JSONSchemaValidator{
		rawSchema:  rawSchema,
		jsonSchema: jsonSchema,
	}, nil
}

// ValidateData match provided data with JSON schema.
func (validator *JSONSchemaValidator) ValidateData(ctx context.Context, data Data) error {
	jsonData := (map[string]interface{})(data)

	err := validator.jsonSchema.Validate(jsonData)
	if err != nil {
		logger := log.LoggerFromContext(ctx, "schemaValidator")
		logger.WithField(log.FieldRenderDataErrors, err).Warn("Invalid data")

		return NewInvalidDataSchemaError(err)
	}

	return nil
}

// MarshalJSON return raw schema representation.
//
//nolint:unparam
func (validator *JSONSchemaValidator) MarshalJSON() ([]byte, error) {
	return validator.rawSchema, nil
}
