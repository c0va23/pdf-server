package fileloader

import (
	"errors"
	"fmt"

	"gitlab.com/c0va23/pdf-server/app/log"
	"gitlab.com/c0va23/pdf-server/app/templates"
)

const allTemplateType = "all"

// TemplateInstanceLoader interface.
type TemplateInstanceLoader interface {
	LoadTemplateInstance(
		templateDirPath string,
		partName string,
		logger log.Logger,
		assets templates.Assets,
	) (HTMLTemplate, error)
	TemplateType() string
	TemplateName(partName string) string
}

// FileTemplateInstanceLoader load templates from file system.
type FileTemplateInstanceLoader struct {
	templateInstanceLoaders []TemplateInstanceLoader
}

// NewFileTemplateInstanceLoader create new FileTemplateInstanceLoader with
// defined template engine loader.
func NewFileTemplateInstanceLoader(
	templateInstanceLoaders ...TemplateInstanceLoader,
) *FileTemplateInstanceLoader {
	return &FileTemplateInstanceLoader{
		templateInstanceLoaders: templateInstanceLoaders,
	}
}

// DefaultFileTemplateInstanceLoader create FileTemplateInstanceLoader with default
// list of template engine loaders.
func DefaultFileTemplateInstanceLoader(
	partialsLoader PartialsLoader,
	markdownHelpers MarkdownHelpers,
) *FileTemplateInstanceLoader {
	return NewFileTemplateInstanceLoader(
		NewMustacheTemplateInstanceLoader(partialsLoader),
		NewHandlebarsTemplateInstanceLoader(partialsLoader, markdownHelpers),
		NewGoTemplateInstanceLoader(partialsLoader, markdownHelpers),
		new(StaticTemplateInstanceLoader),
	)
}

// LoadTemplateInstance iterate over template instance factory when it not return error.
func (instanceLoader FileTemplateInstanceLoader) LoadTemplateInstance(
	templateDirPath string,
	partName string,
	logger log.Logger,
	assets templates.Assets,
) (HTMLTemplate, error) {
	logger = logger.
		WithField(log.FieldTemplateLoader, "instance").
		WithField(log.FieldTemplatePart, partName)

	for _, loader := range instanceLoader.templateInstanceLoaders {
		loaderLogger := logger.
			WithField(log.FieldTemplateType, loader.TemplateType()).
			WithField(log.FieldTemplatePath, loader.TemplateName(partName))

		loaderLogger.Debug("Template instance loading")
		templateInstance, err := loader.LoadTemplateInstance(
			templateDirPath,
			partName,
			loaderLogger,
			assets,
		)

		if notFoundErr := new(TemplateFileNotFoundError); errors.As(err, &notFoundErr) {
			loaderLogger.WithError(err).Debug("Template file not found")

			continue
		}

		if err != nil {
			return nil, fmt.Errorf("load template: %w", err)
		}

		loaderLogger.Debug("Template instance loaded")

		return templateInstance, nil
	}

	return nil, NewTemplateFileNotFoundError(allTemplateType, templateDirPath)
}
