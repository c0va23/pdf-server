package fileloader_test

import (
	"errors"
	"fmt"
	"path/filepath"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	noopTrace "go.opentelemetry.io/otel/trace/noop"

	"gitlab.com/c0va23/pdf-server/app/log"
	"gitlab.com/c0va23/pdf-server/app/templates"
	"gitlab.com/c0va23/pdf-server/app/templates/fileloader"
	"gitlab.com/c0va23/pdf-server/app/templates/futures"

	templatesMocks "gitlab.com/c0va23/pdf-server/mocks/templates"
	fileloaderMocks "gitlab.com/c0va23/pdf-server/mocks/templates/fileloader"
)

func TestFileTemplateLoader_LoadTemplate(t *testing.T) {
	t.Parallel()

	const (
		bodyPartName   = "template"
		footerPartName = "footer"
		headerPartName = "header"

		templatesDirPath = "fixtures"
		templateName     = "template"
	)

	var (
		logger = log.NewLogger(log.LevelFatal, log.TextFormat)

		templateDirPath = filepath.Join(templatesDirPath, templateName)
		templateLogger  = logger.
				WithField(log.FieldTemplateDir, templateDirPath).
				WithField(log.FieldTemplateName, templateName)

		nilTemplate templates.Template = nil

		emptySchemaLoaderMock = new(templatesMocks.SchemaLoader)
		emptyParamsLoader     = new(fileloaderMocks.ParamsLoader)
		emptyExamplesLoader   = new(templatesMocks.ExamplesLoader)
		emptyAssetsMock       = new(templatesMocks.Assets)
	)

	emptyAssetsLoader := func() *fileloaderMocks.AssetsLoader {
		assetsMock := new(fileloaderMocks.AssetsLoader)

		assetsMock.EXPECT().
			LoadAssets(mock.Anything, mock.Anything).
			Return(emptyAssetsMock)

		return assetsMock
	}

	type TemplateInstanceLoaderMockCalls struct {
		bodyTemplateInstance   *fileloaderMocks.HTMLTemplate
		bodyError              error
		headerTemplateInstance fileloader.HTMLTemplate
		headerError            error
		footerTemplateInstance fileloader.HTMLTemplate
		footerError            error
	}

	onlyBodyTemplateInstanceLoaderMockCalls := func(
		bodyTemplateInstance *fileloaderMocks.HTMLTemplate,
	) TemplateInstanceLoaderMockCalls {
		return TemplateInstanceLoaderMockCalls{
			bodyTemplateInstance:   bodyTemplateInstance,
			bodyError:              nil,
			headerTemplateInstance: nil,
			headerError:            fileloader.NewTemplateFileNotFoundError("static", "header.html"),
			footerTemplateInstance: nil,
			footerError:            fileloader.NewTemplateFileNotFoundError("static", "header.html"),
		}
	}

	templateInstanceLoaderMockProvider := func(
		call TemplateInstanceLoaderMockCalls,
		assets templates.Assets,
	) *fileloaderMocks.TemplateInstanceLoader {
		templateInstanceLoader := new(fileloaderMocks.TemplateInstanceLoader)

		templateInstanceLoader.EXPECT().LoadTemplateInstance(
			templateDirPath,
			bodyPartName,
			templateLogger,
			assets,
		).Return(call.bodyTemplateInstance, call.bodyError).Once()

		if call.headerTemplateInstance != nil || call.headerError != nil {
			templateInstanceLoader.EXPECT().LoadTemplateInstance(
				templateDirPath,
				headerPartName,
				templateLogger,
				assets,
			).Return(call.headerTemplateInstance, call.headerError).Once()
		}

		if call.footerTemplateInstance != nil || call.footerError != nil {
			templateInstanceLoader.EXPECT().LoadTemplateInstance(
				templateDirPath,
				footerPartName,
				templateLogger,
				assets,
			).Return(call.footerTemplateInstance, call.footerError).Once()
		}

		return templateInstanceLoader
	}

	schemaLoaderMockProvider := func(
		validator templates.Validator,
		err error,
	) *templatesMocks.SchemaLoader {
		schemaLoaderMock := new(templatesMocks.SchemaLoader)
		schemaLoaderMock.EXPECT().LoadSchema(
			templateDirPath,
			templateLogger,
		).Return(validator, err)

		return schemaLoaderMock
	}

	paramsLoaderMockProvider := func(
		params *fileloader.Params,
		err error,
	) *fileloaderMocks.ParamsLoader {
		paramsLoaderMock := new(fileloaderMocks.ParamsLoader)

		paramsLoaderMock.EXPECT().LoadParams(
			templateDirPath,
			templateLogger,
		).Return(params, err)

		return paramsLoaderMock
	}

	assetsLoaderMockProvider := func(
		assetStore templates.Assets,
	) *fileloaderMocks.AssetsLoader {
		assetsLoaderMock := new(fileloaderMocks.AssetsLoader)
		assetsLoaderMock.EXPECT().LoadAssets(
			templateDirPath,
			templateLogger,
		).Return(assetStore)

		return assetsLoaderMock
	}

	examplesLoaderMockProvider := func(
		examples templates.Examples,
		err error,
	) *templatesMocks.ExamplesLoader {
		examplesLoaderMock := new(templatesMocks.ExamplesLoader)
		examplesLoaderMock.EXPECT().LoadExamples(
			templateDirPath,
			templateLogger,
		).Return(examples, err)

		return examplesLoaderMock
	}

	tracerProvider := noopTrace.NewTracerProvider()

	tracer := tracerProvider.Tracer("template-html2pdf/template")

	scheduler := futures.NewNoopScheduler[templates.RenderResult]()

	type TestCase struct {
		name                   string
		templateInstanceLoader *fileloaderMocks.TemplateInstanceLoader
		schemaLoader           *templatesMocks.SchemaLoader
		paramsLoader           *fileloaderMocks.ParamsLoader
		assetsLoader           *fileloaderMocks.AssetsLoader
		examplesLoader         *templatesMocks.ExamplesLoader

		expectedTemplate templates.Template
		expectedErr      error
	}

	testCases := []TestCase{
		func() TestCase {
			templateErr := errors.New("template error")

			return TestCase{
				name: "load template instance return error",
				templateInstanceLoader: templateInstanceLoaderMockProvider(
					TemplateInstanceLoaderMockCalls{
						bodyError: templateErr,
					},
					emptyAssetsMock,
				),
				schemaLoader:     emptySchemaLoaderMock,
				paramsLoader:     emptyParamsLoader,
				assetsLoader:     emptyAssetsLoader(),
				examplesLoader:   emptyExamplesLoader,
				expectedTemplate: nilTemplate,
				expectedErr:      templateErr,
			}
		}(),
		func() TestCase {
			templateErr := fmt.Errorf(
				"body error: %w",
				fileloader.NewTemplateFileNotFoundError("body", "index.html"),
			)

			return TestCase{
				name: "body not found",
				templateInstanceLoader: templateInstanceLoaderMockProvider(
					TemplateInstanceLoaderMockCalls{
						bodyError: templateErr,
					},
					emptyAssetsMock,
				),
				schemaLoader:     emptySchemaLoaderMock,
				paramsLoader:     emptyParamsLoader,
				assetsLoader:     emptyAssetsLoader(),
				examplesLoader:   emptyExamplesLoader,
				expectedTemplate: nilTemplate,
				expectedErr:      templateErr,
			}
		}(),
		func() TestCase {
			templateInstance := new(fileloaderMocks.HTMLTemplate)

			return TestCase{
				name: "only body",
				templateInstanceLoader: templateInstanceLoaderMockProvider(
					onlyBodyTemplateInstanceLoaderMockCalls(templateInstance),
					emptyAssetsMock,
				),
				schemaLoader:   schemaLoaderMockProvider(nil, nil),
				paramsLoader:   paramsLoaderMockProvider(nil, nil),
				assetsLoader:   assetsLoaderMockProvider(emptyAssetsMock),
				examplesLoader: examplesLoaderMockProvider(nil, nil),
				expectedTemplate: &fileloader.HTML2PDFTemplate{
					Name:      templateName,
					Body:      templateInstance,
					Tracer:    tracer,
					Assets:    emptyAssetsMock,
					Scheduler: scheduler,
				},
				expectedErr: nil,
			}
		}(),
		func() TestCase {
			templateInstance := new(fileloaderMocks.HTMLTemplate)

			landscape := true
			params := &fileloader.Params{
				PDF: fileloader.PDFParams{
					Landscape: &landscape,
				},
			}

			return TestCase{
				name: "body with params",
				templateInstanceLoader: templateInstanceLoaderMockProvider(
					onlyBodyTemplateInstanceLoaderMockCalls(templateInstance),
					emptyAssetsMock,
				),
				schemaLoader:   schemaLoaderMockProvider(nil, nil),
				paramsLoader:   paramsLoaderMockProvider(params, nil),
				assetsLoader:   assetsLoaderMockProvider(emptyAssetsMock),
				examplesLoader: examplesLoaderMockProvider(nil, nil),
				expectedTemplate: &fileloader.HTML2PDFTemplate{
					Name:      templateName,
					Body:      templateInstance,
					Params:    params,
					Tracer:    tracer,
					Assets:    emptyAssetsMock,
					Scheduler: scheduler,
				},
				expectedErr: nil,
			}
		}(),
		func() TestCase {
			templateInstance := new(fileloaderMocks.HTMLTemplate)

			paramsErr := errors.New("params error")

			return TestCase{
				name: "load params return error",
				templateInstanceLoader: templateInstanceLoaderMockProvider(
					onlyBodyTemplateInstanceLoaderMockCalls(templateInstance),
					emptyAssetsMock,
				),
				schemaLoader:     schemaLoaderMockProvider(nil, nil),
				paramsLoader:     paramsLoaderMockProvider(nil, paramsErr),
				assetsLoader:     assetsLoaderMockProvider(emptyAssetsMock),
				examplesLoader:   examplesLoaderMockProvider(nil, nil),
				expectedTemplate: nil,
				expectedErr:      paramsErr,
			}
		}(),
		func() TestCase {
			templateInstance := new(fileloaderMocks.HTMLTemplate)

			schemaErr := errors.New("schema error")

			return TestCase{
				name: "schema load error",
				templateInstanceLoader: templateInstanceLoaderMockProvider(
					onlyBodyTemplateInstanceLoaderMockCalls(templateInstance),
					emptyAssetsMock,
				),
				schemaLoader:     schemaLoaderMockProvider(nil, schemaErr),
				paramsLoader:     paramsLoaderMockProvider(nil, nil),
				assetsLoader:     assetsLoaderMockProvider(emptyAssetsMock),
				examplesLoader:   examplesLoaderMockProvider(nil, nil),
				expectedTemplate: nilTemplate,
				expectedErr:      schemaErr,
			}
		}(),
		func() TestCase {
			templateInstance := new(fileloaderMocks.HTMLTemplate)

			validator := new(templatesMocks.Validator)

			return TestCase{
				name: "body with schema",
				templateInstanceLoader: templateInstanceLoaderMockProvider(
					onlyBodyTemplateInstanceLoaderMockCalls(templateInstance),
					emptyAssetsMock,
				),
				schemaLoader:   schemaLoaderMockProvider(validator, nil),
				paramsLoader:   paramsLoaderMockProvider(nil, nil),
				assetsLoader:   assetsLoaderMockProvider(emptyAssetsMock),
				examplesLoader: examplesLoaderMockProvider(nil, nil),
				expectedTemplate: &fileloader.HTML2PDFTemplate{
					Name:      templateName,
					Body:      templateInstance,
					Validator: validator,
					Tracer:    tracer,
					Assets:    emptyAssetsMock,
					Scheduler: scheduler,
				},
				expectedErr: nil,
			}
		}(),
		func() TestCase {
			templateInstance := new(fileloaderMocks.HTMLTemplate)

			examples := templates.Examples{
				"map":   templates.Data{"key": 123},
				"array": templates.Data{"array": []int{1, 2, 3}},
			}

			return TestCase{
				name: "body with examples",
				templateInstanceLoader: templateInstanceLoaderMockProvider(
					onlyBodyTemplateInstanceLoaderMockCalls(templateInstance),
					emptyAssetsMock,
				),
				schemaLoader:   schemaLoaderMockProvider(nil, nil),
				paramsLoader:   paramsLoaderMockProvider(nil, nil),
				assetsLoader:   assetsLoaderMockProvider(emptyAssetsMock),
				examplesLoader: examplesLoaderMockProvider(examples, nil),
				expectedTemplate: &fileloader.HTML2PDFTemplate{
					Name:      templateName,
					Body:      templateInstance,
					Examples:  examples,
					Tracer:    tracer,
					Assets:    emptyAssetsMock,
					Scheduler: scheduler,
				},
				expectedErr: nil,
			}
		}(),
		func() TestCase {
			templateInstance := new(fileloaderMocks.HTMLTemplate)

			examplesErr := errors.New("load examples error")

			return TestCase{
				name: "body with examples",
				templateInstanceLoader: templateInstanceLoaderMockProvider(
					onlyBodyTemplateInstanceLoaderMockCalls(templateInstance),
					emptyAssetsMock,
				),
				schemaLoader:     schemaLoaderMockProvider(nil, nil),
				paramsLoader:     paramsLoaderMockProvider(nil, nil),
				assetsLoader:     assetsLoaderMockProvider(emptyAssetsMock),
				examplesLoader:   examplesLoaderMockProvider(nil, examplesErr),
				expectedTemplate: nil,
				expectedErr:      examplesErr,
			}
		}(),
		func() TestCase {
			templateInstance := new(fileloaderMocks.HTMLTemplate)

			assetStore := new(templatesMocks.Assets)

			return TestCase{
				name: "body with examples",
				templateInstanceLoader: templateInstanceLoaderMockProvider(
					onlyBodyTemplateInstanceLoaderMockCalls(templateInstance),
					emptyAssetsMock,
				),
				schemaLoader:   schemaLoaderMockProvider(nil, nil),
				paramsLoader:   paramsLoaderMockProvider(nil, nil),
				assetsLoader:   assetsLoaderMockProvider(assetStore),
				examplesLoader: examplesLoaderMockProvider(nil, nil),
				expectedTemplate: &fileloader.HTML2PDFTemplate{
					Name:      templateName,
					Body:      templateInstance,
					Assets:    assetStore,
					Tracer:    tracer,
					Scheduler: scheduler,
				},
				expectedErr: nil,
			}
		}(),
		func() TestCase {
			bodyTemplateInstance := new(fileloaderMocks.HTMLTemplate)
			headerTemplateInstance := new(fileloaderMocks.HTMLTemplate)
			footerTemplateInstance := new(fileloaderMocks.HTMLTemplate)

			assetStore := new(templatesMocks.Assets)

			examples := templates.Examples{
				"map":   templates.Data{"key": 123},
				"array": templates.Data{"array": []int{1, 2, 3}},
			}

			validator := new(templatesMocks.Validator)

			landscape := true
			params := &fileloader.Params{
				PDF: fileloader.PDFParams{
					Landscape: &landscape,
				},
			}

			return TestCase{
				name: "all loaders",
				templateInstanceLoader: templateInstanceLoaderMockProvider(
					TemplateInstanceLoaderMockCalls{
						bodyTemplateInstance:   bodyTemplateInstance,
						headerTemplateInstance: headerTemplateInstance,
						footerTemplateInstance: footerTemplateInstance,
					},
					assetStore,
				),
				schemaLoader:   schemaLoaderMockProvider(validator, nil),
				paramsLoader:   paramsLoaderMockProvider(params, nil),
				assetsLoader:   assetsLoaderMockProvider(assetStore),
				examplesLoader: examplesLoaderMockProvider(examples, nil),
				expectedTemplate: &fileloader.HTML2PDFTemplate{
					Name:      templateName,
					Body:      bodyTemplateInstance,
					Header:    headerTemplateInstance,
					Footer:    footerTemplateInstance,
					Assets:    assetStore,
					Params:    params,
					Examples:  examples,
					Validator: validator,
					Tracer:    tracer,
					Scheduler: scheduler,
				},
				expectedErr: nil,
			}
		}(),
	}

	for _, testCase := range testCases {
		t.Run(testCase.name, func(t *testing.T) {
			t.Parallel()

			templateLoader := fileloader.NewFileTemplateLoader(
				testCase.templateInstanceLoader,
				testCase.schemaLoader,
				testCase.paramsLoader,
				testCase.assetsLoader,
				testCase.examplesLoader,
				nil,
				tracerProvider,
				scheduler,
			)

			actualTemplate, actualErr := templateLoader.LoadTemplate(
				templatesDirPath,
				templateName,
				templateLogger,
			)

			assert.Equal(t, testCase.expectedTemplate, actualTemplate)
			assert.ErrorIs(t, actualErr, testCase.expectedErr)
		})
	}
}
