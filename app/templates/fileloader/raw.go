package fileloader

import (
	"context"
	"fmt"
	"os"
	"path/filepath"

	"go.opentelemetry.io/otel/trace"

	"gitlab.com/c0va23/pdf-server/app/log"
	"gitlab.com/c0va23/pdf-server/app/templates"
	"gitlab.com/c0va23/pdf-server/app/templates/futures"
)

const rawTemplateFileName = "raw.pdf"

// RawTemplateLoader loads raw templates.
type RawTemplateLoader struct {
	tracerProvider trace.TracerProvider
}

// NewRawTemplateLoader constructor.
func NewRawTemplateLoader(
	tracerProvider trace.TracerProvider,
) *RawTemplateLoader {
	return &RawTemplateLoader{
		tracerProvider: tracerProvider,
	}
}

// LoadTemplate loads template.
func (loader *RawTemplateLoader) LoadTemplate(
	templatesDirPath string,
	templateName string,
	logger log.Logger,
) (templates.Template, error) {
	templateDirPath := filepath.Join(templatesDirPath, templateName)

	templateLogger := logger.
		WithField(log.FieldTemplateDir, templateDirPath).
		WithField(log.FieldTemplateName, templateName)

	templatePath := filepath.Join(templateDirPath, rawTemplateFileName)

	name := templates.Name(templateName)

	payload, err := os.ReadFile(templatePath)
	if err != nil {
		if os.IsNotExist(err) {
			return nil, templates.NewTemplateNotExistsError(name)
		}

		return nil, fmt.Errorf("read template file: %w", err)
	}

	templateLogger.Infof("Raw template loaded")

	tracer := loader.tracerProvider.Tracer("template-raw/" + templateName)

	return &RawTemplate{
		name:    name,
		payload: payload,
		tracer:  tracer,
	}, nil
}

// TemplateType returns template type.
func (*RawTemplateLoader) TemplateType() string {
	return "raw"
}

// RawTemplate render raw template.
type RawTemplate struct {
	name    templates.Name
	payload []byte
	tracer  trace.Tracer
}

// GetName returns template name.
func (template *RawTemplate) GetName() templates.Name {
	return template.name
}

// GetExamples returns template examples.
func (*RawTemplate) GetExamples() templates.Examples {
	return nil
}

// GetValidator returns template validator.
func (*RawTemplate) GetValidator() templates.Validator {
	return nil
}

func (template *RawTemplate) renderSpanName() string {
	return fmt.Sprintf("template-raw/%s.render()", template.name)
}

// Render renders template.
func (template *RawTemplate) Render(
	ctx context.Context,
	_ templates.Data,
) templates.RenderFuture {
	_, span := template.tracer.Start(ctx, template.renderSpanName())
	defer span.End()

	return futures.NewResolved(futures.SuccessResult(template.payload))
}
