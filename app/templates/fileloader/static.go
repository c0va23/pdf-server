package fileloader

import (
	"fmt"
	"os"
	"path/filepath"

	"gitlab.com/c0va23/pdf-server/app/log"
	"gitlab.com/c0va23/pdf-server/app/templates"
)

const staticTemplateExtension = ".html"

// StaticTemplate copy HTML on render.
type StaticTemplate struct {
	payload string
	logger  log.Logger
}

// NewStaticTemplate constructor.
func NewStaticTemplate(
	payload string,
	logger log.Logger,
) *StaticTemplate {
	return &StaticTemplate{
		payload: payload,
		logger:  logger,
	}
}

// Render static template.
func (st *StaticTemplate) Render(_ interface{}) (string, error) {
	return st.payload, nil
}

// StaticTemplateInstanceLoader load static template.
type StaticTemplateInstanceLoader struct{}

// TemplateType return static template type.
func (*StaticTemplateInstanceLoader) TemplateType() string {
	return "static"
}

// TemplateName return static template name.
func (*StaticTemplateInstanceLoader) TemplateName(partName string) string {
	return partName + staticTemplateExtension
}

// LoadTemplateInstance static partName template from templateDir.
func (loader *StaticTemplateInstanceLoader) LoadTemplateInstance(
	templateDir string,
	partName string,
	logger log.Logger,
	_ templates.Assets,
) (HTMLTemplate, error) {
	templatePath := filepath.Join(templateDir, loader.TemplateName(partName))

	payload, err := os.ReadFile(templatePath) //nolint:gosec
	if nil != err {
		if os.IsNotExist(err) {
			return nil, NewTemplateFileNotFoundError(loader.TemplateType(), templatePath)
		}

		return nil, fmt.Errorf("read static file: %w", err)
	}

	return NewStaticTemplate(string(payload), logger), nil
}
