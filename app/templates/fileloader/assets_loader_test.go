package fileloader_test

import (
	"net/http"
	"path/filepath"
	"testing"

	"github.com/stretchr/testify/assert"

	"gitlab.com/c0va23/pdf-server/app/log"
	"gitlab.com/c0va23/pdf-server/app/templates"
	"gitlab.com/c0va23/pdf-server/app/templates/fileloader"
)

func TestFileAssetsLoader(t *testing.T) {
	t.Parallel()

	logger := log.NewLogger(log.LevelFatal, log.TextFormat)
	assetLoader := fileloader.NewFileAssetsLoader()

	type TestCase struct {
		name          string
		path          string
		expectedStore templates.Assets
	}

	testCases := []TestCase{
		{
			name: "templates with assets",
			path: "fixtures/templates/assets_template",
			expectedStore: fileloader.NewFileAssets(
				http.Dir(filepath.Join("fixtures/templates/assets_template", "assets")),
			),
		},
		{
			name:          "templates without assets",
			path:          staticTemplatePath,
			expectedStore: nil,
		},
	}

	for _, testCase := range testCases {
		t.Run(testCase.name, func(t *testing.T) {
			t.Parallel()

			actualAssetStore := assetLoader.LoadAssets(testCase.path, logger)

			assert.Equal(t, testCase.expectedStore, actualAssetStore)
		})
	}
}
