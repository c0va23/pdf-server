package fileloader

import (
	"context"
	"errors"
	"fmt"

	"go.opentelemetry.io/otel/codes"
	"go.opentelemetry.io/otel/trace"

	"gitlab.com/c0va23/pdf-server/app/log"
	"gitlab.com/c0va23/pdf-server/app/pdfrender"
	"gitlab.com/c0va23/pdf-server/app/templates"
	"gitlab.com/c0va23/pdf-server/app/templates/futures"
)

// HTMLTemplate render template with data.
type HTMLTemplate interface {
	Render(data interface{}) (string, error)
}

// PDFParams definition.
type PDFParams struct {
	Landscape         *bool    `json:"landscape"`
	PrintBackground   *bool    `json:"printBackground"`
	PreferCSSPageSize *bool    `json:"preferCSSPageSize"`
	Scale             *float64 `json:"scale"`
	PaperWidth        *float64 `json:"paperWidth"`
	PaperHeight       *float64 `json:"paperHeight"`
	MarginTop         *float64 `json:"marginTop"`
	MarginBottom      *float64 `json:"marginBottom"`
	MarginLeft        *float64 `json:"marginLeft"`
	MarginRight       *float64 `json:"marginRight"`

	WaitLifecycleEvent *pdfrender.WaitLifecycleEvent `json:"waitLifecycleEvent"`
}

// BuildPDFRenderConfig build pdfrender.Config from PDFParams.
func (params *PDFParams) BuildPDFRenderConfig() *pdfrender.Config {
	if params == nil {
		return nil
	}

	return &pdfrender.Config{
		Landscape:          params.Landscape,
		PrintBackground:    params.PrintBackground,
		Scale:              params.Scale,
		PaperWidth:         params.PaperWidth,
		PaperHeight:        params.PaperHeight,
		MarginTop:          params.MarginTop,
		MarginBottom:       params.MarginBottom,
		MarginLeft:         params.MarginLeft,
		MarginRight:        params.MarginRight,
		PreferCSSPageSize:  params.PreferCSSPageSize,
		WaitLifecycleEvent: params.WaitLifecycleEvent,
	}
}

// Params definition.
type Params struct {
	PDF PDFParams `json:"pdf"`
}

// HTML2PDFTemplate wrap TemplateInstance with Schema.
type HTML2PDFTemplate struct {
	Name      templates.Name
	Body      HTMLTemplate
	Header    HTMLTemplate
	Footer    HTMLTemplate
	Params    *Params
	Validator templates.Validator
	Assets    templates.Assets
	Examples  templates.Examples
	PdfRender pdfrender.PDFRenderer
	Tracer    trace.Tracer
	Scheduler futures.Scheduler[futures.Result[[]byte]]
}

// GetName return template name.
func (template *HTML2PDFTemplate) GetName() templates.Name {
	return template.Name
}

// GetExamples return template examples.
func (template *HTML2PDFTemplate) GetExamples() templates.Examples {
	return template.Examples
}

// GetValidator return template validator.
func (template *HTML2PDFTemplate) GetValidator() templates.Validator {
	return template.Validator
}

// GetAssets return template assets.
func (template *HTML2PDFTemplate) GetAssets() templates.Assets {
	return template.Assets
}

func (template *HTML2PDFTemplate) renderSpanName() string {
	return fmt.Sprintf("template-html2pdf/%s.render()", template.Name)
}

func (template *HTML2PDFTemplate) scheduleRenderSpanName() string {
	return fmt.Sprintf("template-html2pdf/%s.scheduleRender()", template.Name)
}

// Render template with data.
func (template *HTML2PDFTemplate) Render(
	ctx context.Context,
	data templates.Data,
) templates.RenderFuture {
	ctx, span := template.Tracer.Start(ctx, template.scheduleRenderSpanName())
	defer span.End()

	return template.Scheduler.Schedule(futures.WrapResultFunc(func() ([]byte, error) {
		return template.renderExecution(ctx, data)
	}))
}

func (template *HTML2PDFTemplate) renderExecution(
	ctx context.Context,
	data templates.Data,
) ([]byte, error) {
	ctx, span := template.Tracer.Start(ctx, template.renderSpanName())
	defer span.End()

	payload, err := template.renderWrapper(ctx, data)
	if err != nil {
		span.SetStatus(codes.Error, err.Error())

		return nil, fmt.Errorf("render: %w", err)
	}

	return payload, nil
}

func (template *HTML2PDFTemplate) renderWrapper(
	ctx context.Context,
	data templates.Data,
) ([]byte, error) {
	document, pdfParams, err := template.renderHTML(ctx, data)
	if err != nil {
		return nil, fmt.Errorf("render: %w", err)
	}

	config := pdfParams.BuildPDFRenderConfig()

	payload, err := template.PdfRender.RenderPDF(ctx, config, *document)
	if err != nil {
		return nil, fmt.Errorf("render pdf: %w", err)
	}

	return payload, nil
}

func (template *HTML2PDFTemplate) renderHTML(
	ctx context.Context,
	data templates.Data,
) (*pdfrender.Document, *PDFParams, error) {
	logger := log.LoggerFromContext(ctx, "render").WithField(log.FieldRenderData, data)

	logger.Debug("Render with data")

	bodyPayload, err := template.RenderBodyHTML(ctx, data)
	if nil != err {
		return nil, nil, fmt.Errorf("render body: %w", err)
	}

	noTemplateError := templates.NewNoTemplateError()

	headerPayload, err := template.RenderHeaderHTML(ctx, data)
	if nil != err && !errors.Is(err, noTemplateError) {
		return nil, nil, fmt.Errorf("render header: %w", err)
	}

	footerPayload, err := template.RenderFooterHTML(ctx, data)
	if nil != err && !errors.Is(err, noTemplateError) {
		return nil, nil, fmt.Errorf("render footer: %w", err)
	}

	var pdfParams *PDFParams
	if template.Params != nil {
		pdfParams = &template.Params.PDF
	}

	return &pdfrender.Document{
		BodyHTML:     bodyPayload,
		HeaderHTML:   headerPayload,
		FooterHTML:   footerPayload,
		TemplateName: pdfrender.TemplateName(template.Name),
	}, pdfParams, nil
}

// RenderBodyHTML implements templates.HTMLPreviewer.
func (template *HTML2PDFTemplate) RenderBodyHTML(
	ctx context.Context,
	data templates.Data,
) (string, error) {
	_, span := template.Tracer.Start(ctx, "render-html/body")
	defer span.End()

	html, err := template.Body.Render(data)
	if nil != err {
		span.SetStatus(codes.Error, err.Error())

		return "", fmt.Errorf("render body: %w", err)
	}

	return html, nil
}

// RenderHeaderHTML implements templates.HTMLPreviewer.
func (template *HTML2PDFTemplate) RenderHeaderHTML(
	ctx context.Context,
	data templates.Data,
) (*string, error) {
	if nil == template.Header {
		return nil, templates.NewNoTemplateError()
	}

	_, span := template.Tracer.Start(ctx, "render-html/header")
	defer span.End()

	html, err := template.Header.Render(data)
	if nil != err {
		span.SetStatus(codes.Error, err.Error())

		return nil, fmt.Errorf("render header: %w", err)
	}

	return &html, nil
}

// RenderFooterHTML implements templates.HTMLPreviewer.
func (template *HTML2PDFTemplate) RenderFooterHTML(
	ctx context.Context,
	data templates.Data,
) (*string, error) {
	if nil == template.Footer {
		return nil, templates.NewNoTemplateError()
	}

	_, span := template.Tracer.Start(ctx, "render-html/footer")
	defer span.End()

	html, err := template.Footer.Render(data)
	if nil != err {
		span.SetStatus(codes.Error, err.Error())

		return nil, fmt.Errorf("render footer: %w", err)
	}

	return &html, nil
}
