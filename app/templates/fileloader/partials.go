package fileloader

import (
	"context"
	"fmt"
	"os"
	"path"
	"path/filepath"
	"strings"

	"gitlab.com/c0va23/pdf-server/app/log"
)

const partialsDirName = "partials"

// PartialPreprocessor interface.
type PartialPreprocessor interface {
	Extensions() string
	PreprocessTemplate(ctx context.Context, data []byte) ([]byte, error)
}

// PartialsLoader interface.
type PartialsLoader interface {
	LoadPartials(
		ctx context.Context,
		templateDirName string,
		templateExtension string,
	) (map[string][]byte, error)
}

// DirPartialsLoader load partials from directory.
type DirPartialsLoader struct {
	processors map[string]PartialPreprocessor
}

// OrderedPartialProcessors list of processors in order of execution.
type OrderedPartialProcessors []PartialPreprocessor

// NewOrderedPartialProcessors create new OrderedPartialProcessors.
func NewOrderedPartialProcessors(
	markdownPartialPreprocessor *MarkdownPartialPreprocessor,
) OrderedPartialProcessors {
	return []PartialPreprocessor{
		markdownPartialPreprocessor,
	}
}

// NewDirPartialsLoader create new DirPartialsLoader.
func NewDirPartialsLoader(
	processorsList OrderedPartialProcessors,
) *DirPartialsLoader {
	processorsMap := make(map[string]PartialPreprocessor)

	for _, processor := range processorsList {
		processorsMap[processor.Extensions()] = processor
	}

	return &DirPartialsLoader{
		processors: processorsMap,
	}
}

// LoadPartials load partials from directory.
func (partialsLoader *DirPartialsLoader) LoadPartials(
	ctx context.Context,
	templateDirName string,
	templateExtension string,
) (map[string][]byte, error) {
	logger := log.LoggerFromContext(ctx, "partials_loader")

	partialsDirPath := path.Join(templateDirName, partialsDirName)

	rootPartialsGlob := path.Join(partialsDirPath, "*"+templateExtension+"*")

	rootFilePaths, err := filepath.Glob(rootPartialsGlob)
	if err != nil {
		return nil, fmt.Errorf("list root files: %w", err)
	}

	innerPartialsGlob := path.Join(partialsDirPath, "**/*"+templateExtension+"*")

	innerFilePaths, err := filepath.Glob(innerPartialsGlob)
	if err != nil {
		return nil, fmt.Errorf("list inner files: %w", err)
	}

	filePaths := make([]string, 0, len(rootFilePaths)+len(innerFilePaths))
	filePaths = append(filePaths, rootFilePaths...)
	filePaths = append(filePaths, innerFilePaths...)

	partials := make(map[string][]byte, len(filePaths))

	for _, filePath := range filePaths {
		name, partial, err := partialsLoader.loadPartial(
			ctx,
			partialsDirPath,
			filePath,
			templateExtension,
		)
		if err != nil {
			return nil, fmt.Errorf("load %s partial (%s): %w", filePath, templateExtension, err)
		}

		logger.WithField("partial_name", name).Debug("Partial is loaded")

		partials[name] = partial
	}

	return partials, nil
}

func (partialsLoader *DirPartialsLoader) loadPartial(
	ctx context.Context,
	includesPath string,
	filePath string,
	templateExtension string,
) (name string, partial []byte, err error) {
	payload, err := os.ReadFile(filePath)
	if err != nil {
		return "", nil, fmt.Errorf("read file: %w", err)
	}

	name, err = filepath.Rel(includesPath, filePath)
	if err != nil {
		return "", nil, fmt.Errorf("get relative path: %w", err)
	}

	for {
		extension := filepath.Ext(name)
		name = strings.TrimSuffix(name, extension)

		if extension == templateExtension {
			break
		}

		payload, err = partialsLoader.processPayload(
			ctx,
			payload,
			extension,
		)
		if err != nil {
			return "", nil, fmt.Errorf("process payload of file %s: %w", filePath, err)
		}
	}

	return name, payload, nil
}

func (partialsLoader *DirPartialsLoader) processPayload(
	ctx context.Context,
	payload []byte,
	extension string,
) ([]byte, error) {
	processor, found := partialsLoader.processors[extension]
	if !found {
		return nil, &UnknownPartialExtensionError{
			extension: extension,
		}
	}

	payload, err := processor.PreprocessTemplate(ctx, payload)
	if err != nil {
		return nil, fmt.Errorf("preprocess partial: %w", err)
	}

	return payload, nil
}

// UnknownPartialExtensionError is an error for unknown partial extension.
type UnknownPartialExtensionError struct {
	extension string
}

// Error returns error message.
func (err *UnknownPartialExtensionError) Error() string {
	return "unknown partial extension " + err.extension
}
