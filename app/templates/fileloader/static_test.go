package fileloader_test

import (
	"testing"

	"gitlab.com/c0va23/pdf-server/app/log"
	"gitlab.com/c0va23/pdf-server/app/templates/fileloader"

	templatesMocks "gitlab.com/c0va23/pdf-server/mocks/templates"
)

func TestStaticTemplateInstanceLoader_LoadTemplateInstance(t *testing.T) {
	t.Parallel()

	logger := log.NewLogger(log.LevelFatal, log.TextFormat)

	bodyPartName := "template"

	templateLoader := new(fileloader.StaticTemplateInstanceLoader)

	assetsMock := new(templatesMocks.Assets)

	testCases := []LoadTemplateInstanceTestCase{
		{
			name:            "when template found",
			templateDirPath: "fixtures/templates/static_template",
			expectedError:   nil,
		},
		{
			name:            "template not found",
			templateDirPath: "fixtures/templates/not_found",
			expectedError: fileloader.NewTemplateFileNotFoundError(
				templateLoader.TemplateType(),
				"fixtures/templates/not_found/template.html",
			),
		},
	}

	loader := func(templatePath string) (fileloader.HTMLTemplate, error) {
		return templateLoader.LoadTemplateInstance(templatePath, bodyPartName, logger, assetsMock)
	}

	testLoadTemplateInstance(t, loader, testCases)
}

func TestStaticTemplate_Render(t *testing.T) {
	t.Parallel()

	logger := log.NewLogger(log.LevelFatal, log.TextFormat)
	payload := "<html><body><h1>Static template</h1></body></html>\n"

	staticTemplate := fileloader.NewStaticTemplate(payload, logger)

	testCases := []RenderTemplateTestCase{
		{
			name:            "when template found",
			templateData:    struct{}{},
			expectedPayload: payload,
			expectedError:   "",
		},
	}

	testRenderTemplateTestCases(t, staticTemplate, testCases)
}
