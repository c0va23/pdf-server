package composition

import (
	"context"
	"fmt"
	"os"
	"path/filepath"

	"github.com/itchyny/gojq"
	"gopkg.in/yaml.v3"

	templatesPkg "gitlab.com/c0va23/pdf-server/app/templates"
)

const (
	yamlPlannerFileName = "composition.yaml"
)

// YAML Planner Definition

type yamlPlannerDefinition struct {
	Templates []yamlPlannerDefinitionStatement `yaml:"templates"`
}

type yamlPlannerDefinitionStatement struct {
	TemplateName string                           `yaml:"templateName"`
	Condition    *yamlPlannerDefinitionExpression `yaml:"if"`
	Transform    *yamlPlannerDefinitionExpression `yaml:"transform"`
}

type yamlPlannerDefinitionExpression struct {
	Jq *string `yaml:"jq"`
}

func newYamlPlanerDefinition() yamlPlannerDefinition {
	return yamlPlannerDefinition{
		Templates: []yamlPlannerDefinitionStatement{},
	}
}

// YAML Planner Loader

type yamlPlannerLoader struct {
	store templatesPkg.Store
}

// NewYamlPlannerLoader construct yaml planner loader.
func NewYamlPlannerLoader(store templatesPkg.Store) *yamlPlannerLoader {
	return &yamlPlannerLoader{
		store: store,
	}
}

// LoaderType return loader type.
func (*yamlPlannerLoader) LoaderType() string {
	return "yamlPlannerLoader"
}

// LoadPlanner yaml planner loader.
func (loader *yamlPlannerLoader) LoadPlanner(
	templateDirPath string,
) (CompositePlanner, error) {
	definition, err := loader.loadDefinition(templateDirPath)
	if err != nil {
		return nil, fmt.Errorf("load definition: %w", err)
	}

	items, err := loader.parseTemplateDefinitionList(definition.Templates)
	if err != nil {
		return nil, fmt.Errorf("parse template definition list: %w", err)
	}

	return &yamlPlanner{
		items: items,
	}, nil
}

func (loader *yamlPlannerLoader) loadDefinition(
	templateDirPath string,
) (yamlPlannerDefinition, error) {
	templatePath := filepath.Join(templateDirPath, yamlPlannerFileName)

	file, err := os.Open(templatePath)
	if err != nil {
		if os.IsNotExist(err) {
			return yamlPlannerDefinition{}, &PlannerNotFoundError{
				plannerType: loader.LoaderType(),
				path:        templatePath,
				inner:       err,
			}
		}
	}

	definition := newYamlPlanerDefinition()

	err = yaml.NewDecoder(file).Decode(&definition)
	if err != nil {
		return yamlPlannerDefinition{}, fmt.Errorf("decode file: %w", err)
	}

	return definition, nil
}

func (loader *yamlPlannerLoader) parseTemplateDefinitionList(
	templateDefinitions []yamlPlannerDefinitionStatement,
) ([]yamlPlannerStatement, error) {
	items := make([]yamlPlannerStatement, 0, len(templateDefinitions))

	for _, templateDefinition := range templateDefinitions {
		item, err := loader.parseTemplateDefinition(templateDefinition)
		if err != nil {
			return nil, fmt.Errorf("parse template definition: %s: %w", templateDefinition.TemplateName, err)
		}

		items = append(items, item)
	}

	return items, nil
}

func (loader *yamlPlannerLoader) parseTemplateDefinition(
	templateDefinition yamlPlannerDefinitionStatement,
) (yamlPlannerStatement, error) {
	template, err := loader.store.Fetch(templatesPkg.Name(templateDefinition.TemplateName))
	if err != nil {
		return yamlPlannerStatement{},
			fmt.Errorf("get template %s: %w", templateDefinition.TemplateName, err)
	}

	condition, err := loader.parseTemplateConditionDefinition(templateDefinition.Condition)
	if err != nil {
		return yamlPlannerStatement{},
			fmt.Errorf("parse condition (.if): %w", err)
	}

	transform, err := loader.parseTemplateConditionDefinition(templateDefinition.Transform)
	if err != nil {
		return yamlPlannerStatement{},
			fmt.Errorf("parse condition (.if): %w", err)
	}

	return yamlPlannerStatement{
		template:  template,
		condition: condition,
		transform: transform,
	}, nil
}

func (*yamlPlannerLoader) parseTemplateConditionDefinition(
	conditionDefinition *yamlPlannerDefinitionExpression,
) (*yamlPlannerExpression, error) {
	if conditionDefinition == nil {
		return nil, nil
	}

	if conditionDefinition.Jq == nil {
		return nil, &parseCompositionError{
			message: "jq is required",
		}
	}

	jqQuery, err := gojq.Parse(*conditionDefinition.Jq)
	if err != nil {
		return nil, fmt.Errorf("parse jq: %w", err)
	}

	return &yamlPlannerExpression{
		jqQuery: jqQuery,
	}, nil
}

// YAML Planner

type yamlPlanner struct {
	items []yamlPlannerStatement
}

// PlannerType return planner type.
func (*yamlPlanner) PlannerType() string {
	return "yamlPlanner"
}

// BuildPlan build render plan.
func (planner *yamlPlanner) BuildPlan(
	ctx context.Context,
	data templatesPkg.Data,
) ([]templatesPkg.RenderTask, error) {
	items := make([]templatesPkg.RenderTask, 0, len(planner.items))

	for index, plannerStatement := range planner.items {
		task, err := plannerStatement.buildTask(ctx, data)
		if err != nil {
			return nil, fmt.Errorf(
				"build task (%d, %s): %w",
				index,
				plannerStatement.template.GetName(),
				err,
			)
		}

		if task == nil {
			continue
		}

		items = append(items, *task)
	}

	return items, nil
}

// YAML Planner Item

type yamlPlannerStatement struct {
	template  templatesPkg.Template
	condition *yamlPlannerExpression
	transform *yamlPlannerExpression
}

func (plannerStatement *yamlPlannerStatement) buildTask(
	ctx context.Context,
	data templatesPkg.Data,
) (*templatesPkg.RenderTask, error) {
	conditionResult, err := plannerStatement.evalCondition(ctx, data)
	if err != nil {
		return nil, fmt.Errorf(
			"eval condition: %w",
			err,
		)
	}

	if !conditionResult {
		return nil, nil
	}

	transformedData, err := plannerStatement.transformedData(ctx, data)
	if err != nil {
		return nil, fmt.Errorf(
			"eval transform: %w",
			err,
		)
	}

	return &templatesPkg.RenderTask{
		TemplateName: string(plannerStatement.template.GetName()),
		Template:     plannerStatement.template,
		Data:         transformedData,
	}, nil
}

func (plannerStatement *yamlPlannerStatement) evalCondition(
	ctx context.Context,
	data templatesPkg.Data,
) (bool, error) {
	if plannerStatement.condition == nil {
		return true, nil
	}

	conditionResult, err := plannerStatement.condition.evalAsBool(ctx, data)
	if err != nil {
		return true, fmt.Errorf("condition: %w", err)
	}

	return conditionResult, nil
}

func (plannerStatement *yamlPlannerStatement) transformedData(
	ctx context.Context,
	data templatesPkg.Data,
) (templatesPkg.Data, error) {
	if plannerStatement.transform == nil {
		return data, nil
	}

	transformedData, err := plannerStatement.transform.evalAsData(ctx, data)
	if err != nil {
		return nil, fmt.Errorf(
			"transform data: %w",
			err,
		)
	}

	return transformedData, nil
}

// YAML Planner Item Expression

type yamlPlannerExpression struct {
	jqQuery *gojq.Query
}

func (expression yamlPlannerExpression) eval(
	ctx context.Context,
	data templatesPkg.Data,
) (any, error) {
	if expression.jqQuery != nil {
		result, err := expression.evalJqQuery(ctx, expression.jqQuery, data)
		if err != nil {
			return nil, fmt.Errorf("jq: %w", err)
		}

		return result, nil
	}

	panic("unreachable")
}

func (*yamlPlannerExpression) evalJqQuery(
	ctx context.Context,
	jqQuery *gojq.Query,
	data templatesPkg.Data,
) (any, error) {
	result, found := jqQuery.RunWithContext(ctx, map[string]any(data)).Next()
	if !found {
		return nil, &compositionEvalError{message: "jq return no data"}
	}

	return result, nil
}

func (expression yamlPlannerExpression) evalAsBool(
	ctx context.Context,
	data templatesPkg.Data,
) (bool, error) {
	result, err := expression.eval(ctx, data)
	if err != nil {
		return false, fmt.Errorf("eval: %w", err)
	}

	resultBool, resultIsBool := result.(bool)
	if !resultIsBool {
		return false, &compositionEvalError{
			message: fmt.Sprintf("result is not bool (it is %T)", result),
		}
	}

	return resultBool, nil
}

func (expression yamlPlannerExpression) evalAsData(
	ctx context.Context,
	data templatesPkg.Data,
) (templatesPkg.Data, error) {
	result, err := expression.eval(ctx, data)
	if err != nil {
		return nil, fmt.Errorf("eval: %w", err)
	}

	resultData, resultIsData := result.(map[string]any)

	if !resultIsData {
		return nil, &compositionEvalError{message: "result is not data"}
	}

	return templatesPkg.Data(resultData), nil
}

// Errors

type compositionEvalError struct {
	message string
}

func (err *compositionEvalError) Error() string {
	return "composition expression eval: " + err.message
}

type parseCompositionError struct {
	message string
}

func (err *parseCompositionError) Error() string {
	return "parse composition: " + err.message
}
