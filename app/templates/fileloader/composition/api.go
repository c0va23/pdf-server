package composition

import (
	"context"
	"fmt"

	templatesPkg "gitlab.com/c0va23/pdf-server/app/templates"
)

// CompositePlanner build plan for composition template rendering.
type CompositePlanner interface {
	BuildPlan(context.Context, templatesPkg.Data) ([]templatesPkg.RenderTask, error)

	PlannerType() string
}

// PlannerNotFoundError returned if planner file not found.
type PlannerNotFoundError struct {
	plannerType string
	path        string
	inner       error
}

func (err *PlannerNotFoundError) Error() string {
	return fmt.Sprintf(
		"planner %s not found by path %s: %s",
		err.plannerType,
		err.path,
		err.inner.Error(),
	)
}

func (err *PlannerNotFoundError) Unwrap() error {
	return err.inner
}
