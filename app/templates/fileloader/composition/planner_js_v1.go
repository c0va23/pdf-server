package composition

import (
	"context"
	"errors"
	"fmt"
	"os"
	"path"
	"sync"

	"github.com/dop251/goja"
	"github.com/dop251/goja_nodejs/console"
	"github.com/dop251/goja_nodejs/require"

	"gitlab.com/c0va23/pdf-server/app/log"
	templatesPkg "gitlab.com/c0va23/pdf-server/app/templates"
)

// JavaScript Planner Loader

type jsPlannerLoadError struct {
	message string
}

func (err *jsPlannerLoadError) Error() string {
	return "js planner error: %s" + err.message
}

const mainJsPath = "composition.js"

type jsPlannerLoader struct {
	templatesStore templatesPkg.Store
}

// NewJsPlannerLoader constructs JavaScript planner loader.
func NewJsPlannerLoader(
	templatesStore templatesPkg.Store,
) *jsPlannerLoader {
	return &jsPlannerLoader{
		templatesStore: templatesStore,
	}
}

// LoadPlanner loads JavaScript planer.
func (loader *jsPlannerLoader) LoadPlanner(
	templateDirPath string,
) (CompositePlanner, error) {
	scriptPath := path.Join(templateDirPath, mainJsPath)

	scriptSrc, err := os.ReadFile(scriptPath)
	if err != nil {
		return nil, fmt.Errorf("read js planner %s file: %w", scriptPath, err)
	}

	runtime := goja.New()

	requireRegistry := require.NewRegistryWithLoader(
		loader.buildSourceLoader(templateDirPath),
	)

	consolePrinter, err := loader.configureConsole(requireRegistry, runtime)
	if err != nil {
		return nil, fmt.Errorf("configure console: %w", err)
	}

	exportsValue, err := runtime.RunScript(mainJsPath, string(scriptSrc))
	if err != nil {
		return nil, fmt.Errorf("parser js planner %s script: %w", scriptPath, err)
	}

	exportsObject := exportsValue.ToObject(runtime)

	if exportsObject == nil {
		return nil, &jsPlannerLoadError{
			message: fmt.Sprintf(
				"Script export wrong value type. Expected object, got %T",
				exportsValue.ExportType(),
			),
		}
	}

	buildPlanValue := exportsObject.Get("buildPlan")

	buildPlan, isFunction := buildPlanValue.Export().(func(goja.FunctionCall) goja.Value)
	if !isFunction {
		return nil, &jsPlannerLoadError{
			message: fmt.Sprintf(
				"buildPlan should be function. But got %t",
				buildPlanValue.ExportType(),
			),
		}
	}

	return &jsPlanner{
		templatesStore: loader.templatesStore,
		runtime:        runtime,
		object:         exportsObject,
		buildPlanCall:  buildPlan,
		mutex:          new(sync.Mutex),
		loggerWrapper:  consolePrinter,
	}, nil
}

func (*jsPlannerLoader) configureConsole(
	requireRegistry *require.Registry,
	runtime *goja.Runtime,
) (*jsLoggerConsolePrinter, error) {
	consolePrinter := newNoopJsLoggerConsolePrinter()

	requireRegistry.RegisterNativeModule(
		console.ModuleName,
		console.RequireWithPrinter(consolePrinter),
	)

	requireRegistry.Enable(runtime)

	// Define console as global variable
	_, err := runtime.RunString(
		fmt.Sprintf(
			"const %s = require('%s')",
			console.ModuleName,
			console.ModuleName,
		),
	)
	if err != nil {
		return nil, fmt.Errorf("define console: %w", err)
	}

	return consolePrinter, nil
}

// LoaderType returns type of JavaScript planner loader.
func (*jsPlannerLoader) LoaderType() string {
	return "js-planner"
}

func (*jsPlannerLoader) buildSourceLoader(
	templateDirPath string,
) func(sourcePath string) ([]byte, error) {
	return func(sourcePath string) ([]byte, error) {
		fullPath := path.Join(templateDirPath, sourcePath)

		sourceBytes, err := os.ReadFile(fullPath)
		if err != nil {
			return nil, fmt.Errorf("load source at %s: %w", fullPath, err)
		}

		return sourceBytes, nil
	}
}

// JavaScript planner

const (
	templateNameField = "templateName"
	dataField         = "data"
)

type jsFieldNotFoundError struct {
	fieldName string
}

func (err *jsFieldNotFoundError) Error() string {
	return "field not found %s" + err.fieldName
}

type jsWrongFieldTypeError[E any] struct {
	value any
}

func (err *jsWrongFieldTypeError[E]) Error() string {
	var expected E

	return fmt.Sprintf(
		"wrong field type (expected %T, actual %T)",
		expected,
		err.value,
	)
}

type jsPlanner struct {
	templatesStore templatesPkg.Store
	runtime        *goja.Runtime
	object         goja.Value
	buildPlanCall  func(goja.FunctionCall) goja.Value
	mutex          *sync.Mutex
	loggerWrapper  *jsLoggerConsolePrinter
}

// PlannerType returns type of JavaScript planner.
func (*jsPlanner) PlannerType() string {
	return "js-planner"
}

// BuildPlan by executing JavaScript composition.
func (planner *jsPlanner) BuildPlan(
	ctx context.Context,
	data templatesPkg.Data,
) ([]templatesPkg.RenderTask, error) {
	planner.mutex.Lock()
	defer planner.mutex.Unlock()

	cleanLoggerContext := planner.loggerWrapper.withinContext(ctx)
	defer cleanLoggerContext()

	dataValue := planner.runtime.ToValue(map[string]any(data))

	resultValue := planner.buildPlanCall(goja.FunctionCall{
		This: planner.object,
		Arguments: []goja.Value{
			dataValue,
		},
	})

	result := resultValue.Export()

	taskValues, err := jsCastField[[]any](result)
	if err != nil {
		return nil, fmt.Errorf("cast plan call result: %w", err)
	}

	tasks := make([]templatesPkg.RenderTask, 0, len(taskValues))

	for index, taskValue := range taskValues {
		task, err := planner.parseRenderTask(taskValue)
		if err != nil {
			return nil, fmt.Errorf("parse task %d: %w", index, err)
		}

		tasks = append(tasks, task)
	}

	return tasks, nil
}

func (planner *jsPlanner) parseRenderTask(
	taskValue any,
) (templatesPkg.RenderTask, error) {
	var emptyTask templatesPkg.RenderTask

	taskMap, isTaskMap := taskValue.(map[string]any)
	if !isTaskMap {
		return emptyTask, &jsWrongFieldTypeError[map[string]any]{
			value: taskValue,
		}
	}

	templateName, err := getAndCastField[string](taskMap, templateNameField)
	if err != nil {
		return emptyTask, fmt.Errorf("parse template name: %w", err)
	}

	// missed data field is allowed
	taskData, err := getAndCastField[map[string]any](taskMap, dataField)
	if noField := new(jsFieldNotFoundError); err != nil && !errors.As(err, &noField) {
		return emptyTask, fmt.Errorf("parse data (%s): %w", templateName, err)
	}

	template, err := planner.templatesStore.Fetch(templatesPkg.Name(templateName))
	if err != nil {
		return emptyTask, fmt.Errorf("fetch template (%s): %w", templateName, err)
	}

	return templatesPkg.RenderTask{
		TemplateName: templateName,
		Template:     template,
		Data:         taskData,
	}, nil
}

func getAndCastField[T any](fields map[string]any, fieldName string) (T, error) {
	var emptyValue T

	rawValue, found := fields[fieldName]
	if !found {
		return emptyValue, &jsFieldNotFoundError{
			fieldName: fieldName,
		}
	}

	value, err := jsCastField[T](rawValue)
	if err != nil {
		return emptyValue, fmt.Errorf("cast: %w", err)
	}

	return value, nil
}

func jsCastField[T any](rawValue any) (T, error) {
	var emptyValue T

	value, isCasted := rawValue.(T)
	if !isCasted {
		return emptyValue, &jsWrongFieldTypeError[T]{
			value: value,
		}
	}

	return value, nil
}

// Console Printer

type jsLoggerConsolePrinter struct {
	logger log.Logger
}

func newNoopJsLoggerConsolePrinter() *jsLoggerConsolePrinter {
	return &jsLoggerConsolePrinter{
		logger: nil,
	}
}

// Log write info message to logger.
func (printer *jsLoggerConsolePrinter) Log(message string) {
	if printer.logger == nil {
		return
	}

	printer.logger.Info(message)
}

// Warn write warn message to logger.
func (printer *jsLoggerConsolePrinter) Warn(message string) {
	if printer.logger == nil {
		return
	}

	printer.logger.Warn(message)
}

// Error write error message to logger.
func (printer *jsLoggerConsolePrinter) Error(message string) {
	if printer.logger == nil {
		return
	}

	printer.logger.Error(message)
}

const jsLoggerKey = "jsLogger"

func (printer *jsLoggerConsolePrinter) withinContext(ctx context.Context) func() {
	printer.logger = log.LoggerFromContext(ctx, jsLoggerKey)

	return func() {
		printer.logger = nil
	}
}
