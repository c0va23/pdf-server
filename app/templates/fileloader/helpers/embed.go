package helpers

import (
	"fmt"
	"io"

	"github.com/cristalhq/base64"

	"gitlab.com/c0va23/pdf-server/app/templates"
)

type base64Encoder interface {
	EncodeToString(src []byte) (encoded string)
}

// EmbedHelpers struct provide embed file content helpers.
type EmbedHelpers struct {
	assets  templates.Assets
	encoder base64Encoder
}

// NewEmbedHelpers build AssetsHelpers instance.
func NewEmbedHelpers(assets templates.Assets) *EmbedHelpers {
	encoder := base64.RawStdEncoding

	return &EmbedHelpers{
		assets,
		encoder,
	}
}

// EmbedText returns text content of file.
func (helpers *EmbedHelpers) EmbedText(path string) string {
	content, err := helpers.getFileContent(path)
	if err != nil {
		panic(fmt.Errorf("embed text: %w", err))
	}

	return string(content)
}

// EmbedBase64Encoded returns base64 encoded content of file.
func (helpers *EmbedHelpers) EmbedBase64Encoded(path string) string {
	rawContent, err := helpers.getFileContent(path)
	if err != nil {
		panic(fmt.Errorf("embed base64 encoded: %w", err))
	}

	encodedContent := helpers.encoder.EncodeToString(rawContent)

	return encodedContent
}

func (helpers *EmbedHelpers) getFileContent(path string) ([]byte, error) {
	file, err := helpers.assets.Open(path)
	if err != nil {
		return nil, fmt.Errorf("open file %s: %w", path, err)
	}

	content, err := io.ReadAll(file)
	if err != nil {
		return nil, fmt.Errorf("read file %s: %w", path, err)
	}

	return content, nil
}
