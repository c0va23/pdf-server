package fileloader_test

import (
	"errors"
	"testing"

	"github.com/stretchr/testify/assert"

	"gitlab.com/c0va23/pdf-server/app/templates/fileloader"
)

const (
	staticTemplatePath = "fixtures/templates/static_template"
)

const bodyPartName = "template"

type LoadTemplateInstanceTestCase struct {
	name            string
	templateDirPath string
	expectedError   error
}

func testLoadTemplateInstance(
	t *testing.T,
	loader func(templatePath string) (fileloader.HTMLTemplate, error),
	testCases []LoadTemplateInstanceTestCase,
) {
	t.Helper()

	for _, testCase := range testCases {
		t.Run(testCase.name, func(t *testing.T) {
			t.Parallel()

			actualTemplate, actualErr := loader(testCase.templateDirPath)

			// result template not subject to value check
			if testCase.expectedError != nil {
				assert.Nil(t, actualTemplate)
			} else {
				assert.NotNil(t, actualTemplate)
			}

			assert.ErrorIs(t, actualErr, testCase.expectedError)
		})
	}
}

func assertUnwrappedError(t *testing.T, err error, targetMessage string) {
	t.Helper()

	for err := err; err != nil; err = errors.Unwrap(err) {
		if err.Error() == targetMessage {
			return
		}
	}

	t.Errorf("error %s not contains %s", err, targetMessage)
}

type RenderTemplateTestCase struct {
	name            string
	templateData    interface{}
	expectedPayload string
	expectedError   string
}

type TemplateRender interface {
	Render(data interface{}) (payload string, err error)
}

func testRenderTemplateTestCases(
	t *testing.T,
	templateRender TemplateRender,
	testCases []RenderTemplateTestCase,
) {
	t.Helper()

	for _, testCase := range testCases {
		t.Run(testCase.name, func(t *testing.T) {
			t.Parallel()

			actualPayload, actualError := templateRender.Render(testCase.templateData)

			assert.Equal(t, testCase.expectedPayload, actualPayload)

			if testCase.expectedError == "" {
				assert.NoError(t, actualError)
			} else {
				assertUnwrappedError(t, actualError, testCase.expectedError)
			}
		})
	}
}
