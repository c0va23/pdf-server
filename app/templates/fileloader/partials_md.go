package fileloader

import (
	"context"

	"github.com/gomarkdown/markdown"
	markdownHtml "github.com/gomarkdown/markdown/html"
	markdownParser "github.com/gomarkdown/markdown/parser"

	"gitlab.com/c0va23/pdf-server/app/log"
)

// MarkdownPartialPreprocessor pre-processor markdown partials.
type MarkdownPartialPreprocessor struct {
	markdownExtensions markdownParser.Extensions
}

// NewMarkdownPartialPreprocessor create new MarkdownPartialPreprocessor.
func NewMarkdownPartialPreprocessor() *MarkdownPartialPreprocessor {
	return &MarkdownPartialPreprocessor{
		markdownExtensions: markdownParser.NoIntraEmphasis |
			markdownParser.Tables |
			markdownParser.Strikethrough |
			markdownParser.SpaceHeadings |
			markdownParser.HardLineBreak |
			markdownParser.NonBlockingSpace |
			markdownParser.DefinitionLists |
			markdownParser.OrderedListStart,
	}
}

// Extensions return extensions for markdown partials.
func (*MarkdownPartialPreprocessor) Extensions() string {
	return ".md"
}

// PreprocessTemplate preprocess markdown partials.
func (processor *MarkdownPartialPreprocessor) PreprocessTemplate(
	ctx context.Context,
	rawPayload []byte,
) ([]byte, error) {
	logger := log.LoggerFromContext(ctx, "markdown_partial_preprocessor")
	logger.Debug("Process markdown")

	return processor.PreprocessString(rawPayload)
}

// PreprocessString preprocess markdown partials.
func (processor *MarkdownPartialPreprocessor) PreprocessString(
	rawPayload []byte,
) ([]byte, error) {
	markdownParer := markdownParser.NewWithExtensions(processor.markdownExtensions)
	markdownRender := markdownHtml.NewRenderer(markdownHtml.RendererOptions{
		Flags: markdownHtml.CommonFlags,
	})

	return markdown.ToHTML(
		rawPayload,
		markdownParer,
		markdownRender,
	), nil
}
