package fileloader

import (
	"context"
	"fmt"
	"os"
	"path"

	handlebars "github.com/mailgun/raymond/v2"

	"gitlab.com/c0va23/pdf-server/app/log"
	"gitlab.com/c0va23/pdf-server/app/templates"
	"gitlab.com/c0va23/pdf-server/app/templates/fileloader/helpers"
)

type unknownHandlebarsHelperError struct {
	msg string
}

func (e *unknownHandlebarsHelperError) Error() string {
	return e.msg
}

const (
	handlebarsTemplateExtension = ".handlebars"
)

// HandlebarsTemplate instance.
type HandlebarsTemplate struct {
	template *handlebars.Template
}

// Render handlebars template.
func (template *HandlebarsTemplate) Render(data interface{}) (string, error) {
	html, err := template.template.Exec(data)
	if err != nil {
		return "", fmt.Errorf("render handlebars template: %w", err)
	}

	return html, nil
}

// HandlebarsTemplateInstanceLoader load handlebars template.
type HandlebarsTemplateInstanceLoader struct {
	partialsLoader  PartialsLoader
	markdownHelpers MarkdownHelpers
}

// NewHandlebarsTemplateInstanceLoader constructor.
func NewHandlebarsTemplateInstanceLoader(
	partialsLoader PartialsLoader,
	markdownHelpers MarkdownHelpers,
) *HandlebarsTemplateInstanceLoader {
	return &HandlebarsTemplateInstanceLoader{
		partialsLoader:  partialsLoader,
		markdownHelpers: markdownHelpers,
	}
}

// LoadTemplateInstance load template instance.
func (loader *HandlebarsTemplateInstanceLoader) LoadTemplateInstance(
	templateDir string,
	partName string,
	logger log.Logger,
	assets templates.Assets,
) (HTMLTemplate, error) {
	ctx := log.ContextWithLogger(context.Background(), logger)

	templatePath := path.Join(templateDir, loader.TemplateName(partName))

	template, err := handlebars.ParseFile(templatePath)
	if err != nil {
		if os.IsNotExist(err) {
			return nil, NewTemplateFileNotFoundError(loader.TemplateType(), templatePath)
		}

		return nil, fmt.Errorf("parse handlebars template: %w", err)
	}

	err = loader.registerHelpers(template, assets)
	if err != nil {
		return nil, fmt.Errorf("register helpers: %w", err)
	}

	partials, err := loader.partialsLoader.LoadPartials(ctx, templateDir, handlebarsTemplateExtension)
	if err != nil {
		return nil, fmt.Errorf("load handlebars template: %w", err)
	}

	template.RegisterPartials(loader.buildPartials(partials))

	return &HandlebarsTemplate{
		template: template,
	}, nil
}

func (loader *HandlebarsTemplateInstanceLoader) registerHelpers(
	template *handlebars.Template,
	assets templates.Assets,
) (err error) {
	defer func() {
		panicErr := recover()
		if panicErr == nil {
			return
		}

		switch castedErr := panicErr.(type) {
		case error:
			err = castedErr
		default:
			err = &unknownHandlebarsHelperError{
				msg: fmt.Sprint(panicErr),
			}
		}
	}()

	embedHelpers := helpers.NewEmbedHelpers(assets)

	template.RegisterHelpers(map[string]interface{}{
		"format_time": helpers.MustFormatTime,
		"markdown":    handlebarsHTMLHelper(loader.markdownHelpers.Markdown),
		"equal":       handlebarsEqualWithElse,

		"embed_text":   embedHelpers.EmbedText,
		"embed_base64": embedHelpers.EmbedBase64Encoded,
	})

	return nil
}

// TemplateType return handlebars template type.
func (*HandlebarsTemplateInstanceLoader) TemplateType() string {
	return "handlebars"
}

// TemplateName return handlebars template name.
func (*HandlebarsTemplateInstanceLoader) TemplateName(partName string) string {
	return partName + handlebarsTemplateExtension
}

func (*HandlebarsTemplateInstanceLoader) buildPartials(
	partials map[string][]byte,
) map[string]string {
	stringPartials := make(map[string]string, len(partials))

	for name, partial := range partials {
		stringPartials[name] = string(partial)
	}

	return stringPartials
}

func handlebarsHTMLHelper[T any](
	helper func(params T) string,
) func(params T) handlebars.SafeString {
	return func(params T) handlebars.SafeString {
		return handlebars.SafeString(helper(params))
	}
}

func handlebarsEqualWithElse(a any, b any, options *handlebars.Options) string {
	if handlebars.Str(a) == handlebars.Str(b) {
		return options.Fn()
	}

	return options.Inverse()
}
