package fileloader_test

import (
	"errors"
	"os"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"

	"gitlab.com/c0va23/pdf-server/app/log"
	"gitlab.com/c0va23/pdf-server/app/templates"
	"gitlab.com/c0va23/pdf-server/app/templates/fileloader"

	templatesMocks "gitlab.com/c0va23/pdf-server/mocks/templates"
)

func TestLoadTemplatesMap(t *testing.T) {
	t.Parallel()

	var (
		logger          = log.NewLogger(log.LevelFatal, log.TextFormat)
		templatesLogger = logger.WithField(log.LoggerField, "templates")

		nilTemplatesMap templates.Store

		templateLoaderErr = errors.New("load error")

		templatesMap = templates.MapStore{
			templates.Name("assets_template"):   new(templatesMocks.Template),
			templates.Name("go_template"):       new(templatesMocks.Template),
			templates.Name("mustache_template"): new(templatesMocks.Template),
			templates.Name("static_template"):   new(templatesMocks.Template),
			templates.Name("invalid_schema"):    new(templatesMocks.Template),
			templates.Name("valid_schema"):      new(templatesMocks.Template),
			templates.Name("params_template"):   new(templatesMocks.Template),
			templates.Name("footer_template"):   new(templatesMocks.Template),
			templates.Name("examples_template"): new(templatesMocks.Template),
			templates.Name("invalid_example"):   new(templatesMocks.Template),
		}

		expectedTemplatesStore = make(templates.MapStore, len(templatesMap)+1)
	)

	failTemplateLoaderMockProvider := func(templateLoaderErr error) *templatesMocks.TemplateLoader {
		templateLoaderMock := new(templatesMocks.TemplateLoader)
		templateLoaderMock.EXPECT().LoadTemplate(
			"fixtures/templates",
			"assets_template",
			templatesLogger,
		).Return(nil, templateLoaderErr)

		templateLoaderMock.EXPECT().TemplateType().Return("mock")

		return templateLoaderMock
	}

	successTemplateLoaderMockProvider := func() *templatesMocks.TemplateLoader {
		templateLoaderMock := new(templatesMocks.TemplateLoader)

		for templateName, template := range templatesMap {
			template.(*templatesMocks.Template).EXPECT().GetName().Return(templateName)

			templateLoaderMock.EXPECT().LoadTemplate(
				"fixtures/templates",
				string(templateName),
				templatesLogger,
			).Return(template, nil)
		}

		return templateLoaderMock
	}

	compositeTemplateName := templates.Name("composition:only_static")
	compositeTemplate := new(templatesMocks.Template)
	compositeTemplate.EXPECT().GetName().Return(compositeTemplateName)

	for templateName, template := range templatesMap {
		expectedTemplatesStore[templateName] = template.(*templatesMocks.Template)
	}

	expectedTemplatesStore[compositeTemplateName] = compositeTemplate

	compositionLoader := new(templatesMocks.TemplateLoader)
	compositionLoader.
		EXPECT().
		LoadTemplate("fixtures/compositions", "only_static", mock.Anything).
		Return(compositeTemplate, nil)
	compositionLoader.
		EXPECT().
		TemplateType().
		Return("composite_mock")

	templateLoaderBuilder := new(templatesMocks.TemplateLoaderBuilder)
	templateLoaderBuilder.
		EXPECT().
		BuildTemplateLoader(mock.Anything).
		Return(compositionLoader, nil)

	notExistsConfig := fileloader.Config{
		DirPath:    "not_found",
		ReloadMode: fileloader.TemplateReloadModeNone,
	}

	existsConfig := fileloader.Config{
		DirPath:             "fixtures/templates",
		CompositionsDirPath: "fixtures/compositions",
		ReloadMode:          fileloader.TemplateReloadModeNone,
	}

	type TestCase struct {
		name                 string
		config               fileloader.Config
		templateLoaderMock   *templatesMocks.TemplateLoader
		expectedTemplatesMap templates.Store
		expectedError        error
	}

	testCases := []TestCase{
		{
			name:                 "templates dir not exists",
			config:               notExistsConfig,
			templateLoaderMock:   new(templatesMocks.TemplateLoader),
			expectedTemplatesMap: nilTemplatesMap,
			expectedError:        os.ErrNotExist,
		},
		{
			name:                 "template loader return error",
			config:               existsConfig,
			templateLoaderMock:   failTemplateLoaderMockProvider(templateLoaderErr),
			expectedTemplatesMap: nilTemplatesMap,
			expectedError:        templateLoaderErr,
		},
		{
			name:                 "template loader return template instance",
			config:               existsConfig,
			templateLoaderMock:   successTemplateLoaderMockProvider(),
			expectedTemplatesMap: expectedTemplatesStore,
			expectedError:        nil,
		},
	}

	for _, testCase := range testCases {
		t.Run(testCase.name, func(t *testing.T) {
			t.Parallel()

			defer testCase.templateLoaderMock.AssertExpectations(t)

			templatesStoreProvider := new(templatesMocks.StoreProvider)
			defer templatesStoreProvider.AssertExpectations(t)

			if testCase.expectedError == nil {
				templatesStoreProvider.EXPECT().ProvideTemplatesStore(mock.Anything)
			}

			storeLoader := fileloader.NewStoreLoader(
				testCase.config,
				fileloader.OrderedTemplateLoaders{testCase.templateLoaderMock},
				fileloader.OrderedTemplateLoaderBuilders{templateLoaderBuilder},
				templatesStoreProvider,
				logger,
			)

			actualTemplatesMap, actualErr := fileloader.LoadStore(storeLoader)

			storeIsEqual(t, testCase.expectedTemplatesMap, actualTemplatesMap)

			if testCase.expectedError == nil {
				assert.Nil(t, actualErr)
			} else {
				assert.Error(t, testCase.expectedError, actualErr)
			}
		})
	}
}

func storeIsEqual(t *testing.T, expectedStore templates.Store, actualStore templates.Store) {
	t.Helper()

	if expectedStore == nil {
		if actualStore != nil {
			t.Error("expected nil store")
		}

		return
	}

	if actualStore == nil {
		t.Error("expected not nil store")

		return
	}

	expectedTemplates, err := expectedStore.List()
	if err != nil {
		panic(err)
	}

	actualTemplates, err := actualStore.List()
	if err != nil {
		panic(err)
	}

	assert.ElementsMatch(t, expectedTemplates, actualTemplates)
}
