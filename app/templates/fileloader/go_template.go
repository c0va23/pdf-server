package fileloader

import (
	"bytes"
	"context"
	"fmt"
	templatePkg "html/template"
	"os"
	"path/filepath"

	"gitlab.com/c0va23/pdf-server/app/log"
	"gitlab.com/c0va23/pdf-server/app/templates"
	"gitlab.com/c0va23/pdf-server/app/templates/fileloader/helpers"
)

const (
	goTemplateExtension = ".tmpl"
)

// GoTemplate wrap go template and JSON schema.
type GoTemplate struct {
	template *templatePkg.Template
	logger   log.Logger
}

// NewGoTemplate constructor.
func NewGoTemplate(template *templatePkg.Template, logger log.Logger) *GoTemplate {
	return &GoTemplate{
		template: template,
		logger:   logger,
	}
}

// Render go template.
func (gt *GoTemplate) Render(data interface{}) (string, error) {
	buf := new(bytes.Buffer)
	err := gt.template.Execute(buf, data)

	if nil != err {
		return "", fmt.Errorf("execute go template: %w", err)
	}

	return buf.String(), nil
}

// GoTemplateInstanceLoader load go template.
type GoTemplateInstanceLoader struct {
	partialsLoader  PartialsLoader
	markdownHelpers MarkdownHelpers
}

// NewGoTemplateInstanceLoader constructor.
func NewGoTemplateInstanceLoader(
	partialsLoader PartialsLoader,
	markMarkdownHelpers MarkdownHelpers,
) *GoTemplateInstanceLoader {
	return &GoTemplateInstanceLoader{
		partialsLoader:  partialsLoader,
		markdownHelpers: markMarkdownHelpers,
	}
}

// TemplateType return go template type.
func (*GoTemplateInstanceLoader) TemplateType() string {
	return "go_template"
}

// TemplateName return go template name.
func (*GoTemplateInstanceLoader) TemplateName(partName string) string {
	return partName + goTemplateExtension
}

// LoadTemplateInstance go template partName from templateDirPath.
func (loader *GoTemplateInstanceLoader) LoadTemplateInstance(
	templateDirPath string,
	partName string,
	logger log.Logger,
	assets templates.Assets,
) (HTMLTemplate, error) {
	templatePath := filepath.Join(templateDirPath, loader.TemplateName(partName))

	ctx := log.ContextWithLogger(context.Background(), logger)

	partials, err := loader.partialsLoader.LoadPartials(ctx, templateDirPath, goTemplateExtension)
	if err != nil {
		return nil, fmt.Errorf("load partials: %w", err)
	}

	template, err := templatePkg.
		New("template.tmpl").
		Funcs(loader.helpersMap(assets)).
		ParseFiles(templatePath)
	if nil != err {
		if os.IsNotExist(err) {
			return nil, NewTemplateFileNotFoundError(loader.TemplateType(), templatePath)
		}

		return nil, fmt.Errorf("parse go templates: %w", err)
	}

	for partialName, partialPayload := range partials {
		_, err := template.New(partialName).Parse(string(partialPayload))
		if err != nil {
			return nil, fmt.Errorf("parse go template partial %s: %w", partialName, err)
		}
	}

	return NewGoTemplate(template, logger), nil
}

func (loader *GoTemplateInstanceLoader) helpersMap(assets templates.Assets) templatePkg.FuncMap {
	embedHelpers := helpers.NewEmbedHelpers(assets)

	return templatePkg.FuncMap{
		"format_time": helpers.FormatTime,
		"markdown":    goTemplateHTMLHelper(loader.markdownHelpers.Markdown),

		"embed_text":   goTemplateHTMLHelper(embedHelpers.EmbedText),
		"embed_base64": goTemplateHTMLHelper(embedHelpers.EmbedBase64Encoded),
	}
}

func goTemplateHTMLHelper[T any](helper func(params T) string) func(params T) templatePkg.HTML {
	return func(params T) templatePkg.HTML {
		return templatePkg.HTML(helper(params))
	}
}
