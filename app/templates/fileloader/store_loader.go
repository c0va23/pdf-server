package fileloader

import (
	"errors"
	"fmt"
	"os"
	"path"

	"github.com/urfave/cli/v2"
	"golang.org/x/exp/maps"

	"gitlab.com/c0va23/pdf-server/app/log"
	"gitlab.com/c0va23/pdf-server/app/templates"
	"gitlab.com/c0va23/pdf-server/app/templates/fileloader/remote"
)

// NotDirInChildTemplatesDirError returned when child templates dir contains not dir entry.
type NotDirInChildTemplatesDirError struct {
	childTemplatesDirPath string
	notDirEntryName       string
}

func (err NotDirInChildTemplatesDirError) Error() string {
	return fmt.Sprintf(
		`child templates dir "%s" contains not dir entry "%s"`,
		err.childTemplatesDirPath,
		err.notDirEntryName,
	)
}

// SuitableTemplateLoaderNotFoundError returned when template loader not found.
type SuitableTemplateLoaderNotFoundError struct {
	sourceError error
}

func newTemplateLoaderNotFoundError(sourceErrors ...error) SuitableTemplateLoaderNotFoundError {
	return SuitableTemplateLoaderNotFoundError{
		sourceError: errors.Join(sourceErrors...),
	}
}

func (err SuitableTemplateLoaderNotFoundError) Error() string {
	return fmt.Sprintf("suitable template loader not found: %s", err.sourceError)
}

func (err SuitableTemplateLoaderNotFoundError) Unwrap() error {
	return err.sourceError
}

type templateReloadMode string

// Template Reload modes.
const (
	TemplateReloadModeNone   = templateReloadMode("none")
	TemplateReloadModeAlways = templateReloadMode("always")
)

func allTemplateReloadModes() []templateReloadMode {
	return []templateReloadMode{
		TemplateReloadModeNone,
		TemplateReloadModeAlways,
	}
}

// Set implement flag.Value interface.
func (reload *templateReloadMode) Set(value string) error {
	for _, allowedReloadMode := range allTemplateReloadModes() {
		if string(allowedReloadMode) == value {
			*reload = templateReloadMode(value)

			return nil
		}
	}

	return newUnknownTemplateReloadModeError(value)
}

// String implement Stringer interface.
func (reload *templateReloadMode) String() string {
	return string(*reload)
}

// UnknownTemplateReloadModeError returned when unknown template reload mode passed.
type UnknownTemplateReloadModeError struct {
	mode string
}

func newUnknownTemplateReloadModeError(mode string) *UnknownTemplateReloadModeError {
	return &UnknownTemplateReloadModeError{
		mode: mode,
	}
}

func (err *UnknownTemplateReloadModeError) Error() string {
	return "unknown template reload mode %s" + err.mode
}

// Config for templates.
type Config struct {
	DirPath             string
	CompositionsDirPath string
	ReloadMode          templateReloadMode
	Recursive           bool
}

// DefaultConfig return default config.
func DefaultConfig() Config {
	return Config{
		DirPath:             "templates/",
		CompositionsDirPath: "compositions/",
		ReloadMode:          TemplateReloadModeNone,
		Recursive:           false,
	}
}

// BindFlags return cli flag bindings.
func (config *Config) BindFlags() []cli.Flag {
	return []cli.Flag{
		&cli.StringFlag{
			Name:        "templates-dir",
			Aliases:     []string{"t"},
			EnvVars:     []string{"TEMPLATES_DIR"},
			Value:       config.DirPath,
			Destination: &config.DirPath,
			Hidden:      false,
			Usage:       "Templates directory path",
		},
		&cli.StringFlag{
			Name:        "compositions-dir",
			Aliases:     []string{},
			EnvVars:     []string{"COMPOSITIONS_DIR"},
			Value:       config.CompositionsDirPath,
			Destination: &config.CompositionsDirPath,
			Hidden:      false,
			Usage:       "Compositions directory path",
		},
		&cli.GenericFlag{
			Name:    "templates-reload-mode",
			Aliases: []string{},
			EnvVars: []string{"TEMPLATES_RELOAD_MODE"},
			Value:   &config.ReloadMode,
			Hidden:  false,
			Usage: fmt.Sprintf(
				"Mode of templates reloading. Allowed values: %s",
				allTemplateReloadModes(),
			),
		},
		&cli.BoolFlag{
			Name:        "templates-recursive",
			Aliases:     []string{},
			EnvVars:     []string{"TEMPLATES_RECURSIVE"},
			Value:       config.Recursive,
			Destination: &config.Recursive,
			Hidden:      false,
			Usage:       "Load templates recursively",
		},
	}
}

// OrderedTemplateLoaders is a list of template loaders.
type OrderedTemplateLoaders []templates.TemplateLoader

// DefaultOrderedTemplateLoaders return default template loaders.
func DefaultOrderedTemplateLoaders(
	fileTemplateLoader *FileTemplateLoader,
	rawTemplateLoader *RawTemplateLoader,
	httpTemplateLoader *remote.HTTPLoader,
) OrderedTemplateLoaders {
	return OrderedTemplateLoaders{
		fileTemplateLoader,
		rawTemplateLoader,
		httpTemplateLoader,
	}
}

// OrderedTemplateLoaderBuilders is a list of template loader builders.
type OrderedTemplateLoaderBuilders []templates.TemplateLoaderBuilder

// DefaultOrderedTemplateLoaderBuilders return default template loader builders.
func DefaultOrderedTemplateLoaderBuilders(
	compositionTemplateLoaderBuilder *compositionLoaderBuilder,
) OrderedTemplateLoaderBuilders {
	return OrderedTemplateLoaderBuilders{
		compositionTemplateLoaderBuilder,
	}
}

// LoadStore load templates from directory.
func LoadStore(loader *StoreLoader) (templates.Store, error) {
	store, err := loader.loadTemplatesMap()
	if err != nil {
		return nil, fmt.Errorf("load templates map: %w", err)
	}

	return store, nil
}

// StoreLoader load templates from directory.
type StoreLoader struct {
	config                        Config
	orderedTemplateLoaders        OrderedTemplateLoaders
	orderedTemplateLoaderBuilders OrderedTemplateLoaderBuilders
	storeProvider                 templates.StoreProvider
	logger                        log.Logger
}

// NewStoreLoader construct new TemplatesMapLoader.
func NewStoreLoader(
	config Config,
	orderedTemplateLoaders OrderedTemplateLoaders,
	orderedTemplateLoaderBuilders OrderedTemplateLoaderBuilders,
	storeProvider templates.StoreProvider,
	logger log.Logger,
) *StoreLoader {
	logger = logger.WithField(log.LoggerField, "templates")

	return &StoreLoader{
		config:                        config,
		orderedTemplateLoaders:        orderedTemplateLoaders,
		orderedTemplateLoaderBuilders: orderedTemplateLoaderBuilders,
		storeProvider:                 storeProvider,
		logger:                        logger,
	}
}

func (loader *StoreLoader) loadTemplatesMap() (templates.Store, error) {
	templateStores, err := loader.loadTemplates(
		loader.config.DirPath,
		loader.orderedTemplateLoaders,
	)
	if err != nil {
		return nil, fmt.Errorf("load templates: %w", err)
	}

	dependentTemplateLoaders, err := loader.buildDependentTemplateLoaders(templateStores)
	if err != nil {
		return nil, fmt.Errorf("build dependent template loaders: %w", err)
	}

	dependentTemplateStores, err := loader.loadTemplates(
		loader.config.CompositionsDirPath,
		dependentTemplateLoaders,
	)
	if err != nil {
		return nil, fmt.Errorf("load compositions: %w", err)
	}

	store := fallbackStore{
		templateStores,
		dependentTemplateStores,
	}

	loader.storeProvider.ProvideTemplatesStore(store)

	return store, nil
}

func (loader *StoreLoader) buildDependentTemplateLoaders(
	templateStores templates.Store,
) (OrderedTemplateLoaders, error) {
	dependentTemplateLoaders := make(
		OrderedTemplateLoaders,
		0,
		len(loader.orderedTemplateLoaderBuilders),
	)

	for _, templateLoaderBuilder := range loader.orderedTemplateLoaderBuilders {
		templateLoader, err := templateLoaderBuilder.BuildTemplateLoader(templateStores)
		if err != nil {
			return nil, fmt.Errorf("build composition loader: %w", err)
		}

		dependentTemplateLoaders = append(dependentTemplateLoaders, templateLoader)
	}

	return dependentTemplateLoaders, nil
}

func (loader *StoreLoader) loadTemplates(
	dirPath string,
	orderedTemplateLoaders OrderedTemplateLoaders,
) (templates.Store, error) {
	stores, err := loader.buildLoaderStoresMap(dirPath, orderedTemplateLoaders)
	if err != nil {
		return nil, fmt.Errorf("build loader stores map: %w", err)
	}

	templateDirs, err := os.ReadDir(dirPath)
	if nil != err {
		return nil, fmt.Errorf("read templates dir: %w", err)
	}

	for _, templateDir := range templateDirs {
		templateName := templateDir.Name()

		err := loader.loadTemplate(
			dirPath,
			orderedTemplateLoaders,
			stores,
			templateName,
		)
		if err != nil {
			return nil, fmt.Errorf("load template name %s: %w", templateName, err)
		}
	}

	return fallbackStore(maps.Values(stores)), nil
}

func (loader *StoreLoader) buildLoaderStoresMap(
	dirPath string,
	orderedTemplateLoaders OrderedTemplateLoaders,
) (map[templates.TemplateLoader]templates.Store, error) {
	stores := make(map[templates.TemplateLoader]templates.Store, len(orderedTemplateLoaders))

	for _, templateLoader := range orderedTemplateLoaders {
		store, err := loader.buildStore(loader.config.ReloadMode, dirPath, templateLoader)
		if err != nil {
			return nil, fmt.Errorf("build store: %w", err)
		}

		stores[templateLoader] = store
	}

	return stores, nil
}

// revive:disable:cognitive-complexity
func (loader *StoreLoader) tryTemplateLoader(
	dirPath string,
	orderedTemplateLoaders OrderedTemplateLoaders,
	templateName string,
) (templates.Template, templates.TemplateLoader, error) {
	var loadErrors []error

	for _, templateLoader := range orderedTemplateLoaders {
		template, err := templateLoader.LoadTemplate(dirPath, templateName, loader.logger)
		if nil != err {
			loader.
				logger.
				WithError(err).
				WithField("templateName", templateName).
				WithField("loader type", templateLoader.TemplateType()).
				Error("load template")

			if notFound := new(templates.TemplateNotExistsError); errors.As(err, &notFound) {
				loadErrors = append(loadErrors, err)

				continue
			}

			// HTML template not found
			if notFound := new(TemplateFileNotFoundError); errors.As(err, &notFound) {
				loadErrors = append(loadErrors, err)

				continue
			}

			return nil, nil, fmt.Errorf("load template type %s: %w", templateLoader.TemplateType(), err)
		}

		return template, templateLoader, nil
	}

	return nil, nil, newTemplateLoaderNotFoundError(loadErrors...)
}

func (loader *StoreLoader) loadTemplate(
	dirPath string,
	orderedTemplateLoaders OrderedTemplateLoaders,
	stores map[templates.TemplateLoader]templates.Store,
	templateName string,
) error {
	template, templateLoader, err := loader.tryTemplateLoader(
		dirPath,
		orderedTemplateLoaders,
		templateName,
	)
	if loader.config.Recursive && loader.isRecursiveSuitable(err) {
		//nolint:govet // allow shadow
		if err := loader.loadTemplateDirectory(
			dirPath,
			orderedTemplateLoaders,
			stores,
			templateName,
		); err != nil {
			return fmt.Errorf("load template directory: %w", err)
		}

		return nil
	}

	if err != nil {
		return fmt.Errorf("try load template: %w", err)
	}

	store := stores[templateLoader]

	if err := store.Put(template); err != nil {
		return fmt.Errorf("put template: %w", err)
	}

	return nil
}

func (*StoreLoader) isRecursiveSuitable(err error) bool {
	var loaderNotFound SuitableTemplateLoaderNotFoundError

	return errors.As(err, &loaderNotFound)
}

func (loader *StoreLoader) loadTemplateDirectory(
	rootDirPath string,
	orderedTemplateLoaders OrderedTemplateLoaders,
	stores map[templates.TemplateLoader]templates.Store,
	templateName string,
) error {
	parentDirPath := path.Join(rootDirPath, templateName)

	loader.logger.Infof("scan %s for child templates", templateName)

	childDirs, err := os.ReadDir(parentDirPath)
	if err != nil {
		return fmt.Errorf("read template dir: %w", err)
	}

	for _, childEntry := range childDirs {
		if !childEntry.IsDir() {
			return NotDirInChildTemplatesDirError{
				childTemplatesDirPath: parentDirPath,
				notDirEntryName:       childEntry.Name(),
			}
		}

		childTemplateName := path.Join(templateName, childEntry.Name())

		if err := loader.loadTemplate(
			rootDirPath,
			orderedTemplateLoaders,
			stores,
			childTemplateName,
		); err != nil {
			return fmt.Errorf("load child template name %s: %w", childTemplateName, err)
		}
	}

	return nil
}

func (loader *StoreLoader) buildStore(
	reloadMode templateReloadMode,
	templatesDirPath string,
	templateLoader templates.TemplateLoader,
) (templates.Store, error) {
	templatesMap := make(map[templates.Name]templates.Template)

	mapStore := templates.MapStore(templatesMap)

	switch reloadMode {
	case TemplateReloadModeNone:
		return mapStore, nil
	case TemplateReloadModeAlways:
		return NewDevStore(
			templatesDirPath,
			loader.logger,
			templateLoader,
			mapStore,
		), nil
	default:
		return nil, newUnknownTemplateReloadModeError(reloadMode.String())
	}
}

// DevStore is store for development mode. Reload template on every fetch.
type DevStore struct {
	templateDirPath string
	logger          log.Logger
	templateLoader  templates.TemplateLoader
	originStore     templates.Store
}

// NewDevStore construct new DevStore.
func NewDevStore(
	templateDirPath string,
	logger log.Logger,
	templateLoader templates.TemplateLoader,
	originStore templates.Store,
) *DevStore {
	return &DevStore{
		templateDirPath: templateDirPath,
		logger:          logger,
		templateLoader:  templateLoader,
		originStore:     originStore,
	}
}

// List all templates.
func (store *DevStore) List() ([]templates.Template, error) {
	templateList, err := store.originStore.List()
	if err != nil {
		return nil, fmt.Errorf("list templates: %w", err)
	}

	return templateList, nil
}

// Fetch template by name. Check original store first, then reload template.
func (store *DevStore) Fetch(templateName templates.Name) (templates.Template, error) {
	_, err := store.originStore.Fetch(templateName)
	if err != nil {
		return nil, fmt.Errorf("fetch template: %w", err)
	}

	template, err := store.templateLoader.LoadTemplate(
		store.templateDirPath,
		string(templateName),
		store.logger,
	)
	if err != nil {
		return nil, fmt.Errorf("load template: %w", err)
	}

	return template, nil
}

// Put template to store.
func (store *DevStore) Put(template templates.Template) error {
	if err := store.originStore.Put(template); err != nil {
		return fmt.Errorf("put template: %w", err)
	}

	return nil
}

type fallbackStore []templates.Store

// List all templates.
func (templateStores fallbackStore) List() ([]templates.Template, error) {
	allTemplatesList := []templates.Template{}

	for _, templateStore := range templateStores {
		templateList, err := templateStore.List()
		if err != nil {
			return nil, fmt.Errorf("list templates: %w", err)
		}

		allTemplatesList = append(allTemplatesList, templateList...)
	}

	return allTemplatesList, nil
}

// Fetch template by name.
func (templateStores fallbackStore) Fetch(templateName templates.Name) (templates.Template, error) {
	var errorList []error

	for _, templateStore := range templateStores {
		template, err := templateStore.Fetch(templateName)
		if err != nil {
			errorList = append(errorList, err)

			continue
		}

		return template, nil
	}

	return nil, fmt.Errorf("fetch template: %w", errors.Join(errorList...))
}

// Put template to store.
func (fallbackStore) Put(_ templates.Template) error {
	panic("not implemented")
}
