package fileloader

import (
	"bufio"
	"bytes"
	"context"
	"errors"
	"fmt"
	"path/filepath"
	"strconv"
	"strings"

	"github.com/pdfcpu/pdfcpu/pkg/pdfcpu"
	"github.com/pdfcpu/pdfcpu/pkg/pdfcpu/model"
	"github.com/pdfcpu/pdfcpu/pkg/pdfcpu/validate"
	"go.opentelemetry.io/otel/attribute"
	"go.opentelemetry.io/otel/codes"
	"go.opentelemetry.io/otel/trace"

	"gitlab.com/c0va23/pdf-server/app/log"
	templatesPkg "gitlab.com/c0va23/pdf-server/app/templates"
	compositionPkg "gitlab.com/c0va23/pdf-server/app/templates/fileloader/composition"
	"gitlab.com/c0va23/pdf-server/app/templates/futures"
)

const compositionTemplateNamePrefix = "composition:"

type compositionLoader struct {
	store          templatesPkg.Store
	plannerLoaders []CompositePlannerLoader
	schemaLoader   templatesPkg.SchemaLoader
	examplesLoader templatesPkg.ExamplesLoader
	tracerProvider trace.TracerProvider
}

// LoadTemplate load template dir.
func (loader *compositionLoader) LoadTemplate(
	templatesDirPath string,
	templateName string,
	logger log.Logger,
) (templatesPkg.Template, error) {
	// Truncate prefix on reload (dev mode).
	templateName = strings.TrimPrefix(templateName, compositionTemplateNamePrefix)

	templateDirPath := filepath.Join(templatesDirPath, templateName)

	templateLogger := logger.
		WithField(log.FieldTemplateLoader, "composition").
		WithField(log.FieldTemplateName, templateName)

	var planner compositionPkg.CompositePlanner

	for _, plannerLoader := range loader.plannerLoaders {
		var err error

		planner, err = plannerLoader.LoadPlanner(templateDirPath)
		if notFound := new(compositionPkg.PlannerNotFoundError); errors.As(err, &notFound) {
			continue
		}

		if err != nil {
			return nil, fmt.Errorf("load planner (%s): %w", plannerLoader.LoaderType(), err)
		}

		break
	}

	schema, err := loader.schemaLoader.LoadSchema(templateDirPath, templateLogger)
	if err != nil {
		return nil, fmt.Errorf("load schema: %w", err)
	}

	examples, err := loader.examplesLoader.LoadExamples(templateDirPath, templateLogger)
	if err != nil {
		return nil, fmt.Errorf("load examples: %w", err)
	}

	templateLogger.Infof("Composition is loaded")

	tracer := loader.tracerProvider.Tracer("template-composition/" + templateName)

	return &compositeTemplate{
		name:     templatesPkg.Name(compositionTemplateNamePrefix + templateName),
		schema:   schema,
		examples: examples,
		planner:  planner,
		tracer:   tracer,
	}, nil
}

// CompositePlannerLoader used to load CompositePlanner.
type CompositePlannerLoader interface {
	LoadPlanner(
		templateDirPath string,
	) (compositionPkg.CompositePlanner, error)

	LoaderType() string
}

// TemplateType return template type.
func (*compositionLoader) TemplateType() string {
	return "composition"
}

type compositionLoaderBuilder struct {
	schemaLoader   templatesPkg.SchemaLoader
	examplesLoader templatesPkg.ExamplesLoader
	tracerProvider trace.TracerProvider
}

// NewCompositionLoaderBuilder constructor.
func NewCompositionLoaderBuilder(
	schemaLoader templatesPkg.SchemaLoader,
	examplesLoader templatesPkg.ExamplesLoader,
	tracerProvider trace.TracerProvider,
) *compositionLoaderBuilder {
	return &compositionLoaderBuilder{
		schemaLoader:   schemaLoader,
		examplesLoader: examplesLoader,
		tracerProvider: tracerProvider,
	}
}

// BuildCompositionLoader build template loader.
func (builder *compositionLoaderBuilder) BuildTemplateLoader(
	store templatesPkg.Store,
) (templatesPkg.TemplateLoader, error) {
	return &compositionLoader{
		store: store,
		plannerLoaders: []CompositePlannerLoader{
			compositionPkg.NewYamlPlannerLoader(store),
			compositionPkg.NewJsPlannerLoader(store),
		},
		schemaLoader:   builder.schemaLoader,
		examplesLoader: builder.examplesLoader,
		tracerProvider: builder.tracerProvider,
	}, nil
}

// Composite template

type noTasksToRenderError struct{}

func (*noTasksToRenderError) Error() string {
	return "not tasks to render"
}

type compositeTemplate struct {
	name     templatesPkg.Name
	examples templatesPkg.Examples
	schema   templatesPkg.Validator
	planner  compositionPkg.CompositePlanner
	tracer   trace.Tracer
}

// GetName return template name.
func (template *compositeTemplate) GetName() templatesPkg.Name {
	return template.name
}

// GetExamples return template examples.
func (template *compositeTemplate) GetExamples() templatesPkg.Examples {
	return template.examples
}

// GetValidator return template validator.
func (template *compositeTemplate) GetValidator() templatesPkg.Validator {
	return template.schema
}

// Render render template.
func (template *compositeTemplate) Render(
	ctx context.Context,
	data templatesPkg.Data,
) templatesPkg.RenderFuture {
	ctx, span := template.tracer.Start(ctx, template.buildSpanName("Render"))
	defer span.End()

	payload, err := template.renderWrapper(ctx, data)
	if err != nil {
		span.SetStatus(codes.Error, err.Error())

		return futures.NewResolved(futures.FailureResult[[]byte](err))
	}

	return futures.NewResolved(futures.SuccessResult(payload))
}

func (template *compositeTemplate) renderWrapper(
	ctx context.Context,
	data templatesPkg.Data,
) ([]byte, error) {
	renderTasks, err := template.BuildPlan(ctx, data)
	if err != nil {
		return nil, fmt.Errorf("build plan: %w", err)
	}

	if len(renderTasks) == 0 {
		return nil, &noTasksToRenderError{}
	}

	renderFutures := template.renderTemplates(ctx, renderTasks)

	if len(renderFutures) == 1 {
		//nolint:govet // shadow
		payload, err := renderFutures[0].Await().Get()
		if err != nil {
			return nil, fmt.Errorf("get only first payload: %w", err)
		}

		return payload, nil
	}

	payload, err := template.mergeResults(ctx, renderFutures)
	if err != nil {
		return nil, fmt.Errorf("merge results: %w", err)
	}

	return payload, nil
}

// BuildPlan of composite template execution.
func (template *compositeTemplate) BuildPlan(
	ctx context.Context,
	data templatesPkg.Data,
) ([]templatesPkg.RenderTask, error) {
	ctx, span := template.tracer.Start(ctx, template.buildSpanName("BuildPlan"))
	defer span.End()

	span.SetAttributes(
		attribute.String("planner.type", template.planner.PlannerType()),
	)

	renderTasks, err := template.planner.BuildPlan(ctx, data)
	if err != nil {
		span.SetStatus(codes.Error, err.Error())

		return nil, fmt.Errorf("build plan: %w", err)
	}

	return renderTasks, nil
}

func (template *compositeTemplate) mergeResults(
	ctx context.Context,
	renderFutures []templatesPkg.RenderFuture,
) ([]byte, error) {
	_, span := template.tracer.Start(ctx, template.buildSpanName("MergeResults"))
	defer span.End()

	mergedPayload, err := template.mergePDFs(ctx, renderFutures)
	if err != nil {
		span.SetStatus(codes.Error, err.Error())

		return nil, fmt.Errorf("merge templates: %w", err)
	}

	return mergedPayload, nil
}

func (template *compositeTemplate) buildSpanName(action string) string {
	return fmt.Sprintf("template-composition/%s.%s", template.name, action)
}

func (template *compositeTemplate) renderTemplates(
	ctx context.Context,
	renderTasks []templatesPkg.RenderTask,
) []templatesPkg.RenderFuture {
	ctx, span := template.tracer.Start(ctx, template.buildSpanName("renderTemplates"))
	defer span.End()

	renderFutures := make([]templatesPkg.RenderFuture, 0, len(renderTasks))

	for _, renderTask := range renderTasks {
		templateLogger := log.LoggerFromContext(ctx, "compositeTemplate").
			WithField(log.FieldTemplateName, renderTask.TemplateName)

		templateLogger.Infof("Render template")

		renderFuture := renderTask.Template.Render(ctx, renderTask.Data)

		renderFutures = append(renderFutures, renderFuture)
	}

	return renderFutures
}

func (template *compositeTemplate) mergePDFs(
	ctx context.Context,
	renderFutures []templatesPkg.RenderFuture,
) ([]byte, error) {
	conf := model.NewDefaultConfiguration()
	conf.Cmd = model.MERGECREATE
	conf.CreateBookmarks = false

	ctxDest, err := template.readAndValidate(ctx, renderFutures[0], conf)
	if err != nil {
		return nil, fmt.Errorf("read and validate first pdf: %w", err)
	}

	for index, renderFuture := range renderFutures[1:] {
		if err = template.appendTo(ctx, renderFuture, strconv.Itoa(index), ctxDest); err != nil {
			return nil, fmt.Errorf("append %d pdf: %w", index, err)
		}
	}

	if err := template.optimizePdf(ctx, ctxDest); err != nil {
		return nil, fmt.Errorf("optimize: %w", err)
	}

	return template.buildMergedPdf(ctx, ctxDest)
}

func (template *compositeTemplate) optimizePdf(ctx context.Context, ctxDest *model.Context) error {
	_, span := template.tracer.Start(ctx, "pdfcpu.optimizeXRefTable")
	defer span.End()

	if err := pdfcpu.OptimizeXRefTable(ctxDest); err != nil {
		return fmt.Errorf("optimize pdf: %w", err)
	}

	return nil
}

func (template *compositeTemplate) buildMergedPdf(
	ctx context.Context,
	ctxDest *model.Context,
) ([]byte, error) {
	_, span := template.tracer.Start(ctx, "pdfcpu.writePdfContext")
	defer span.End()

	mergedResult := new(bytes.Buffer)

	ctxDest.Write.Writer = bufio.NewWriter(mergedResult)

	if err := pdfcpu.Write(ctxDest); err != nil {
		return nil, fmt.Errorf("write pdf: %w", err)
	}

	if err := ctxDest.Write.Flush(); err != nil {
		return nil, fmt.Errorf("flush buffer: %w", err)
	}

	return mergedResult.Bytes(), nil
}

func (template *compositeTemplate) readAndValidate(
	ctx context.Context,
	payloadFuture templatesPkg.RenderFuture,
	conf *model.Configuration,
) (pdfCtx *model.Context, err error) {
	payload, err := payloadFuture.Await().Get()
	if err != nil {
		return nil, fmt.Errorf("await and get pdf: %w", err)
	}

	_, span := template.tracer.Start(ctx, "pdfcpu.readPdfContext")
	defer span.End()

	rs := bytes.NewReader(payload)

	pdfCtx, err = pdfcpu.Read(rs, conf)
	if err != nil {
		return nil, fmt.Errorf("read pdf: %w", err)
	}

	if err = validate.XRefTable(pdfCtx.XRefTable); err != nil {
		return nil, fmt.Errorf("validate pdf: %w", err)
	}

	if pdfCtx.Version() == model.V20 {
		return nil, fmt.Errorf("pdf version: %w", pdfcpu.ErrUnsupportedVersion)
	}

	return pdfCtx, nil
}

// appendTo appends rs to ctxDest's page tree.
func (template *compositeTemplate) appendTo(
	ctx context.Context,
	renderFuture templatesPkg.RenderFuture,
	fName string,
	ctxDest *model.Context,
) error {
	ctxSource, err := template.readAndValidate(ctx, renderFuture, ctxDest.Configuration)
	if err != nil {
		return fmt.Errorf("read and validate pdf: %w", err)
	}

	_, span := template.tracer.Start(ctx, "pdfcpu.mergeXRefTables")
	defer span.End()

	zipPDFs := false
	dividerPage := false

	// Merge source context into dest context.
	if err := pdfcpu.MergeXRefTables(fName, ctxSource, ctxDest, zipPDFs, dividerPage); err != nil {
		return fmt.Errorf("merge pdf: %w", err)
	}

	return nil
}
