package fileloader_test

import (
	"testing"

	"github.com/stretchr/testify/assert"

	"gitlab.com/c0va23/pdf-server/app/log"
	"gitlab.com/c0va23/pdf-server/app/templates/fileloader"
)

func TestFileSchemaLoader_LoadSchema(t *testing.T) {
	t.Parallel()

	var (
		logger = log.NewLogger(log.LevelFatal, log.TextFormat)

		schemaLoader = fileloader.NewFileSchemaLoader()

		notError = ""
	)

	type TestCase struct {
		name                 string
		templateDirPath      string
		expectSchemaReturned bool
		expectedErrorMessage string
	}

	testCases := []TestCase{
		{
			name:                 "schema not found",
			templateDirPath:      "not_found",
			expectSchemaReturned: false,
			expectedErrorMessage: notError,
		},
		{
			name: "schema invalid",

			templateDirPath:      "fixtures/templates/invalid_schema",
			expectSchemaReturned: false,
			expectedErrorMessage: `'/type' does not validate with`,
		},
		{
			name:                 "",
			templateDirPath:      "fixtures/templates/valid_schema",
			expectSchemaReturned: true,
			expectedErrorMessage: notError,
		},
	}

	for _, testCase := range testCases {
		t.Run(testCase.name, func(t *testing.T) {
			t.Parallel()

			actualSchema, actualError := schemaLoader.LoadSchema(testCase.templateDirPath, logger)

			if testCase.expectSchemaReturned {
				assert.NotNil(t, actualSchema)
			} else {
				assert.Nil(t, actualSchema)
			}

			if testCase.expectedErrorMessage == notError {
				assert.NoError(t, actualError)
			} else {
				assert.ErrorContains(t, actualError, testCase.expectedErrorMessage)
			}
		})
	}
}
