package remote

import (
	"bufio"
	"context"
	"fmt"
	"io"
	"net/http"
	"os"
	"path"
	"strings"

	handlebars "github.com/mailgun/raymond/v2"
	"go.opentelemetry.io/otel/trace"

	"gitlab.com/c0va23/pdf-server/app/log"
	"gitlab.com/c0va23/pdf-server/app/templates"
	"gitlab.com/c0va23/pdf-server/app/templates/futures"
)

const httpTemplateFileName = "template.http"

const handlebarsHTTPTemplateFileName = httpTemplateFileName + ".hbs"

const maxHTTPStatusCode = 299

// BadHTTPStatusCodeError bad http status code error.
type BadHTTPStatusCodeError struct {
	statusCode int
}

func (e *BadHTTPStatusCodeError) Error() string {
	return fmt.Sprintf("bad http code: %d", e.statusCode)
}

// HTTPLoader loads http templates.
type HTTPLoader struct {
	tracerProvider trace.TracerProvider
	schemaLoader   templates.SchemaLoader
	examplesLoader templates.ExamplesLoader
}

// NewHTTPTemplateLoader constructor.
func NewHTTPTemplateLoader(
	tracerProvider trace.TracerProvider,
	schemaLoader templates.SchemaLoader,
	examplesLoader templates.ExamplesLoader,
) *HTTPLoader {
	return &HTTPLoader{
		tracerProvider: tracerProvider,
		schemaLoader:   schemaLoader,
		examplesLoader: examplesLoader,
	}
}

// TemplateType returns template type.
func (*HTTPLoader) TemplateType() string {
	return "remote_http"
}

// LoadTemplate loads template.
func (loader *HTTPLoader) LoadTemplate(
	templatesDirPath string,
	templateName string,
	logger log.Logger,
) (templates.Template, error) {
	templateDirPath := path.Join(templatesDirPath, templateName)
	templatePath := path.Join(templateDirPath, handlebarsHTTPTemplateFileName)

	template, err := handlebars.ParseFile(templatePath)
	if err != nil {
		if os.IsNotExist(err) {
			return nil, templates.NewTemplateNotExistsError(templates.Name(templateName))
		}

		return nil, fmt.Errorf("parse handlebars template: %w", err)
	}

	schema, err := loader.schemaLoader.LoadSchema(templateDirPath, logger)
	if err != nil {
		return nil, fmt.Errorf("load schema: %w", err)
	}

	examples, err := loader.examplesLoader.LoadExamples(templateDirPath, logger)
	if err != nil {
		return nil, fmt.Errorf("load examples: %w", err)
	}

	tracer := loader.tracerProvider.Tracer("template-raw/" + templateName)

	// TODO: load client config and configure
	httpClient := new(http.Client)

	return &HTTPTemplate{
		template:   template,
		name:       templates.Name(templateName),
		tracer:     tracer,
		httpClient: httpClient,
		schema:     schema,
		examples:   examples,
	}, nil
}

// HTTPTemplate instance.
type HTTPTemplate struct {
	template   *handlebars.Template
	name       templates.Name
	tracer     trace.Tracer
	httpClient *http.Client
	schema     templates.Validator
	examples   templates.Examples
}

// GetName return template name.
func (template *HTTPTemplate) GetName() templates.Name {
	return template.name
}

// GetExamples return template examples.
func (template *HTTPTemplate) GetExamples() templates.Examples {
	return template.examples
}

// GetValidator return template validator.
func (template *HTTPTemplate) GetValidator() templates.Validator {
	return template.schema
}

// Render template.
func (template *HTTPTemplate) Render(
	ctx context.Context,
	data templates.Data,
) templates.RenderFuture {
	return futures.GoResult(func() ([]byte, error) {
		body, err := template.download(ctx, data)
		if err != nil {
			return nil, fmt.Errorf("download template: %w", err)
		}

		return body, nil
	})
}

func (template *HTTPTemplate) download(
	ctx context.Context,
	data templates.Data,
) ([]byte, error) {
	ctx, span := template.tracer.Start(ctx, "remote-htt")
	defer span.End()

	logger := log.LoggerFromContext(ctx, "remote-http")

	// remove request URI to allow use server request in client
	request, err := template.buildRequest(ctx, data)
	if err != nil {
		return nil, fmt.Errorf("build request: %w", err)
	}

	span.AddEvent("request parsed")

	response, err := template.httpClient.Do(request)
	if err != nil {
		return nil, fmt.Errorf("do request: %w", err)
	}

	span.AddEvent("header response")

	defer closeResponse(response, logger)

	if response.StatusCode > maxHTTPStatusCode {
		return nil, &BadHTTPStatusCodeError{
			statusCode: response.StatusCode,
		}
	}

	body, err := io.ReadAll(response.Body)
	if err != nil {
		return nil, fmt.Errorf("read response body: %w", err)
	}

	return body, nil
}

func (template *HTTPTemplate) buildRequest(
	ctx context.Context,
	data templates.Data,
) (*http.Request, error) {
	logger := log.LoggerFromContext(ctx, "remote-http")

	rawRequest, err := template.template.Exec(data)
	if err != nil {
		return nil, fmt.Errorf("render handlebars template: %w", err)
	}

	logger.WithField("request", rawRequest).Debug("rendered request")

	rawRequest = strings.ReplaceAll(rawRequest, "\n", "\r\n")

	request, err := http.ReadRequest(bufio.NewReader(strings.NewReader(rawRequest)))
	if err != nil {
		return nil, fmt.Errorf("read request: %w", err)
	}

	request.RequestURI = ""
	request = request.WithContext(ctx)

	logger.WithField("request", fmt.Sprintf("%+v", request)).Debug("parsed request")

	return request, nil
}

func closeResponse(response *http.Response, logger log.Logger) {
	if closeErr := response.Body.Close(); closeErr != nil {
		logger.WithError(closeErr).Errorf("close response body")
	}
}
