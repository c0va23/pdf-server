package templates

import (
	"context"
	"io"
	"time"

	"gitlab.com/c0va23/pdf-server/app/templates/futures"
)

// Data for render and examples.
type Data map[string]interface{}

// Examples map.
type Examples map[string]Data

// Name of template.
type Name string

// RenderResult returned into render future.
type RenderResult = futures.Result[[]byte]

// RenderFuture returned by Render method.
type RenderFuture = futures.Future[RenderResult]

// Template interface.
type Template interface {
	GetName() Name
	GetExamples() Examples
	GetValidator() Validator
	Render(ctx context.Context, data Data) RenderFuture
}

// RenderTask render task returned by planner.
type RenderTask struct {
	TemplateName string
	Template     Template
	Data         Data
}

// CompositeTemplate interface.
type CompositeTemplate interface {
	BuildPlan(
		context.Context,
		Data,
	) ([]RenderTask, error)
}

// Asset struct.
type Asset struct {
	Reader  io.ReadSeekCloser
	ModTime time.Time
}

// Close implements io.Closer.
//
//nolint:wrapcheck
func (a *Asset) Close() error {
	return a.Reader.Close()
}

// Read implements io.Reader.
//
//nolint:wrapcheck
func (a *Asset) Read(b []byte) (int, error) {
	return a.Reader.Read(b)
}

// Seek implements io.Seeker.
//
//nolint:wrapcheck
func (a *Asset) Seek(offset int64, whence int) (int64, error) {
	return a.Reader.Seek(offset, whence)
}

// Assets interface.
type Assets interface {
	Open(string) (*Asset, error)
}

// AssetsProvider interface.
type AssetsProvider interface {
	GetAssets() Assets
}
