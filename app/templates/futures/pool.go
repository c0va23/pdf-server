package futures

// PoolSchedulerConfig for PoolScheduler.
type PoolSchedulerConfig struct {
	Workers int
}

// PoolScheduler for futures.
type PoolScheduler[R any] struct {
	queue chan func()
}

// NewWorkerPoolScheduler constructor.
func NewWorkerPoolScheduler[R any](
	config PoolSchedulerConfig,
) *PoolScheduler[R] {
	scheduler := PoolScheduler[R]{
		queue: make(chan func(), config.Workers),
	}

	for range config.Workers {
		go scheduler.startWorker()
	}

	return &scheduler
}

// ProvidePoolScheduler provider.
func ProvidePoolScheduler[R any](
	config PoolSchedulerConfig,
) (*PoolScheduler[R], func()) {
	scheduler := NewWorkerPoolScheduler[R](config)

	return scheduler, scheduler.close
}

// Schedule task. Implements Scheduler interface.
func (scheduler *PoolScheduler[R]) Schedule(
	executable func() R,
) Future[R] {
	future := &Scheduled[R]{
		fn: executable,
		ch: make(chan struct{}),
	}

	scheduler.queue <- func() {
		future.result = future.fn()

		close(future.ch)
	}

	return future
}

func (scheduler *PoolScheduler[R]) startWorker() {
	for {
		executable, ok := <-scheduler.queue
		if !ok {
			break
		}

		executable()
	}
}

func (scheduler *PoolScheduler[R]) close() {
	close(scheduler.queue)
}
