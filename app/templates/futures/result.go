package futures

// Result container.
type Result[R any] struct {
	value R
	err   error
}

// NewResult constructor.
func NewResult[R any](value R, err error) Result[R] {
	return Result[R]{
		value: value,
		err:   err,
	}
}

// SuccessResult constructor.
func SuccessResult[R any](value R) Result[R] {
	return Result[R]{
		value: value,
		err:   nil,
	}
}

// FailureResult constructor.
func FailureResult[R any](err error) Result[R] {
	var value R

	return Result[R]{
		value: value,
		err:   err,
	}
}

// WrapResultFunc wrap function to return Result.
func WrapResultFunc[R any](fn func() (R, error)) func() Result[R] {
	return func() Result[R] {
		return WrapResult(fn())
	}
}

// WrapResult wrap value and error.
func WrapResult[R any](value R, err error) Result[R] {
	if err != nil {
		return FailureResult[R](err)
	}

	return SuccessResult(value)
}

// Get result value and error.
func (result Result[R]) Get() (R, error) {
	return result.value, result.err
}
