package futures

// GoFuture for futures executed in goroutine.
type GoFuture[R any] struct {
	resultCh chan R
}

// Await implements Future interface.
//
//revive:disable-next-line:confusing-naming
func (future *GoFuture[R]) Await() R {
	return <-future.resultCh
}

// GoResult for futures executed in goroutine.
func GoResult[R any](fn func() (R, error)) *GoFuture[Result[R]] {
	resultCh := make(chan Result[R])

	go func() {
		resultCh <- WrapResult(fn())
	}()

	return &GoFuture[Result[R]]{resultCh: resultCh}
}
