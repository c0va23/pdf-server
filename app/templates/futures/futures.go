package futures

// Future interface.
type Future[R any] interface {
	Await() R
}

// Resolved future.
type Resolved[R any] struct {
	value R
}

// NewResolved constructor.
func NewResolved[R any](value R) *Resolved[R] {
	return &Resolved[R]{
		value: value,
	}
}

// Await implements Future interface.
//
//revive:disable-next-line:confusing-naming
func (future *Resolved[R]) Await() R {
	return future.value
}

// Scheduled future.
type Scheduled[R any] struct {
	fn     func() R
	ch     chan struct{}
	result R
}

// Await implements Future interface.
//
//revive:disable-next-line:confusing-naming
func (future *Scheduled[R]) Await() R {
	<-future.ch

	return future.result
}

// Scheduler interface.
type Scheduler[R any] interface {
	Schedule(func() R) Future[R]
}
