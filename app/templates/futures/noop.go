package futures

// NoopScheduler execute scheduled task in-place.
type NoopScheduler[R any] struct{}

// NewNoopScheduler constructor.
func NewNoopScheduler[R any]() NoopScheduler[R] {
	return NoopScheduler[R]{}
}

// Schedule tasks.
func (NoopScheduler[R]) Schedule(
	executable func() R,
) Future[R] {
	return NewResolved(executable())
}
