package pdfrender

import (
	"context"
)

// Config is print config.
type Config struct {
	Landscape          *bool
	PrintBackground    *bool
	Scale              *float64
	PaperWidth         *float64
	PaperHeight        *float64
	MarginTop          *float64
	MarginBottom       *float64
	MarginLeft         *float64
	MarginRight        *float64
	PreferCSSPageSize  *bool
	WaitLifecycleEvent *WaitLifecycleEvent
}

// TemplateName type.
type TemplateName string

// Document wrap HTML with template metadata.
type Document struct {
	BodyHTML     string
	HeaderHTML   *string
	FooterHTML   *string
	TemplateName TemplateName
}

// CallbackEvent send from page to internal server.
type CallbackEvent struct{}

// CallbackEventChan is channel for CallbackEvent.
type CallbackEventChan chan CallbackEvent

// RenderPoolStats container stats of render pool.
type RenderPoolStats struct {
	Active      int
	Idle        int
	Destroyed   int
	Invalidated int
}

// RenderPoolInfo contains stats and type.
type RenderPoolInfo struct {
	Stats RenderPoolStats
	Type  string
}

// RenderInfo contains pool info and metadata.
type RenderInfo struct {
	Pool     RenderPoolInfo
	Metadata map[string]string
}

// PDFRenderer interface.
type PDFRenderer interface {
	RenderPDF(
		ctx context.Context,
		config *Config,
		document Document,
	) (
		[]byte,
		error,
	)
	Info(context.Context) (*RenderInfo, error)
}
