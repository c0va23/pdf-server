package server_test

import (
	"math/rand"
	"testing"

	"github.com/stretchr/testify/assert"

	"gitlab.com/c0va23/pdf-server/app/pdfrender"
	"gitlab.com/c0va23/pdf-server/app/pdfrender/server"
)

func TestMapStore(t *testing.T) {
	t.Parallel()

	randomSource := rand.NewSource(1)
	mapStore := server.NewMapStore(randomSource)

	emptyDocumentID := 0

	firstDocument := pdfrender.Document{
		BodyHTML:     "doc 1",
		TemplateName: "doc1",
	}

	firstEvents := make(pdfrender.CallbackEventChan)
	defer close(firstEvents)

	secondDocument := pdfrender.Document{
		BodyHTML:     "doc 1",
		TemplateName: "doc1",
	}

	secondEvents := make(pdfrender.CallbackEventChan)
	defer close(secondEvents)

	firstDocumentID := mapStore.Add(firstDocument, firstEvents)
	assert.NotEqual(t, emptyDocumentID, firstDocumentID)

	secondDocumentID := mapStore.Add(secondDocument, secondEvents)
	assert.NotEqual(t, emptyDocumentID, secondDocumentID)

	foundedFirstDocument, foundedFirstEvents, found := mapStore.Get(firstDocumentID)
	assert.True(t, found)
	assert.Equal(t, firstDocument, foundedFirstDocument)
	assert.Equal(t, firstEvents, foundedFirstEvents)

	foundedSecondDocument, foundedSecondEvents, found := mapStore.Get(secondDocumentID)
	assert.True(t, found)
	assert.Equal(t, secondDocument, foundedSecondDocument)
	assert.Equal(t, secondEvents, foundedSecondEvents)

	mapStore.Remove(secondDocumentID)

	_, _, found = mapStore.Get(secondDocumentID)
	assert.False(t, found)

	_, _, found = mapStore.Get(firstDocumentID)
	assert.True(t, found)
}
