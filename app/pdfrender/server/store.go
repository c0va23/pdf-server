package server

import (
	"fmt"
	"math/rand"
	"sync"

	"gitlab.com/c0va23/pdf-server/app/pdfrender"
)

// DocumentID is store key.
type DocumentID uint64

func (id DocumentID) String() string {
	return fmt.Sprintf("%d", id)
}

// Store interface for Handler.
type Store interface {
	Add(pdfrender.Document, pdfrender.CallbackEventChan) DocumentID
	Remove(DocumentID)
	Get(DocumentID) (pdfrender.Document, pdfrender.CallbackEventChan, bool)
}

type documentWithEvents struct {
	document pdfrender.Document
	events   pdfrender.CallbackEventChan
}

// MapStore is default implementation for Store.
type MapStore struct {
	sync.RWMutex
	data map[DocumentID]documentWithEvents
	rand *rand.Rand
}

// NewMapStore constructor.
func NewMapStore(randSrc rand.Source) *MapStore {
	return &MapStore{
		data: map[DocumentID]documentWithEvents{},
		rand: rand.New(randSrc),
	}
}

// Add document to store.
func (mapStore *MapStore) Add(
	document pdfrender.Document,
	events pdfrender.CallbackEventChan,
) DocumentID {
	mapStore.Lock()
	defer mapStore.Unlock()

	documentID := DocumentID(mapStore.rand.Uint64())

	mapStore.data[documentID] = documentWithEvents{
		document: document,
		events:   events,
	}

	return documentID
}

// Remove document by ID.
func (mapStore *MapStore) Remove(documentID DocumentID) {
	mapStore.Lock()
	defer mapStore.Unlock()

	delete(mapStore.data, documentID)
}

// Get document by ID.
func (mapStore *MapStore) Get(documentID DocumentID) (
	pdfrender.Document,
	pdfrender.CallbackEventChan,
	bool,
) {
	mapStore.RLock()
	defer mapStore.RUnlock()

	documentWithEvent, found := mapStore.data[documentID]

	return documentWithEvent.document, documentWithEvent.events, found
}
