package server_test

import (
	"bytes"
	"context"
	"fmt"
	"net/http"
	"net/http/httptest"
	"os"
	"strconv"
	"testing"
	"time"

	"github.com/labstack/echo/v4"
	"github.com/stretchr/testify/assert"
	noopTrace "go.opentelemetry.io/otel/trace/noop"

	"gitlab.com/c0va23/pdf-server/app/log"
	"gitlab.com/c0va23/pdf-server/app/pdfrender"
	"gitlab.com/c0va23/pdf-server/app/pdfrender/server"
	"gitlab.com/c0va23/pdf-server/app/templates"
	"gitlab.com/c0va23/pdf-server/app/utils/testutils"

	pdfRenderServerMocks "gitlab.com/c0va23/pdf-server/mocks/pdfrender/server"
	templatesMocks "gitlab.com/c0va23/pdf-server/mocks/templates"
)

func TestHandlers_Get(t *testing.T) {
	t.Parallel()

	logger := log.NewLogger(log.LevelWarn, log.TextFormat)
	ctx := log.ContextWithLogger(context.TODO(), logger)

	templateName := "demo_template"
	documentID := 123

	validRequestPath := fmt.Sprintf("/templates/%s/%d", templateName, documentID)
	validDocumentHTML := "valid html"

	tracerProvider := noopTrace.NewTracerProvider()

	type TestCase struct {
		name              string
		requestPath       string
		requestDocumentID string
		getDocumentResult *pdfrender.Document
		getDocumentCalled bool
		getDocumentFound  bool
		expectedError     error
		expectedBody      []byte
	}

	testCases := []TestCase{
		{
			name:              "invalid document ID",
			requestPath:       fmt.Sprintf("/templates/%s/invalid-id", templateName),
			requestDocumentID: "invalid-id",
			getDocumentCalled: false,
			getDocumentResult: nil,
			getDocumentFound:  false,
			expectedError:     echo.ErrBadRequest,
			expectedBody:      nil,
		},
		{
			name:              "document ID not found",
			requestPath:       validRequestPath,
			requestDocumentID: strconv.Itoa(documentID),
			getDocumentCalled: true,
			getDocumentResult: nil,
			getDocumentFound:  false,
			expectedError:     echo.ErrNotFound,
			expectedBody:      nil,
		},
		{
			name:              "document ID found",
			requestPath:       validRequestPath,
			requestDocumentID: strconv.Itoa(documentID),
			getDocumentCalled: true,
			getDocumentResult: &pdfrender.Document{
				BodyHTML:     validDocumentHTML,
				TemplateName: pdfrender.TemplateName(templateName),
			},
			getDocumentFound: true,
			expectedError:    nil,
			expectedBody:     []byte(validDocumentHTML),
		},
	}

	for _, testCase := range testCases {
		t.Run(testCase.name, func(t *testing.T) {
			t.Parallel()

			documentStore := new(pdfRenderServerMocks.Store)
			defer documentStore.AssertExpectations(t)

			templatesStoreConsumer := new(templatesMocks.StoreConsumer)
			defer templatesStoreConsumer.AssertExpectations(t)

			handler := server.NewHandler(documentStore, templatesStoreConsumer, logger, tracerProvider)

			req, err := http.NewRequestWithContext(ctx, http.MethodGet, testCase.requestPath, nil)
			assert.NoError(t, err)

			responseRecorder := httptest.NewRecorder()

			if testCase.getDocumentCalled {
				eventsChan := make(pdfrender.CallbackEventChan)
				defer close(eventsChan)

				if testCase.getDocumentResult != nil {
					documentStore.
						EXPECT().
						Get(server.DocumentID(documentID)).
						Return(*testCase.getDocumentResult, eventsChan, true)
				} else {
					documentStore.
						EXPECT().
						Get(server.DocumentID(documentID)).
						Return(pdfrender.Document{}, nil, false)
				}
			}

			echoCtx := echo.New().NewContext(req, responseRecorder)
			echoCtx.SetPath("/templates/:template_name/:document_id")
			echoCtx.SetParamNames("template_name", "document_id")
			echoCtx.SetParamValues(templateName, testCase.requestDocumentID)

			err = handler.Get(echoCtx)

			assert.ErrorIs(t, err, testCase.expectedError)

			assert.Equal(t, testCase.expectedBody, responseRecorder.Body.Bytes())
		})
	}
}

func TestHandlers_GetAsset(t *testing.T) {
	t.Parallel()

	logger := log.NewLogger(log.LevelWarn, log.TextFormat)
	ctx := log.ContextWithLogger(context.TODO(), logger)

	tracerProvider := noopTrace.NewTracerProvider()

	templateName := "demo_template"
	templateNameTyped := templates.Name(templateName)
	assetPath := "styles.css"

	requestPath := fmt.Sprintf("/templates/%s/assets/%s", templateName, assetPath)

	type TestCase struct {
		name                   string
		templatesStoreConsumer func() templates.StoreConsumer
		requestLastModTime     *time.Time
		expectedError          error
		expectedBody           []byte
		expectedCode           int
	}

	type TemplateWithAssets struct {
		*templatesMocks.Template
		*templatesMocks.AssetsProvider
	}

	testCases := []TestCase{
		{
			name: "assets consumer not ready",
			templatesStoreConsumer: func() templates.StoreConsumer {
				templatesStoreConsumer := new(templatesMocks.StoreConsumer)

				templatesStoreConsumer.EXPECT().
					ConsumeTemplatesStore().
					Return(nil, new(templates.StoreProviderNotInitializedError))

				return templatesStoreConsumer
			},
			requestLastModTime: nil,
			expectedError:      echo.ErrInternalServerError,
			expectedBody:       nil,
			expectedCode:       http.StatusInternalServerError,
		},
		{
			name: "template not found",
			templatesStoreConsumer: func() templates.StoreConsumer {
				templatesStoreConsumer := new(templatesMocks.StoreConsumer)

				templatesStore := new(templatesMocks.Store)

				templatesStoreConsumer.EXPECT().
					ConsumeTemplatesStore().
					Return(templatesStore, nil)

				templatesStore.EXPECT().
					Fetch(templateNameTyped).
					Return(nil, templates.NewTemplateNotExistsError(templateNameTyped))

				return templatesStoreConsumer
			},
			requestLastModTime: nil,
			expectedError:      echo.ErrInternalServerError,
			expectedBody:       nil,
			expectedCode:       http.StatusInternalServerError,
		},
		{
			name: "template not have assets",
			templatesStoreConsumer: func() templates.StoreConsumer {
				templatesStoreConsumer := new(templatesMocks.StoreConsumer)
				templatesStore := new(templatesMocks.Store)
				template := new(templatesMocks.Template)

				templatesStoreConsumer.EXPECT().
					ConsumeTemplatesStore().
					Return(templatesStore, nil)

				templatesStore.EXPECT().
					Fetch(templateNameTyped).
					Return(template, nil)

				return templatesStoreConsumer
			},
			requestLastModTime: nil,
			expectedError:      echo.ErrInternalServerError,
			expectedBody:       nil,
			expectedCode:       http.StatusInternalServerError,
		},
		{
			name: "asset not found",
			templatesStoreConsumer: func() templates.StoreConsumer {
				templatesStoreConsumer := new(templatesMocks.StoreConsumer)
				templatesStore := new(templatesMocks.Store)
				template := new(templatesMocks.Template)
				assetsProvider := new(templatesMocks.AssetsProvider)
				templateWithAssets := TemplateWithAssets{
					template,
					assetsProvider,
				}
				assets := new(templatesMocks.Assets)

				templatesStoreConsumer.EXPECT().
					ConsumeTemplatesStore().
					Return(templatesStore, nil)

				templatesStore.EXPECT().
					Fetch(templateNameTyped).
					Return(templateWithAssets, nil)

				assetsProvider.EXPECT().GetAssets().Return(assets)
				assets.EXPECT().Open(assetPath).Return(nil, os.ErrNotExist)

				return templatesStoreConsumer
			},
			requestLastModTime: nil,
			expectedError:      echo.ErrNotFound,
			expectedBody:       nil,
			expectedCode:       http.StatusNotFound,
		},
		func() TestCase {
			assetPayload := []byte("valid asset")

			return TestCase{
				name: "open asset successful",
				templatesStoreConsumer: func() templates.StoreConsumer {
					templatesStoreConsumer := new(templatesMocks.StoreConsumer)
					templatesStore := new(templatesMocks.Store)
					template := new(templatesMocks.Template)
					assetsProvider := new(templatesMocks.AssetsProvider)
					templateWithAssets := TemplateWithAssets{
						template,
						assetsProvider,
					}
					assets := new(templatesMocks.Assets)

					templatesStoreConsumer.EXPECT().
						ConsumeTemplatesStore().
						Return(templatesStore, nil)

					templatesStore.EXPECT().
						Fetch(templateNameTyped).
						Return(templateWithAssets, nil)

					asset := &templates.Asset{
						Reader:  testutils.NewNoopCloser(bytes.NewReader(assetPayload)),
						ModTime: time.Now(),
					}

					assetsProvider.EXPECT().GetAssets().Return(assets)
					assets.EXPECT().Open(assetPath).Return(asset, nil)

					return templatesStoreConsumer
				},
				requestLastModTime: nil,
				expectedError:      nil,
				expectedBody:       assetPayload,
				expectedCode:       http.StatusOK,
			}
		}(),
		func() TestCase {
			assetPayload := []byte("valid asset")

			assetModTime := time.Date(2020, 1, 1, 0, 0, 0, 0, time.UTC)

			return TestCase{
				name: "cache asset successful",
				templatesStoreConsumer: func() templates.StoreConsumer {
					templatesStoreConsumer := new(templatesMocks.StoreConsumer)
					templatesStore := new(templatesMocks.Store)
					template := new(templatesMocks.Template)
					assetsProvider := new(templatesMocks.AssetsProvider)
					templateWithAssets := TemplateWithAssets{
						template,
						assetsProvider,
					}
					assets := new(templatesMocks.Assets)

					templatesStoreConsumer.EXPECT().
						ConsumeTemplatesStore().
						Return(templatesStore, nil)

					templatesStore.EXPECT().
						Fetch(templateNameTyped).
						Return(templateWithAssets, nil)

					asset := &templates.Asset{
						Reader:  testutils.NewNoopCloser(bytes.NewReader(assetPayload)),
						ModTime: assetModTime,
					}

					assetsProvider.EXPECT().GetAssets().Return(assets)
					assets.EXPECT().Open(assetPath).Return(asset, nil)

					return templatesStoreConsumer
				},
				requestLastModTime: &assetModTime,
				expectedError:      nil,
				expectedBody:       nil,
				expectedCode:       http.StatusNotModified,
			}
		}(),
	}

	for _, testCase := range testCases {
		t.Run(testCase.name, func(t *testing.T) {
			t.Parallel()

			templatesStoreConsumer := testCase.templatesStoreConsumer()

			handler := server.NewHandler(nil, templatesStoreConsumer, logger, tracerProvider)

			req, err := http.NewRequestWithContext(ctx, http.MethodGet, requestPath, nil)
			assert.NoError(t, err)

			if testCase.requestLastModTime != nil {
				req.Header.Add(echo.HeaderIfModifiedSince, testCase.requestLastModTime.Format(http.TimeFormat))
			}

			responseRecorder := httptest.NewRecorder()

			echoCtx := echo.New().NewContext(req, responseRecorder)
			echoCtx.SetPath("/templates/:template_name/assets/*")
			echoCtx.SetParamNames("template_name", "*")
			echoCtx.SetParamValues(templateName, assetPath)

			err = handler.GetAsset(echoCtx)

			assert.ErrorIs(t, err, testCase.expectedError)

			assert.Equal(t, testCase.expectedBody, responseRecorder.Body.Bytes())

			if testCase.expectedError == nil {
				assert.Equal(t, testCase.expectedCode, responseRecorder.Code)
			}
		})
	}
}
