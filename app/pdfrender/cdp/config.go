package cdp

import (
	"fmt"
	"time"

	"github.com/urfave/cli/v2"

	"gitlab.com/c0va23/pdf-server/app/pdfrender"
)

// Default values for RenderConfig.
const (
	DefaultRenderTimeout = time.Second * 30
	defaultRetryAttempts = 3
)

// RenderConfig for BrowserRender.
type RenderConfig struct {
	WaitLifecycleEvent pdfrender.WaitLifecycleEvent
	RenderTimeout      time.Duration
	RetryAttempts      int
}

// DefaultRenderConfig return RenderConfig with default values.
func DefaultRenderConfig() RenderConfig {
	return RenderConfig{
		WaitLifecycleEvent: pdfrender.DefaultWaitLifecycleEvent,
		RenderTimeout:      DefaultRenderTimeout,
		RetryAttempts:      defaultRetryAttempts,
	}
}

// BindFlags binds flags to config.
func (config *RenderConfig) BindFlags() []cli.Flag {
	return []cli.Flag{
		&cli.GenericFlag{
			Name:    "wait-lifecycle-event",
			EnvVars: []string{"WAIT_LIFECYCLE_EVENT"},
			Value:   &config.WaitLifecycleEvent,
			Usage: fmt.Sprintf(
				"The name of the page event that will be expected before the PDF "+
					"print command is invoked. Allowed values: %s.",
				pdfrender.WaitLifecycleEvents,
			),
			Hidden: false,
		},
		&cli.DurationFlag{
			Name:        "render-timeout",
			EnvVars:     []string{"RENDER_TIMEOUT"},
			Value:       config.RenderTimeout,
			Destination: &config.RenderTimeout,
			Aliases:     []string{},
			Usage:       "Max render duration",
			FilePath:    "",
			Required:    false,
			Hidden:      false,
			DefaultText: "",
			HasBeenSet:  false,
		},
		&cli.IntFlag{
			Name:        "render-retry-attempts",
			EnvVars:     []string{"RENDER_RETRY_ATTEMPTS"},
			Value:       config.RetryAttempts,
			Required:    false,
			Destination: &config.RetryAttempts,
			Hidden:      false,
			Usage:       "Number of attempts to render",
			Aliases:     []string{},
			FilePath:    "",
			DefaultText: "",
			HasBeenSet:  false,
		},
	}
}
