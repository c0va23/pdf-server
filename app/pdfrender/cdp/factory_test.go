package cdp_test

import (
	"context"
	"testing"

	pool "github.com/jolestar/go-commons-pool/v2"
	"github.com/stretchr/testify/assert"

	"gitlab.com/c0va23/pdf-server/app/log"
	"gitlab.com/c0va23/pdf-server/app/pdfrender/cdp"
)

func TestBrowserFactory(t *testing.T) {
	t.Parallel()

	for _, factoryType := range cdp.FactoryTypeList() {
		testTestBrowserFactoryType(t, factoryType)
	}
}

func testTestBrowserFactoryType(t *testing.T, factoryType cdp.FactoryType) {
	t.Helper()

	runnerConfig := getBrowserRunnerConfig()
	if runnerConfig == nil {
		t.Skip("Browser not found")
	}

	t.Run(factoryType.String(), func(t *testing.T) {
		t.Parallel()

		var (
			logger = log.NewLogger(log.LevelFatal, log.TextFormat)

			ctx = context.TODO()

			destroyObject = func(t *testing.T, factory *cdp.Factory, object *pool.PooledObject) {
				t.Helper()

				destroyErr := factory.DestroyObject(ctx, object)
				assert.NoError(t, destroyErr)
			}
		)

		t.Run(factoryType.String(), func(t *testing.T) {
			t.Parallel()

			t.Run("MakeObject/DestroyObject", func(t *testing.T) {
				t.Parallel()

				factory, closeFactory, err := makeBrowserFactory(ctx, logger, *runnerConfig, factoryType)
				assert.NoError(t, err)

				defer closeFactory()

				obj, err := factory.MakeObject(ctx)
				assert.NoError(t, err)

				defer destroyObject(t, factory, obj)

				assert.NotNil(t, obj.Object)
			})

			t.Run("ValidateObject", func(t *testing.T) {
				t.Parallel()

				factory, closeFactory, err := makeBrowserFactory(ctx, logger, *runnerConfig, factoryType)
				assert.NoError(t, err)

				defer closeFactory()

				obj, err := factory.MakeObject(ctx)
				assert.NoError(t, err)

				defer destroyObject(t, factory, obj)

				valid := factory.ValidateObject(ctx, obj)

				assert.True(t, valid)
			})

			t.Run("ActivateObject", func(t *testing.T) {
				t.Parallel()

				factory, closeFactory, err := makeBrowserFactory(ctx, logger, *runnerConfig, factoryType)
				assert.NoError(t, err)

				defer closeFactory()

				obj, err := factory.MakeObject(ctx)
				assert.NoError(t, err)
				defer destroyObject(t, factory, obj)

				activateErr := factory.ActivateObject(ctx, obj)
				assert.NoError(t, activateErr)
			})
			t.Run("PassivateObject", func(t *testing.T) {
				t.Parallel()

				factory, closeFactory, err := makeBrowserFactory(ctx, logger, *runnerConfig, factoryType)
				assert.NoError(t, err)

				defer closeFactory()

				obj, err := factory.MakeObject(ctx)
				assert.NoError(t, err)

				defer destroyObject(t, factory, obj)

				passivateErr := factory.PassivateObject(ctx, obj)
				assert.NoError(t, passivateErr)
			})
		})
	})
}
