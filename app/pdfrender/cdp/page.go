package cdp

import (
	"context"
	"fmt"

	fastBase64 "github.com/cristalhq/base64"
	"github.com/mafredri/cdp"
	"github.com/mafredri/cdp/protocol/io"
	"github.com/mafredri/cdp/protocol/page"
	"github.com/mafredri/cdp/rpcc"
	"go.opentelemetry.io/otel/codes"
	"go.opentelemetry.io/otel/trace"
)

// Page alias of cdp.Page for mocks.
type Page interface {
	Enable(context.Context) error
	Disable(context.Context) error
	Reload(context.Context, *page.ReloadArgs) error
	SetLifecycleEventsEnabled(context.Context, *page.SetLifecycleEventsEnabledArgs) error
	LifecycleEvent(context.Context) (page.LifecycleEventClient, error)
	PrintToPDF(context.Context, *page.PrintToPDFArgs) (*page.PrintToPDFReply, error)
	Navigate(context.Context, *page.NavigateArgs) (*page.NavigateReply, error)
	GetNavigationHistory(context.Context) (*page.GetNavigationHistoryReply, error)
}

// TracedPage wrap cdp.Page with tracing.
type TracedPage struct {
	page   cdp.Page
	conn   *rpcc.Conn
	tracer trace.Tracer
}

// TracePage wrap cdp.Page.
func TracePage(cdpPage cdp.Page, conn *rpcc.Conn, tracerProvider trace.TracerProvider) *TracedPage {
	tracer := tracerProvider.Tracer("TracedPage")

	return &TracedPage{
		page:   cdpPage,
		conn:   conn,
		tracer: tracer,
	}
}

// Enable wraps cdp.Page.Enable with span.
func (tracedPage *TracedPage) Enable(ctx context.Context) error {
	ctxWithSpan, span := tracedPage.tracer.Start(ctx, "Enable")
	defer span.End()

	if err := tracedPage.page.Enable(ctxWithSpan); err != nil {
		span.SetStatus(codes.Error, err.Error())

		return tracedPage.wrapTraceError(err)
	}

	return nil
}

// Disable wraps cdp.Page.Disable with span.
func (tracedPage *TracedPage) Disable(ctx context.Context) error {
	ctxWithSpan, span := tracedPage.tracer.Start(ctx, "Disable")
	defer span.End()

	if err := tracedPage.page.Disable(ctxWithSpan); err != nil {
		span.SetStatus(codes.Error, err.Error())

		return tracedPage.wrapTraceError(err)
	}

	return nil
}

// Reload wraps cdp.Page.Reload with span.
func (tracedPage *TracedPage) Reload(ctx context.Context, args *page.ReloadArgs) error {
	ctxWithSpan, span := tracedPage.tracer.Start(ctx, "Reload")
	defer span.End()

	if err := tracedPage.page.Reload(ctxWithSpan, args); err != nil {
		span.SetStatus(codes.Error, err.Error())

		return tracedPage.wrapTraceError(err)
	}

	return nil
}

// GetNavigationHistory wraps cdp.Page.GetNavigationHistory with span.
func (tracedPage *TracedPage) GetNavigationHistory(
	ctx context.Context,
) (*page.GetNavigationHistoryReply, error) {
	ctxWithSpan, span := tracedPage.tracer.Start(ctx, "GetNavigationHistory")
	defer span.End()

	reply, err := tracedPage.page.GetNavigationHistory(ctxWithSpan)
	if err != nil {
		span.SetStatus(codes.Error, err.Error())

		return nil, tracedPage.wrapTraceError(err)
	}

	return reply, nil
}

// SetLifecycleEventsEnabled wraps cdp.Page.SetLifecycleEventsEnabled with span.
func (tracedPage *TracedPage) SetLifecycleEventsEnabled(
	ctx context.Context,
	args *page.SetLifecycleEventsEnabledArgs,
) error {
	ctxWithSpan, span := tracedPage.tracer.Start(ctx, "SetLifecycleEventsEnabled")
	defer span.End()

	if err := tracedPage.page.SetLifecycleEventsEnabled(ctxWithSpan, args); err != nil {
		span.SetStatus(codes.Error, err.Error())

		return tracedPage.wrapTraceError(err)
	}

	return nil
}

// LifecycleEvent wraps cdp.Page.LifecycleEvent with span.
func (tracedPage *TracedPage) LifecycleEvent(
	ctx context.Context,
) (page.LifecycleEventClient, error) {
	ctxWithSpan, span := tracedPage.tracer.Start(ctx, "LifecycleEvent")
	defer span.End()

	client, err := tracedPage.page.LifecycleEvent(ctxWithSpan)
	if err != nil {
		span.SetStatus(codes.Error, err.Error())

		return nil, tracedPage.wrapTraceError(err)
	}

	return client, nil
}

type fastBase64Payload struct {
	buf []byte
}

const (
	quotePadding = len(`"`)
)

// UnmarshalJSON implment Unmarshaler interface.
func (payload *fastBase64Payload) UnmarshalJSON(value []byte) (err error) {
	unquotedValueLen := len(value) - quotePadding
	unquotedValue := value[quotePadding:unquotedValueLen]

	payload.buf, err = fastBase64.StdEncoding.DecodeToBytes(unquotedValue)
	if err != nil {
		return fmt.Errorf("fastBase64: %w", err)
	}

	return nil
}

type customPrintPdfReplay struct {
	Data   fastBase64Payload `json:"data"`
	Stream *io.StreamHandle  `json:"stream,omitempty"`
}

// PrintToPDF wraps cdp.Page.PrintToPDF with span.
func (tracedPage *TracedPage) PrintToPDF(
	ctx context.Context,
	args *page.PrintToPDFArgs,
) (*page.PrintToPDFReply, error) {
	ctxWithSpan, span := tracedPage.tracer.Start(ctx, "PrintToPDF")
	defer span.End()

	customReply := new(customPrintPdfReplay)

	err := rpcc.Invoke(ctxWithSpan, "Page.printToPDF", args, customReply, tracedPage.conn)
	if err != nil {
		span.SetStatus(codes.Error, err.Error())

		return nil, tracedPage.wrapTraceError(err)
	}

	reply := &page.PrintToPDFReply{
		Data:   customReply.Data.buf,
		Stream: customReply.Stream,
	}

	return reply, nil
}

// Navigate wraps cdp.Page.Navigate with span.
func (tracedPage *TracedPage) Navigate(
	ctx context.Context,
	args *page.NavigateArgs,
) (*page.NavigateReply, error) {
	ctxWithSpan, span := tracedPage.tracer.Start(ctx, "Navigate")
	defer span.End()

	reply, err := tracedPage.page.Navigate(ctxWithSpan, args)
	if err != nil {
		span.SetStatus(codes.Error, err.Error())

		return nil, tracedPage.wrapTraceError(err)
	}

	return reply, nil
}

func (*TracedPage) wrapTraceError(err error) error {
	return fmt.Errorf("trace: %w", err)
}
