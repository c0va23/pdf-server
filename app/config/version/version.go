package version

import (
	"fmt"
	"runtime"
)

// Version returned by --version flag.
var (
	Version = "0.0.1" //nolint:gochecknoglobals
)

// Info container.
type Info struct {
	Version string `json:"version"`
	OS      string `json:"os"`
	Arch    string `json:"arch"`
}

func (info Info) String() string {
	return fmt.Sprintf("%s (%s/%s)", info.Version, info.OS, info.Arch)
}

// GetInfo collect Info.
func GetInfo() Info {
	return Info{
		Version: Version,
		OS:      runtime.GOOS,
		Arch:    runtime.GOARCH,
	}
}
