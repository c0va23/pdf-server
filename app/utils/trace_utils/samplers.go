package traceUtils

import (
	sdkTrace "go.opentelemetry.io/otel/sdk/trace"
)

type excludeTraceSampler struct {
	excludeSpanNamesSet map[string]struct{}
	delegate            sdkTrace.Sampler
}

func newExcludeTraceSampler(
	delegate sdkTrace.Sampler,
	excludeSpanNames ...string,
) excludeTraceSampler {
	excludeSpanNameSet := make(map[string]struct{}, len(excludeSpanNames))

	for _, spanName := range excludeSpanNames {
		excludeSpanNameSet[spanName] = struct{}{}
	}

	return excludeTraceSampler{
		excludeSpanNamesSet: excludeSpanNameSet,
		delegate:            delegate,
	}
}

// ShouldSample returns a sampling result.
func (sampler excludeTraceSampler) ShouldSample(
	params sdkTrace.SamplingParameters,
) sdkTrace.SamplingResult {
	if _, isExcluded := sampler.excludeSpanNamesSet[params.Name]; isExcluded {
		return sdkTrace.SamplingResult{
			Decision: sdkTrace.Drop,
		}
	}

	return sampler.delegate.ShouldSample(params)
}

// Description returns a description of the sampler.
func (excludeTraceSampler) Description() string {
	return "Drop spans with names"
}
