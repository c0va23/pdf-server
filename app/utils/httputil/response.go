package httputil

import (
	"strconv"

	"github.com/labstack/echo/v4"
)

// SetContentLength set content length header.
func SetContentLength(c echo.Context, content []byte) {
	c.Response().Header().Set(
		echo.HeaderContentLength,
		strconv.Itoa(len(content)),
	)
}
