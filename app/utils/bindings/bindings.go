package bindings

import (
	"fmt"
	"strings"
	"time"
)

// Duration bind duration to flag.
type Duration struct {
	value *time.Duration
}

// NewDuration return duration binding.
func NewDuration(innerDuration *time.Duration) *Duration {
	return &Duration{
		value: innerDuration,
	}
}

// Set parser and write internal duration.
func (duration *Duration) Set(str string) error {
	value, err := time.ParseDuration(str)
	if nil != err {
		return fmt.Errorf("parse duration: %w", err)
	}

	*duration.value = value

	return nil
}

// String return duration string.
func (duration *Duration) String() string {
	if duration == nil {
		return "duration is nil"
	}

	if duration.value == nil {
		return "duration value is nil"
	}

	return duration.value.String()
}

const stringSliceSeparator = ","

// StringSlice bind string slice to flag.
type StringSlice struct {
	slice *[]string
}

// NewStringSlice return string slice binding.
func NewStringSlice(slice *[]string) *StringSlice {
	return &StringSlice{
		slice: slice,
	}
}

// Set split value by comma and assign to inner slice.
//
//nolint:unparam // implements github.com/urfave/cli/v2.Generic interface.
func (stringSlice *StringSlice) Set(value string) error {
	values := strings.Split(value, stringSliceSeparator)
	*stringSlice.slice = values

	return nil
}

// String return joined by comma slice.
func (stringSlice *StringSlice) String() string {
	if stringSlice == nil {
		return "string slice is nil"
	}

	if stringSlice.slice == nil {
		return "string slice value is nil"
	}

	return strings.Join(*stringSlice.slice, stringSliceSeparator)
}
