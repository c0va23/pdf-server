package testutils

import "io"

// NoopCloser wrap io.ReadSeeker and add noop Close implementation.
type NoopCloser[T io.ReadSeeker] struct {
	reader T
}

// NewNoopCloser constructor.
func NewNoopCloser[T io.ReadSeeker](reader T) NoopCloser[T] {
	return NoopCloser[T]{
		reader: reader,
	}
}

// Read implements io.Reader.
//
//nolint:wrapcheck
func (closer NoopCloser[T]) Read(buf []byte) (int, error) {
	return closer.reader.Read(buf)
}

// Seek implements io.Seeker.
//
//nolint:wrapcheck
func (closer NoopCloser[T]) Seek(offset int64, wince int) (int64, error) {
	return closer.reader.Seek(offset, wince)
}

// Close implements io.Closer.
func (NoopCloser[T]) Close() error {
	return nil
}
