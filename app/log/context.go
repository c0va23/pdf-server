package log

import (
	"context"

	"github.com/sirupsen/logrus"
)

type contextKey string

// Context keys.
const (
	contextKeyLogger = contextKey("logger")

	ContextKeyRequestID = contextKey("requestID")
)

// LoggerFromContext fetch logger from context.
func LoggerFromContext(ctx context.Context, loggerName string) Logger {
	logger := ctx.Value(contextKeyLogger).(Logger)
	logger = logger.WithField(LoggerField, loggerName)

	return logger
}

// ContextWithLogger create new context with logger.
func ContextWithLogger(ctx context.Context, logger Logger) context.Context {
	return context.WithValue(ctx, contextKeyLogger, logger)
}

// WithLoggerFields create new context with logger with fields.
func WithLoggerFields(ctx context.Context, fields logrus.Fields) context.Context {
	logger := LoggerFromContext(ctx, "").WithFields(fields)

	return ContextWithLogger(ctx, logger)
}
