package log

import (
	"fmt"

	"github.com/urfave/cli/v2"
)

// Config  defines the configuration of the logger.
type Config struct {
	Level  Level
	Format Format
}

// DefaultConfig returns the default config.
func DefaultConfig() Config {
	return Config{
		Level:  LevelInfo,
		Format: TextFormat,
	}
}

// BindFlags binds the flags to the config.
func (config *Config) BindFlags() []cli.Flag {
	return []cli.Flag{
		&cli.GenericFlag{
			Name:      "log-level",
			Value:     &config.Level,
			EnvVars:   []string{"LOG_LEVEL"},
			Usage:     fmt.Sprintf("Logging level. Available values: %s", AvailableLevels()),
			Hidden:    false,
			Required:  false,
			Aliases:   []string{},
			TakesFile: false,
		},
		&cli.GenericFlag{
			Name:      "log-format",
			Value:     &config.Format,
			EnvVars:   []string{"LOG_FORMAT"},
			Usage:     fmt.Sprintf("Logging format. Available values: %s", AvailableFormats()),
			Hidden:    false,
			Required:  false,
			Aliases:   []string{},
			TakesFile: false,
		},
	}
}
