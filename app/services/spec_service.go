package services

import (
	"errors"
	"fmt"

	"gitlab.com/c0va23/pdf-server/app/templates"
)

// JSONSchema define marshalable JSON schema instance.
type JSONSchema interface {
	MarshalJSON() ([]byte, error)
}

// TemplateSpec for building API specification.
type TemplateSpec struct {
	Name        string
	Schema      JSONSchema
	Examples    []ExampleSpec
	IsComposite bool
}

// ExampleSpec for building API specification.
type ExampleSpec struct {
	Name string
}

// SpecService build internal representation of API specification.
type SpecService interface {
	GetSpec() ([]TemplateSpec, error)
	GetTemplateSchema(templateName string) (JSONSchema, error)
}

// TemplateSpecService build API spec from templates.Store.
type TemplateSpecService struct {
	templatesStore templates.Store
}

// NewTemplateSpecService construct TemplateSpecService instance.
func NewTemplateSpecService(templatesStore templates.Store) *TemplateSpecService {
	return &TemplateSpecService{
		templatesStore: templatesStore,
	}
}

// GetSpec return API specification from templates.Store.
func (service *TemplateSpecService) GetSpec() ([]TemplateSpec, error) {
	templateList, err := service.templatesStore.List()
	if err != nil {
		return nil, fmt.Errorf("list templates: %w", err)
	}

	templateMetadataList := make([]TemplateSpec, 0, len(templateList))

	for _, template := range templateList {
		examples := template.GetExamples()
		exampleMetadataList := make([]ExampleSpec, 0, len(examples))

		for exampleName := range examples {
			exampleMetadataList = append(exampleMetadataList, ExampleSpec{
				Name: exampleName,
			})
		}

		_, isComposite := template.(templates.CompositeTemplate)

		templateMetadataList = append(templateMetadataList, TemplateSpec{
			Name:        string(template.GetName()),
			Examples:    exampleMetadataList,
			Schema:      template.GetValidator(),
			IsComposite: isComposite,
		})
	}

	return templateMetadataList, nil
}

// GetTemplateSchema return template spec from templates.Store.
func (service *TemplateSpecService) GetTemplateSchema(templateName string) (JSONSchema, error) {
	template, err := service.templatesStore.Fetch(templates.Name(templateName))
	if notFound := new(templates.TemplateNotExistsError); errors.As(err, &notFound) {
		return nil, NewTemplateNotFound(err)
	} else if err != nil {
		return nil, fmt.Errorf("template not found: %w", err)
	}

	return template.GetValidator(), nil
}
