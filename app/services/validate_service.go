package services

import (
	"context"
	"fmt"

	"gitlab.com/c0va23/pdf-server/app/log"
	"gitlab.com/c0va23/pdf-server/app/templates"
)

// ValidateService interface.
type ValidateService interface {
	Validate(
		ctx context.Context,
		filter []string,
	) error
}

const validateServiceLogger = "validator"

// SimpleValidateService parse templates and render all examples.
type SimpleValidateService struct {
	templatesStore templates.Store
}

// NewSimpleValidateService constructor.
func NewSimpleValidateService(
	templatesStore templates.Store,
) *SimpleValidateService {
	return &SimpleValidateService{
		templatesStore,
	}
}

// Validate iterate over all templates and render all examples. Return false
// if any render of example return error.
func (validateService *SimpleValidateService) Validate(
	ctx context.Context,
	filter []string,
) error {
	templateList, err := validateService.templatesStore.List()
	if nil != err {
		return fmt.Errorf("list templates: %w", err)
	}

	for _, template := range templateList {
		if err := validateService.validateTemplate(ctx, filter, template); err != nil {
			return fmt.Errorf("validate template examples: %w", err)
		}
	}

	return nil
}

func (*SimpleValidateService) validateTemplate(
	ctx context.Context,
	filter []string,
	template templates.Template,
) error {
	logger := log.LoggerFromContext(ctx, validateServiceLogger).
		WithField(log.FieldTemplateName, template.GetName())

	for exampleName, exampleData := range template.GetExamples() {
		if isSkipped(filter, template, exampleName) {
			continue
		}

		_, err := template.Render(
			ctx,
			exampleData,
		).Await().Get()
		if nil != err {
			return fmt.Errorf("render example %s/%s: %w", template.GetName(), exampleName, err)
		}

		logger.
			WithField(log.FieldTemplateExample, exampleName).
			Info("Success render template")
	}

	return nil
}

const templateExampleSeparator = "/"

func isSkipped(filter []string, template templates.Template, exampleName string) bool {
	if len(filter) == 0 {
		return false
	}

	targetPair := string(template.GetName()) + templateExampleSeparator + exampleName

	for _, filterPair := range filter {
		if filterPair == targetPair {
			return false
		}
	}

	return true
}
