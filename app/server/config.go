package server

import (
	cliV2 "github.com/urfave/cli/v2"
)

// StaticDirConfig define path and enabling to static dir.
type StaticDirConfig struct {
	Path    string
	Enabled bool
}

// DefaultStaticDirConfig provider.
func DefaultStaticDirConfig() StaticDirConfig {
	return StaticDirConfig{
		Path:    "./app/server/static/",
		Enabled: false,
	}
}

// Flags return config bindings for cli.
func (config *StaticDirConfig) Flags() []cliV2.Flag {
	return []cliV2.Flag{
		&cliV2.BoolFlag{
			Name:        "static-dir-enabled",
			Usage:       "Read static from dir instead embedded",
			Destination: &config.Enabled,
			Value:       config.Enabled,
			Hidden:      false,
			Aliases:     []string{},
			EnvVars:     []string{"STATIC_DIR_ENABLED"},
			FilePath:    "",
			Required:    false,
			HasBeenSet:  false,
			DefaultText: "",
		},
		&cliV2.StringFlag{
			Name:        "static-dir-path",
			Usage:       "Path to static dir",
			Destination: &config.Path,
			Value:       config.Path,
			Aliases:     []string{},
			EnvVars:     []string{"STATIC_DIR_PATH"},
			FilePath:    "",
			Required:    false,
			Hidden:      false,
			HasBeenSet:  false,
			DefaultText: "",
			TakesFile:   false,
		},
	}
}
