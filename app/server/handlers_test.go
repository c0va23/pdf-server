package server_test

import (
	"bytes"
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"net/http"
	"net/http/httptest"
	"testing"

	echo "github.com/labstack/echo/v4"
	"github.com/stretchr/testify/assert"

	"gitlab.com/c0va23/pdf-server/app/log"
	"gitlab.com/c0va23/pdf-server/app/server"
	"gitlab.com/c0va23/pdf-server/app/services"
	"gitlab.com/c0va23/pdf-server/app/templates"

	servicesMocks "gitlab.com/c0va23/pdf-server/mocks/services"
)

func TestHandlers_RenderData(t *testing.T) {
	t.Parallel()

	const (
		templateName = "template_name"
	)

	var (
		logger = log.NewLogger(log.LevelFatal, log.TextFormat)
		ctx    = log.ContextWithLogger(context.TODO(), logger)

		validData = services.Data{
			"key": "value",
		}

		validPayload = []byte("PDF Payload")
	)

	type TestCase struct {
		name            string
		renderData      services.Data
		renderedError   error
		renderedPayload []byte
		expectedError   error
	}

	testCases := []TestCase{
		{
			name:            "empty request body",
			renderData:      nil,
			renderedError:   nil,
			renderedPayload: nil,
			expectedError:   echo.NewHTTPError(http.StatusBadRequest, "not empty body required"),
		},
		func() TestCase {
			templateNotFound := services.NewTemplateNotFound(
				templates.NewTemplateNotExistsError(templateName),
			)

			return TestCase{
				name:            "template not found",
				renderData:      validData,
				renderedError:   templateNotFound,
				renderedPayload: nil,
				expectedError:   echo.NewHTTPError(http.StatusNotFound, templateNotFound),
			}
		}(),
		{
			name:            "invalid body",
			renderData:      validData,
			renderedPayload: nil,
			renderedError: services.NewDataValidationError(
				templates.NewInvalidDataSchemaError(errors.New("json schema error"))),
			expectedError: echo.NewHTTPError(
				http.StatusUnprocessableEntity,
				"invalid data schema: json schema error",
			),
		},
		{
			name:            "success",
			renderData:      validData,
			renderedPayload: validPayload,
			renderedError:   nil,
			expectedError:   nil,
		},
	}

	for _, testCase := range testCases {
		t.Run(testCase.name, func(t *testing.T) {
			t.Parallel()

			pdfRenderService := new(servicesMocks.PDFRenderService)
			defer pdfRenderService.AssertExpectations(t)

			statsService := new(servicesMocks.StatsService)
			specService := new(servicesMocks.SpecService)

			handlers := server.NewHandlers(pdfRenderService, statsService, specService)

			var requestBody io.Reader

			if testCase.renderData != nil {
				requestBuffer := new(bytes.Buffer)
				if err := json.NewEncoder(requestBuffer).Encode(testCase.renderData); err != nil {
					panic(err)
				}

				requestBody = bytes.NewBuffer(requestBuffer.Bytes())
			}

			req, _ := http.NewRequestWithContext(ctx, http.MethodPost, "/render/"+templateName, requestBody)
			req.Header.Add(echo.HeaderContentType, echo.MIMEApplicationJSON)

			res := httptest.NewRecorder()

			echoCtx := echo.New().NewContext(req, res)
			echoCtx.SetPath("/render/:template_name")
			echoCtx.SetParamNames("template_name")
			echoCtx.SetParamValues(templateName)

			if testCase.renderedError != nil || testCase.renderedPayload != nil {
				pdfRenderService.EXPECT().RenderPDFWithData(
					echoCtx.Request().Context(),
					templateName,
					testCase.renderData,
				).Return(
					testCase.renderedPayload,
					testCase.renderedError,
				)
			}

			//nolint:contextcheck
			actualErr := handlers.RenderData(echoCtx)

			if testCase.expectedError == nil {
				assert.Nil(t, actualErr)
				assert.Equal(t, testCase.renderedPayload, res.Body.Bytes())
			} else {
				assert.Equal(t, testCase.expectedError, actualErr)
			}
		})
	}
}

func TestHandlers_RenderExample(t *testing.T) {
	t.Parallel()

	var (
		templateName = "template_name"
		exampleName  = "simple"

		logger = log.NewLogger(log.LevelFatal, log.TextFormat)
		ctx    = log.ContextWithLogger(context.TODO(), logger)
	)

	type TestCase struct {
		name          string
		renderPayload []byte
		renderError   error
		expectedError error
	}

	testCases := []TestCase{
		func() TestCase {
			templateNotFound := services.NewTemplateNotFound(
				templates.NewTemplateNotExistsError(templates.Name(templateName)),
			)

			return TestCase{
				name:          "template not found",
				renderPayload: nil,
				renderError:   templateNotFound,
				expectedError: echo.NewHTTPError(http.StatusNotFound, templateNotFound),
			}
		}(),
		func() TestCase {
			exampleNotFound := services.NewExampleNotFoundError(exampleName)

			return TestCase{
				name:          "example not found",
				renderPayload: nil,
				renderError:   exampleNotFound,
				expectedError: echo.NewHTTPError(http.StatusNotFound, exampleNotFound),
			}
		}(),
		{
			name:          "invalid data",
			renderPayload: nil,
			renderError: services.NewDataValidationError(
				templates.NewInvalidDataSchemaError(errors.New("json schema error"))),
			expectedError: echo.NewHTTPError(
				http.StatusUnprocessableEntity,
				"invalid data schema: json schema error",
			),
		},
		{
			name:          "success",
			renderPayload: []byte("PDF Payload"),
			renderError:   nil,
			expectedError: nil,
		},
	}

	for _, testCase := range testCases {
		t.Run(testCase.name, func(t *testing.T) {
			t.Parallel()

			pdfRenderService := new(servicesMocks.PDFRenderService)
			defer pdfRenderService.AssertExpectations(t)

			statsService := new(servicesMocks.StatsService)
			schemaService := new(servicesMocks.SpecService)

			handlers := server.NewHandlers(pdfRenderService, statsService, schemaService)

			reqPath := fmt.Sprintf("/templates/%s/examples/%s", templateName, exampleName)
			req, _ := http.NewRequestWithContext(ctx, http.MethodGet, reqPath, nil)
			req.Header.Add(echo.HeaderContentType, echo.MIMEApplicationJSON)

			res := httptest.NewRecorder()

			echoCtx := echo.New().NewContext(req, res)
			echoCtx.SetPath("/templates/:template_name/examples/:example_name")
			echoCtx.SetParamNames("template_name", "example_name")
			echoCtx.SetParamValues(templateName, exampleName)

			pdfRenderService.EXPECT().RenderPDFExample(
				echoCtx.Request().Context(),
				templateName,
				exampleName,
			).Return(testCase.renderPayload, testCase.renderError)

			//nolint:contextcheck
			actualErr := handlers.RenderExample(echoCtx)

			assert.Equal(t, testCase.expectedError, actualErr)
			assert.Equal(t, testCase.renderPayload, res.Body.Bytes())
		})
	}
}
