package server

import (
	"errors"
	"fmt"
	"net/http"
	"net/url"

	echo "github.com/labstack/echo/v4"

	"gitlab.com/c0va23/pdf-server/app/config/version"
	"gitlab.com/c0va23/pdf-server/app/log"
	"gitlab.com/c0va23/pdf-server/app/services"
	"gitlab.com/c0va23/pdf-server/app/utils/httputil"
)

const handlerLogger = "handler"

const (
	pdfContentType  = "application/pdf"
	jsonContentType = "application/json"
)

const (
	templateNameParam = "template_name"
)

// Handlers render template to PDF.
type Handlers struct {
	pdfRenderService services.PDFRenderService
	statsService     services.StatsService
	specService      services.SpecService
	binding          *echo.DefaultBinder
}

// NewHandlers constructor.
func NewHandlers(
	pdfRenderService services.PDFRenderService,
	statsService services.StatsService,
	specService services.SpecService,
) *Handlers {
	return &Handlers{
		pdfRenderService: pdfRenderService,
		statsService:     statsService,
		specService:      specService,
		binding:          new(echo.DefaultBinder),
	}
}

func (*Handlers) writePDF(
	c echo.Context,
	pdfData []byte,
) error {
	httputil.SetContentLength(c, pdfData)

	return c.Blob(http.StatusOK, pdfContentType, pdfData)
}

func (*Handlers) buildError(err error) error {
	if targetError := new(services.DataValidationError); errors.As(err, &targetError) {
		return echo.NewHTTPError(http.StatusUnprocessableEntity, targetError.Unwrap().Error())
	}

	if targetError := new(services.TemplateNotFoundError); errors.As(err, &targetError) {
		return echo.NewHTTPError(http.StatusNotFound, targetError)
	}

	if targetError := new(services.ExampleNotFoundError); errors.As(err, &targetError) {
		return echo.NewHTTPError(http.StatusNotFound, targetError)
	}

	return echo.ErrInternalServerError
}

// RenderData handler.
func (handlers *Handlers) RenderData(c echo.Context) error {
	requestCtx := c.Request().Context()

	templateName, err := extractTemplateName(c)
	if err != nil {
		return echo.NewHTTPError(http.StatusBadRequest, err)
	}

	logger := log.LoggerFromContext(requestCtx, handlerLogger).
		WithField(log.FieldTemplateName, templateName)

	logger.Debug("Template request")

	if c.Request().ContentLength == 0 {
		logger.Error("Body empty")

		return echo.NewHTTPError(http.StatusBadRequest, "not empty body required")
	}

	templateData := map[string]interface{}{}
	//nolint:govet // ignore shadow
	if err := handlers.binding.BindBody(c, &templateData); err != nil {
		logger.WithError(err).Error("Json error")

		return echo.NewHTTPError(http.StatusBadRequest, fmt.Sprintf("invalid json: %s", err))
	}

	pdfData, err := handlers.pdfRenderService.RenderPDFWithData(requestCtx, templateName, templateData)
	if err != nil {
		logger.WithError(err).Error("Render PDF with data error")

		return handlers.buildError(err)
	}

	logger.Info("Template response success")

	return handlers.writePDF(c, pdfData)
}

// RenderExample handler.
func (handlers *Handlers) RenderExample(c echo.Context) error {
	requestCtx := c.Request().Context()

	templateName, err := extractTemplateName(c)
	if err != nil {
		return echo.NewHTTPError(http.StatusBadRequest, err)
	}

	exampleName := c.Param("example_name")

	logger := log.LoggerFromContext(requestCtx, handlerLogger).
		WithField(log.FieldTemplateName, templateName).
		WithField(log.FieldTemplateExample, exampleName)

	logger.Debug("Example request")

	pdfData, err := handlers.pdfRenderService.RenderPDFExample(requestCtx, templateName, exampleName)
	if err != nil {
		logger.WithError(err).Error("Render PDF example error")

		return handlers.buildError(err)
	}

	logger.Info("Example response success")

	return handlers.writePDF(c, pdfData)
}

type renderTask struct {
	TemplateName string `json:"template_name"`
	Data         any    `json:"data"`
}

// PreviewRenderPlan accept template name and data and return render tasks.
func (handlers *Handlers) PreviewRenderPlan(c echo.Context) error {
	requestCtx := c.Request().Context()

	templateName, err := extractTemplateName(c)
	if err != nil {
		return echo.NewHTTPError(http.StatusBadRequest, err)
	}

	logger := log.LoggerFromContext(requestCtx, handlerLogger).
		WithField(log.FieldTemplateName, templateName)

	logger.Debug("Preview render plan request")

	if c.Request().ContentLength == 0 {
		logger.Error("Body empty")

		return echo.NewHTTPError(http.StatusBadRequest, "not empty body required")
	}

	templateData := map[string]interface{}{}
	//nolint:govet // ignore shadow
	if err := handlers.binding.BindBody(c, &templateData); err != nil {
		logger.WithError(err).Error("Json error")

		return echo.NewHTTPError(http.StatusBadRequest, fmt.Sprintf("invalid json: %s", err))
	}

	renderPlan, err := handlers.pdfRenderService.PreviewRenderPlan(
		requestCtx,
		templateName,
		templateData,
	)
	if err != nil {
		logger.WithError(err).Error("Preview render plan error")

		return handlers.buildError(err)
	}

	renderPlanJSON := make([]renderTask, 0, len(renderPlan))
	for _, task := range renderPlan {
		renderPlanJSON = append(renderPlanJSON, renderTask{
			TemplateName: task.TemplateName,
			Data:         task.Data,
		})
	}

	logger.Info("Preview render plan response success")

	return c.JSON(http.StatusOK, renderPlanJSON)
}

// GetExamplesData return examples data.
func (handlers *Handlers) GetExamplesData(c echo.Context) error {
	requestCtx := c.Request().Context()

	templateName, err := extractTemplateName(c)
	if err != nil {
		return c.JSON(http.StatusBadRequest, err)
	}

	logger := log.LoggerFromContext(requestCtx, handlerLogger).
		WithField(log.FieldTemplateName, templateName)

	logger.Debug("Examples Data request")

	examplesData, err := handlers.pdfRenderService.GetExamplesData(requestCtx, templateName)
	if err != nil {
		logger.WithError(err).Error("Render PDF example error")

		return handlers.buildError(err)
	}

	logger.Info("Example response success")

	return c.JSON(http.StatusOK, examplesData)
}

// GetSwagger20Spec return Swagger 2.0 specification.
func (handlers *Handlers) GetSwagger20Spec(c echo.Context) error {
	spec, err := handlers.specService.GetSpec()
	if err != nil {
		return handlers.buildError(err)
	}

	swaggerSpec := BuildSwaggerSpecification(spec)

	return c.JSON(http.StatusOK, swaggerSpec)
}

// GetSchema return schema of template if exists.
func (handlers *Handlers) GetSchema(c echo.Context) error {
	requestCtx := c.Request().Context()

	templateName, err := extractTemplateName(c)
	if err != nil {
		return c.JSON(http.StatusBadRequest, err)
	}

	logger := log.LoggerFromContext(requestCtx, handlerLogger).
		WithField(log.FieldTemplateName, templateName)

	schema, err := handlers.specService.GetTemplateSchema(templateName)
	if err != nil {
		logger.WithError(err).Error("get template schema error")

		return handlers.buildError(err)
	}

	if schema == nil {
		logger.Warn("get template schema without schema")

		return echo.NewHTTPError(http.StatusNotFound)
	}

	logger.Warn("get template schema success")

	return c.JSON(http.StatusOK, schema)
}

func extractTemplateName(c echo.Context) (string, error) {
	rawTemplateName := c.Param(templateNameParam)

	templateName, err := url.PathUnescape(rawTemplateName)
	if err != nil {
		return "", fmt.Errorf("unescape template name: %w", err)
	}

	return templateName, nil
}

// PoolStats of internal render pool.
type PoolStats struct {
	Active      int `json:"active"`
	Idle        int `json:"idle"`
	Destroyed   int `json:"destroyed"`
	Invalidated int `json:"invalidated"`
}

// Status response structure.
type Status struct {
	PoolStats      PoolStats         `json:"pool_stats"`
	RenderPoolType string            `json:"render_pool_type"`
	Metadata       map[string]string `json:"metadata"`
}

// Status of services.
func (handlers *Handlers) Status(c echo.Context) error {
	ctx := c.Request().Context()
	logger := log.LoggerFromContext(ctx, handlerLogger)

	stats, err := handlers.statsService.Stats(ctx)
	if err != nil {
		logger.WithError(err).Error("stats error")

		return echo.NewHTTPError(http.StatusInternalServerError, err.Error())
	}

	status := Status{
		PoolStats: PoolStats{
			Active:      stats.RenderInfo.Pool.Stats.Active,
			Idle:        stats.RenderInfo.Pool.Stats.Idle,
			Destroyed:   stats.RenderInfo.Pool.Stats.Destroyed,
			Invalidated: stats.RenderInfo.Pool.Stats.Invalidated,
		},
		RenderPoolType: stats.RenderInfo.Pool.Type,
		Metadata:       stats.RenderInfo.Metadata,
	}

	return c.JSON(http.StatusOK, &status)
}

// Metadata describe server metadata.
type Metadata struct {
	VersionInfo version.Info `json:"versionInfo"`
}

// Metadata collect and return server metadata.
func (*Handlers) Metadata(c echo.Context) error {
	metadata := Metadata{
		VersionInfo: version.GetInfo(),
	}

	return c.JSON(http.StatusOK, &metadata)
}

// StatusSchema return schema for Status type.
func (*Handlers) StatusSchema(c echo.Context) error {
	statusSchema := JSONSchemaObject{
		Type: JSONSchemaTypeObject,
		Properties: map[string]JSONSchema{
			"pool_stats": JSONSchemaObject{
				Type: JSONSchemaTypeObject,
				Properties: map[string]JSONSchema{
					"active": JSONSchemaObject{
						Type: JSONSchemaTypeNumber,
					},
					"idle": JSONSchemaObject{
						Type: JSONSchemaTypeNumber,
					},
					"destroyed": JSONSchemaObject{
						Type: JSONSchemaTypeNumber,
					},
					"invalidated": JSONSchemaObject{
						Type: JSONSchemaTypeNumber,
					},
				},
				Required: []string{
					"active",
					"idle",
					"destroyed",
					"invalidated",
				},
			},
			"render_pool_type": JSONSchemaObject{
				Type: JSONSchemaTypeString,
			},
			"metadata": JSONSchemaObject{
				Type: JSONSchemaTypeObject,
				Additional: JSONSchemaObject{
					Type: JSONSchemaTypeString,
				},
			},
		},
		Required: []string{
			"pool_stats",
			"render_pool_type",
			"metadata",
		},
	}

	return c.JSON(http.StatusOK, statusSchema)
}
