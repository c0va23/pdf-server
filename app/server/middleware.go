package server

import (
	"context"
	"math/rand"
	"net/http"
	"runtime/debug"
	"strconv"
	"time"

	echo "github.com/labstack/echo/v4"

	"gitlab.com/c0va23/pdf-server/app/log"
)

const (
	requestIDqueryParam = "request_id"
	requestIDBits       = 16
)

func generateRequestID() string {
	srcID := rand.Uint64()

	return strconv.FormatUint(srcID, requestIDBits)
}

func isDebugOnlyRoute(debugOnlyRoutes []string, route string) bool {
	for _, debugOnlyRoute := range debugOnlyRoutes {
		if route == debugOnlyRoute {
			return true
		}
	}

	return false
}

// RequestIDLogger inject logger to request context and extract or generate request ID.
func RequestIDLogger(
	logger log.Logger,
	debugOnlyRoutes []string,
) echo.MiddlewareFunc {
	return func(next echo.HandlerFunc) echo.HandlerFunc {
		return func(echoCtx echo.Context) error {
			startTime := time.Now()

			requestID := echoCtx.Request().URL.Query().Get(requestIDqueryParam)
			if requestID == "" {
				requestID = generateRequestID()
			}

			requestLogger := logger.
				WithField(log.LoggerField, "request").
				WithField(log.FieldRequestID, requestID)

			contextWithLogger := log.ContextWithLogger(echoCtx.Request().Context(), requestLogger)
			contextWithLogger = context.WithValue(contextWithLogger, log.ContextKeyRequestID, requestID)

			requestWithLogger := echoCtx.Request().WithContext(contextWithLogger)
			echoCtx.SetRequest(requestWithLogger)

			handlerErr := next(echoCtx)

			duration := time.Since(startTime)

			logResponse(requestLogger, debugOnlyRoutes, echoCtx, duration, handlerErr)

			return handlerErr
		}
	}
}

func logResponse(
	requestLogger log.Logger,
	debugOnlyRoutes []string,
	echoCtx echo.Context,
	duration time.Duration,
	handlerErr error,
) {
	responseLogger := requestLogger.
		WithField(log.FieldRequestRoute, echoCtx.Path()).
		WithField(log.FieldRequestIP, echoCtx.RealIP()).
		WithField(log.FieldRequestMethod, echoCtx.Request().Method).
		WithField(log.FieldRequestPath, echoCtx.Request().URL.Path).
		WithField(log.FieldRequestQuery, echoCtx.QueryString()).
		WithField(log.FieldDuration, log.Duration(duration)).
		WithField(log.FieldResponseCode, echoCtx.Response().Status).
		WithField(log.FieldResponseSize, echoCtx.Response().Size)

	if handlerErr != nil {
		responseLogger.WithError(handlerErr).Error("Response error")

		return
	}

	responseSuccess := "Response success"

	if isDebugOnlyRoute(debugOnlyRoutes, echoCtx.Path()) {
		responseLogger.Debug(responseSuccess)
	} else {
		responseLogger.Info(responseSuccess)
	}
}

// revive:disable:cognitive-complexity // TODO: rewrite function
func panicRecovery(logger log.Logger) echo.MiddlewareFunc {
	return func(next echo.HandlerFunc) echo.HandlerFunc {
		return func(echoCtx echo.Context) (err error) {
			defer func() {
				panicErr := recover()
				if panicErr == nil {
					return
				}

				stack := debug.Stack()

				logger := logger.WithField(log.FieldStack, string(stack))

				castedError, ok := panicErr.(error)
				if ok {
					logger.WithError(castedError).Error("Panic recovered")

					err = echo.NewHTTPError(http.StatusInternalServerError, castedError)

					return
				}

				logger.Errorf("Handler panic without error: %s", panicErr)

				err = echo.ErrInternalServerError
			}()

			err = next(echoCtx)

			return err
		}
	}
}
