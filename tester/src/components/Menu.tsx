import { Collapse, List, ListItemButton, ListItemText, ListSubheader } from '@mui/material'
import { ExpandLess, ExpandMore } from '@mui/icons-material'
import { useState } from 'react'
import { TemplateMetadata } from '../swaggerFetcher'
import { Link, useLocation } from 'react-router-dom'

interface MenuItemProperties {
    templateMetadata: TemplateMetadata,
    isOpen: boolean,
}

function MenuItem (props: MenuItemProperties) {
  const {
    templateMetadata,
    isOpen,
  } = props

  const [open, setOpen] = useState(isOpen)

  function toggleOpen () {
    setOpen(!open)
  }

  const currentPath = useLocation()

  return (
        <>
            <ListItemButton key={`${templateMetadata.templateName}-toggler`} onClick={toggleOpen}>
                <ListItemText>
                        {templateMetadata.templateName}
                </ListItemText>

                {open ? <ExpandLess /> : <ExpandMore />}
            </ListItemButton>

            <Collapse in={open} key={`${templateMetadata.templateName}-submenu`}>
                <List component="div" >
                    <ListItemButton
                        key={`${templateMetadata.templateName}-render`}
                        sx={{ pl: 4 }}
                        component={Link}
                        to={templateMetadata.renderPath}
                        selected={currentPath.pathname === templateMetadata.renderPath}
                    >
                        <ListItemText>
                            Render
                        </ListItemText>
                    </ListItemButton>
                    {templateMetadata.examples.map(example => (
                        <ListItemButton
                            key={`${templateMetadata.templateName}-examples-${example.exampleName}`}
                            sx={{ pl: 4 }}
                            component={Link}
                            to={example.examplePath}
                            selected={currentPath.pathname === example.examplePath}
                        >
                            <ListItemText>
                                Example: {example.exampleName}
                            </ListItemText>
                        </ListItemButton>
                    ))}
                </List>
            </Collapse>
        </>
  )
}

interface MenuProperties {
    templateMetadataList?: TemplateMetadata[]
}

export default function Menu (props: MenuProperties) {
  const compositionPrefix = 'composition:'

  const [searchParamsState, setSearchParamsState] = useState(undefined)

  const filterTemplate = (templateMetadata: TemplateMetadata) => {
    if (searchParamsState === undefined) {
      return true
    }

    return templateMetadata.templateName.toLowerCase().includes(searchParamsState.toLowerCase())
  }

  const filteredTemplateMetadataList = props
    .templateMetadataList
    ?.filter(filterTemplate)

  const compositionTemplateMetadataList =
    filteredTemplateMetadataList
      ?.filter(templateMetadata => templateMetadata.templateName.startsWith(compositionPrefix))
  const nonCompositionTemplateMetadataList =
    filteredTemplateMetadataList
      ?.filter(templateMetadata => !templateMetadata.templateName.startsWith(compositionPrefix))

  return (
    <>
      <input
        type="text"
        placeholder="Search"
        onChange={e => setSearchParamsState(e.target.value)}
      />
      {compositionTemplateMetadataList
        ? <MenuCategory
          templateMetadataList={compositionTemplateMetadataList}
          header="Compositions"
        />
        : null
      }
      {nonCompositionTemplateMetadataList
        ? <MenuCategory
          templateMetadataList={nonCompositionTemplateMetadataList}
          header="Templates"
        />
        : null
      }
      <StatusListItem />
    </>
  )
}

interface MenuCategoryProperties {
    header: string
    templateMetadataList: TemplateMetadata[]
}

export function MenuCategory (props: MenuCategoryProperties) {
  const { templateMetadataList, header } = props
  const currentPath = useLocation()

  return (
        <List
            subheader={
              <ListSubheader>{header}</ListSubheader>
            }
        >
            <>
                {templateMetadataList?.map(templateMetadata => (
                    <MenuItem
                        key={templateMetadata.templateName}
                        templateMetadata={templateMetadata}
                        isOpen={isCurrentTemplate(currentPath.pathname, templateMetadata)}
                    />
                ))}
            </>
        </List>
  )
}

function StatusListItem () {
  const currentPath = useLocation()

  return (<ListItemButton
      key='status-item'
      component={Link}
      to='/status'
      selected={currentPath.pathname === '/status'}
  >
      <ListItemText>
          Status
      </ListItemText>
  </ListItemButton>)
}

function isCurrentTemplate (currentPath: string, templateMetadata: TemplateMetadata): boolean {
  const examplesPaths = templateMetadata.examples.map(example => example.examplePath)

  return currentPath === templateMetadata.renderPath ||
      examplesPaths.includes(currentPath)
}
