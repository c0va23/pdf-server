import { Box, IconButton } from '@mui/material'
import { useContext, useEffect, useMemo, useState } from 'react'
import { useParams } from 'react-router-dom'
import { TemplateMetadata } from '../swaggerFetcher'
import { TitleContext } from './Title'
import { Refresh } from '@mui/icons-material'

interface ExampleProperties {
    templateMetadataList?: TemplateMetadata[]
}

const MILLISECOND = 1000

export function Example (props: ExampleProperties) {
  const { templateMetadataList } = props
  const { templateName, exampleName } = useParams()
  const templateMetadata = templateMetadataList?.find(templateMetadata => templateMetadata.templateName === templateName)
  const exampleMetadata = templateMetadata?.examples.find(exampleMetadata => exampleMetadata.exampleName === exampleName)

  const [reloadCounter, setReloadCounter] = useState(0)
  const dependencies = [templateName, exampleName, reloadCounter]
  const loadStarted = useMemo(() => new Date(), dependencies)
  const [loadFinished, setLoadFinished] = useState(undefined as Date)

  useEffect(() => setLoadFinished(undefined), dependencies)

  const loadedCallback = () => {
    setLoadFinished(new Date())
  }

  const loadDuration = loadFinished === undefined
    ? undefined
    : (loadFinished.getTime() - loadStarted.getTime()) / MILLISECOND

  const titleContext = useContext(TitleContext)

  useEffect(() => {
    titleContext.setTitle(`Template "${templateName}" / Example "${exampleName}"`)
  }, [templateName, exampleName])

  const reload = () => {
    setReloadCounter(reloadCounter + 1)
  }

  return (
        <Box>
            <Box
              style={{
                display: 'flex',
                padding: '0.5em',
              }}
            >
              <Box
                style={{
                  flexGrow: 1,
                }}
              >{loadDuration === undefined
                ? 'Loading...'
                : `Render in ${loadDuration}s`}
              </Box>

              <Box>
                <IconButton onClick={reload}>
                  <Refresh />

                </IconButton>
              </Box>
            </Box>

            {<iframe
                style={{
                  width: '100%',
                  height: '100vh',
                }}
                onLoad={loadedCallback}
                onAbort={loadedCallback}
                src={`${exampleMetadata?.examplePath}?reloadCounter=${reloadCounter}`}
            />}
      </Box>
  )
}
