import { Box, Checkbox, FormControl, FormControlLabel, FormGroup, Slider } from '@mui/material'
import { useContext, useEffect, useState } from 'react'
import { createPortal } from 'react-dom'
import { useParams } from 'react-router-dom'
import { TemplateMetadata } from '../swaggerFetcher'
import { InputPanel } from './InputPanel'
import { PreviewPanel } from './PreviewPanel'
import { TitleContext } from './Title'

export const cleanRenderData = undefined
export const cleanSchemaSpec = undefined

export interface RenderTemplateProperties {
    templateMetadataList?: TemplateMetadata[]
    settingsRef: React.RefObject<HTMLDivElement>
}

const maxSize = 100
const defaultSize = 40

interface ViewSettings {
  size: number
  showInputs: boolean
  showPreview: boolean
}

export function RenderTemplate (props: RenderTemplateProperties) {
  const {
    templateMetadataList,
    settingsRef,
  } = props
  const { templateName } = useParams()

  const [viewSettings, setViewSettings] = useState<ViewSettings>({
    size: defaultSize,
    showInputs: true,
    showPreview: true,
  })

  const [previewRenderData, setPreviewRenderData] = useState(null)

  const templateMetadata = templateMetadataList?.find(templateMetadata =>
    templateMetadata.templateName === templateName,
  )

  const titleContext = useContext(TitleContext)

  useEffect(() => {
    titleContext.setTitle(`Template "${templateName}"`)
  }, [templateName])

  if (templateMetadata === undefined) {
    return (<p>Template {templateName} not found.</p>)
  }

  return (
    <>
        <Box sx={{
          flexDirection: 'row',
          display: 'flex',
        }}>
            <Box
              flex={viewSettings.size}
              visibility={viewSettings.showInputs ? 'visible' : 'collapse'}
            >
              <InputPanel
                key={templateMetadata?.templateName}
                templateMetadata={templateMetadata}
                setPreviewRenderData={setPreviewRenderData}
              />
            </Box>

            <Box
              flex={(maxSize - viewSettings.size)}
              visibility={viewSettings.showPreview ? 'visible' : 'collapse'}
            >
              <PreviewPanel
                renderData={previewRenderData}
                templateMetadata={templateMetadata}
              />
            </Box>
        </Box>
        {settingsRef.current !== null && createPortal(
          <Settings
            setViewSettings={setViewSettings}
            viewSettings={viewSettings}
          />,
          settingsRef.current,
        )}
    </>
  )
}

export const Settings = ({
  setViewSettings,
  viewSettings,
}: {
  viewSettings: ViewSettings,
  setViewSettings: (_: ViewSettings) => void,
}) => {
  return <Box
    sx={{
      width: '20em',
    }}
  >
    <Slider
      min={0}
      max={maxSize}
      step={5}
      value={viewSettings.size}
      onChange={(_event, newValue) => setViewSettings({
        ...viewSettings,
        size: newValue as number,
      })}
    />
    <FormControl>
      <FormGroup>
        <FormControlLabel
          label="Show Inputs"
          control={<Checkbox
            defaultChecked={viewSettings.showInputs}
            onChange={(_event, checked) => setViewSettings({
              ...viewSettings,
              showInputs: checked,
            })}
          />}
        />
        <FormControlLabel
          label="Show Preview"
          control={<Checkbox
            defaultChecked={viewSettings.showPreview}
            onChange={(_event, checked) => setViewSettings({
              ...viewSettings,
              showPreview: checked,
            })}
          />}
        />
      </FormGroup>
    </FormControl>
  </Box>
}
