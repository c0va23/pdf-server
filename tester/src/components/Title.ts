import { createContext } from 'react'

interface TitleContextData {
  title?: string
  setTitle: (_: string) => void
}

export const TitleContext = createContext<TitleContextData>({
  title: undefined,
  // eslint-disable-next-line @typescript-eslint/no-empty-function
  setTitle: () => {},
})
