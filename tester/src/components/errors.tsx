import { Close } from '@mui/icons-material'
import { Alert, IconButton, Snackbar } from '@mui/material'
import React, { useState } from 'react'

interface ErrorsValue {
  showError: (_message: string) => void
}

export const ErrorContext = React.createContext<ErrorsValue>({
  showError: (message) => console.error(message),
})

interface ErrorProviderProps {
  children: React.ReactNode
}

export function ErrorProvider (props: ErrorProviderProps) {
  const [errorMessage, showError] = useState<string>()

  const providerValue = { showError }

  const close = () => showError(undefined)

  return (
    <ErrorContext.Provider value={providerValue}>
      <Snackbar
      color='error'
        open={errorMessage !== undefined}
        onClose={close}
      >
        <Alert severity='error' action={
          <IconButton onClick={close} size='small' color='inherit' >
              <Close fontSize='small' />
          </IconButton>
        }>
          {errorMessage}
        </Alert>
      </Snackbar>

      {props.children}
    </ErrorContext.Provider>
  )
}
