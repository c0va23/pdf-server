import { TabContext, TabPanel } from '@mui/lab'
import { Box, Button, Checkbox, FormControl, FormControlLabel, InputLabel, MenuItem, Select, Tab, Tabs } from '@mui/material'
import Form from '@rjsf/mui'
import validator from '@rjsf/validator-ajv8'
import { useEffect, useState } from 'react'
import { cleanSchemaSpec, cleanRenderData } from './RenderTemplate'
import { JsonInput } from './JsonInput'
import { TemplateMetadata } from '../swaggerFetcher'
import { RenderData } from './types'
import { Refresh } from '@mui/icons-material'

const defaultExample = ''

interface InputPanelProperties {
  templateMetadata: TemplateMetadata
  setPreviewRenderData: (_renderData: RenderData) => void
}

interface ExamplesData {
  [key: string]: object
}

export function InputPanel ({
  templateMetadata, setPreviewRenderData,
}: InputPanelProperties): JSX.Element {
  const [schemaSpec, setSchemaSpec] = useState(cleanSchemaSpec)
  const [livePreview, setLivePreview] = useState(true)
  const [renderData, setRenderData] = useState(cleanRenderData)
  const [selectedExample, setSelectedExample] = useState(defaultExample)

  const reloadSchema = () => {
    if (templateMetadata?.schemaPath !== undefined) {
      fetch(templateMetadata.schemaPath)
        .then(response => response.json())
        .then(setSchemaSpec)
    } else {
      setSchemaSpec(cleanSchemaSpec)
    }
  }

  useEffect(() => {
    reloadSchema()

    // Clean render data
    setRenderData(cleanRenderData)
    setSelectedExample(defaultExample)
  }, [templateMetadata])

  const rawJsonTab = 'rawJson'
  const jsonFormTab = 'jsonForm'
  const jsonPreview = 'json'
  const curlTab = 'curlTab'
  const [inputTab, setInputTab] = useState(rawJsonTab)

  useEffect(() => {
    const newInputTag = schemaSpec !== cleanSchemaSpec
      ? jsonFormTab
      : rawJsonTab

    setInputTab(newInputTag)
  }, [schemaSpec])

  function updateRenderData (newRenderData) {
    setRenderData(newRenderData)

    if (livePreview) {
      setPreviewRenderData(newRenderData)
    }
  }

  const [examplesData, setExamplesData] = useState<ExamplesData>(undefined)

  const reloadExamples = () => {
    if (templateMetadata.examplesDataPath !== undefined) {
      fetch(templateMetadata.examplesDataPath)
        .then(response => response.json())
        .then(setExamplesData)
    } else {
      setExamplesData(null)
    }
  }

  useEffect(() => {
    reloadExamples()
  }, [templateMetadata.examplesDataPath])

  const selectExample = (exampleName?: string) => {
    setSelectedExample(exampleName)
    updateRenderData(examplesData[exampleName])
  }

  const exampleSelectorId = 'example-selector'

  const reload = () => {
    reloadSchema()
    reloadExamples()
  }

  const isCurlAllowed = templateMetadata?.renderPath !== undefined && renderData !== cleanRenderData

  return (
    <>
      <TabContext value={inputTab}>
        <Box sx={{
          display: 'flex',
        }}>
          <Tabs
            value={inputTab}
            onChange={(_event, newTab) => setInputTab(newTab)}
            variant="scrollable"
          >
            <Tab label="Raw JSON" value={rawJsonTab} />
            <Tab
              label="JSON Schema Form"
              value={jsonFormTab}
              disabled={schemaSpec === cleanSchemaSpec} />
            <Tab label="JSON preview" value={jsonPreview} />
            <Tab label="cURL" value={curlTab} disabled={!isCurlAllowed} />
          </Tabs>

          <span style={{
            flexGrow: 1,
          }} />

          <Box sx={{
            padding: '0.5em',
          }}>
            <Button
              variant='text'
              title='Reload Schema & Examples'
              size='small'
              onClick={reload}
              sx={{
                minWidth: '0',
                margin: '0.25em',
              }}
            >
              <Refresh />
            </Button>
          </Box>
        </Box>

        <TabPanel value={rawJsonTab}>
          <JsonInput
            renderData={renderData}
            updateRenderData={updateRenderData} />
        </TabPanel>

        <TabPanel value={jsonFormTab}>
          {schemaSpec && <Form
            schema={schemaSpec}
            formData={renderData}
            validator={validator}
            onChange={event => updateRenderData(event.formData)}
            onSubmit={() => setPreviewRenderData(renderData)}
          >
            <span />
          </Form>}
        </TabPanel>

        <TabPanel value={jsonPreview}>
          <pre>
            {renderData &&
              JSON.stringify(renderData, null, 2)}
          </pre>
        </TabPanel>

        {isCurlAllowed &&
          <TabPanel value={curlTab}>
            <Box component='span' sx={{
              whiteSpace: 'break-spaces',
              wordWrap: 'break-word',
            }}>{buildCurlCommand(templateMetadata.renderPath, renderData)}</Box>
          </TabPanel>}

      </TabContext>

      <Button
        variant="contained"
        disabled={livePreview}
        onClick={() => setPreviewRenderData(renderData)}
        sx={{
          margin: '1em',
        }}
      >
        Submit
      </Button>

      <FormControlLabel
        label="Live preview"
        sx={{
          margin: '1em',
        }}
        control={<Checkbox
          checked={livePreview}
          onChange={event => setLivePreview(event.target.checked)} />} />

      <ExampleSelector
        exampleSelectorId={exampleSelectorId}
        selectedExample={selectedExample}
        examplesData={examplesData}
        selectExample={selectExample}
      />
    </>
  )
}

function ExampleSelector ({
  exampleSelectorId,
  selectedExample,
  examplesData,
  selectExample,
}: {
  exampleSelectorId: string,
  selectedExample: string,
  examplesData: ExamplesData,
  selectExample: (_?: string) => void,
}) {
  return <FormControl
    variant="outlined"
    size="small"
    sx={{
      width: '8em',
      margin: '1em',
    }
  }>
    <InputLabel id={exampleSelectorId}>Example</InputLabel>
    <Select
      id={exampleSelectorId}
      label="Example"
      value={selectedExample}
      disabled={!examplesData || Object.keys(examplesData).length === 0}
      onChange={(event) => selectExample(event.target.value as string)}
    >
      {examplesData && Object.keys(examplesData).map(exampleName => (
          <MenuItem key={exampleName} value={exampleName}>
              {exampleName}
          </MenuItem>
      ))}
    </Select>
  </FormControl>
}

function buildCurlCommand (
  renderPath: string,
  jsonData: RenderData,
): string {
  const url = new URL(renderPath, document.baseURI)

  const jsonString = JSON.stringify(jsonData)

  return `curl -v -X POST  -H 'Content-Type: application/json' --data '${jsonString}' ${url} -o output.pdf`
}
