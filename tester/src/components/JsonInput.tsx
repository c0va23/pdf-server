import { TextField } from '@mui/material'
import { useEffect, useState } from 'react'
import { RenderData } from './types'
import { cleanRenderData } from './RenderTemplate'

export interface JsonInputProperties {
  renderData: RenderData
  updateRenderData: (_renderData: RenderData) => void
}

export function JsonInput ({
  renderData, updateRenderData,
}: JsonInputProperties) {
  const cleanJsonValue = undefined

  const [tempRenderData, setTempRenderData] = useState(cleanJsonValue)
  const [jsonError, setJsonError] = useState(null)

  useEffect(() => {
    const renderDataJson = renderData !== cleanRenderData
      ? JSON.stringify(renderData, null, 2)
      : cleanJsonValue

    setTempRenderData(renderDataJson)
  }, [renderData])

  function updateRenderDataFromJson (newJson) {
    setTempRenderData(newJson)

    try {
      const newRenderData = JSON.parse(newJson)
      updateRenderData(newRenderData)
      setJsonError(null)
    } catch (error) {
      setJsonError(error.toString())
    }
  }

  return (
    <TextField
      id='jsonInput'
      label='JSON'
      value={tempRenderData}
      onChange={(event) => updateRenderDataFromJson(event.target.value)}
      helperText={jsonError}
      error={jsonError !== null}
      multiline
      minRows={3}
      style={{
        overflow: 'scroll',
        maxHeight: '65vh',
      }}
      fullWidth />
  )
}
