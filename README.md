# PDF Server

PDF Server is a platform to create your own PDF printing service.

Only few steps to create your own PDF printing service:
1. Create template
2. Build docker image
3. Run your service


## Usage

See [examples](./examples/README.md)

[Documentation (under construction)](./docs/index.adoc)

## Features

### Template

Every template is a directory with files:
1. `template.<ext>` - HTML template. Required.
2. `schema.json` - JSON Schema for data validation. Optional.
3. `params.json` - JSON with template parameters. Optional.
4. `examples/<example_name>.json` - JSON with example data. Optional.


### Template engine

Available template engines in order of priority (if more than one is available, the first one is used):
1. [Mustache](https://mustache.github.io/mustache.5.html). Simple and secure template engine. `template.mustache` file.
2. [Handlebars](https://handlebarsjs.com/guide/). Powerful template engine with helpers. `template.handlebars` file.
3. [Go html/template](https://pkg.go.dev/html/template). If Handlebars is not enough, you can use Go template engine. `template.tmpl` file.
4. Static. If you not use render request data, you can use static HTML. Dynamic data can be added by JavaScript. `template.html` file.


### Data validation

Optional [JSON Schema](https://json-schema.org/) can be used to validate data before rendering template.

See https://github.com/santhosh-tekuri/jsonschema to examine supported JSON-Schema features.


### Template helpers

Additional template helpers provided for Handlebars go Golang template engine.

See [helpers](./docs/helpers.adoc)

### Assets

You can place `assets` in template directory.
Assets will be available in template by relative path `./assets/`.


### Params configuration

PDF render parameters can be configured by `params.json` file.

| Param | Description |
| ----- | ----------- |
| `pdf.landscape` | Set page orientation to landscape |
| `pdf.printBackground` | Print background graphics |
| `pdf.scale` | Scale of the webpage rendering |
| `pdf.paperWidth` | Paper width in inches |
| `pdf.paperHeight` | Paper height in inches |
| `pdf.marginTop` | Top margin in inches |
| `pdf.marginBottom` | Bottom margin in inches |
| `pdf.marginLeft` | Left margin in inches |
| `pdf.marginRight` | Right margin in inches |
| `pdf.preferCSSPageSize` | Give any CSS @page size declared in the page priority over what is declared in width and height or format options |
| `pdf.waitLifecycleEvent` | Wait for lifecycle event before rendering PDF. See [WaitLifecycleEvent](#Wait%20Lifecycle%20Event's) |

### Wait Lifecycle Event's

By default PDF Server waits for `load` event before rendering PDF.
On load event all sync resources are loaded and rendered.

If you need to wait for async resources, you can use `waitLifecycleEvent` param.

Supported events:
- `init`
- `load`
- `DOMContentLoaded`
- `firstPaint`
- `firstContentfulPaint`
- `firstMeaningfulPaintCandidate`
- `networkAlmostIdle`
- `firstMeaningfulPaint`
- `networkIdle`

Default value `load`.


### Page header and footer

Page header and footer can be configured by `header.<ext>` and `footer.<ext>` files.

Available template engines similar to [Template engine](#template-engine).

    If only of of header or footer exists, missing one will be replaced with default value (not empty template).

Header and footer templates not have access assets.


### Template partials

Handlebars and Go template engines support partials.

Partials can be placed in `partials` directory.



## Overview
```plantuml
@startUML sequence
actor Developer
actor OtherService
actor PDFServer
actor Browser

participant "templates/"

Developer -> "templates/": create template

Developer -> PDFServer: start
activate PDFServer

    PDFServer -> "templates/": read templates
    activate PDFServer
        "templates/" -> PDFServer: templates

        PDFServer -> PDFServer: validate templates
        PDFServer -> Developer: success start
    deactivate PDFServer

    loop
        OtherService -> PDFServer: POST /:template_name\n{"data": "value"}
        activate PDFServer
            PDFServer -> PDFServer: validate date

            PDFServer -> PDFServer: render HTML

            PDFServer -> Browser: HTML
            Browser -> PDFServer: PDF

            PDFServer -> OtherService: PDF
        deactivate PDFServer
    end
deactivate PDFServer
@endUML
```

## Development

### Dev mode

```bash
make -j=2 dev.run
```
