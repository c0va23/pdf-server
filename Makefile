GOARCH := $(shell go env GOARCH)
RUSTARCH := $(shell uname --machine)
GOOS := $(shell go env GOOS)

GOLANGCI_LINT_VERSION := 1.61.0
GOLANGCI_LINT_ARCHIVE_NAME := golangci-lint-${GOLANGCI_LINT_VERSION}-${GOOS}-${GOARCH}
GOLANGCI_LINT_URL := https://github.com/golangci/golangci-lint/releases/download/v${GOLANGCI_LINT_VERSION}/${GOLANGCI_LINT_ARCHIVE_NAME}.tar.gz

MOCKERY_VERSION := v2.45.1

WATCH_EXEC_VERSION := 2.2.1
WATCH_EXEC_DIR_NAME := watchexec-${WATCH_EXEC_VERSION}-${RUSTARCH}-unknown-${GOOS}-musl
WATCH_EXEC_ARCHIVE_NAME := ${WATCH_EXEC_DIR_NAME}.tar.xz
WATCH_EXEC_URL := https://github.com/watchexec/watchexec/releases/download/v${WATCH_EXEC_VERSION}/${WATCH_EXEC_ARCHIVE_NAME}

PACKAGE_NAME :=gitlab.com/c0va23/pdf-server

VERSION_VARIABLE := $(PACKAGE_NAME)/app/config/version.Version

VERSION ?= develop

export GOBIN := $(PWD)/bin
export PATH := $(GOBIN):$(PATH)
export CGO_ENABLED := 0

GENERATE_TARGETS := \
	cmd/pdf-server/wire_gen.go

CHECK_DEPENDENCIES :=

SRC_FILES := $(shell find -L app/ cmd/ -type f ! -name *_test.go ! -path *fixtures*)

GOLANGCI_LINT_CACHE_PATH=.tmp/golangci-lint-cache/
export GOLANGCI_LINT_CACHE=${PWD}/${GOLANGCI_LINT_CACHE_PATH}

.PHONY: default
default: lint test test-integration

export WIRE_VERSION := 0.6.0

bin/wire-${WIRE_VERSION}:
	mkdir -p bin/wire-${WIRE_VERSION}
	GOBIN=${PWD}/$@ go install github.com/google/wire/cmd/wire@v${WIRE_VERSION}

CHECK_DEPENDENCIES += bin/wire
bin/wire: bin/wire-${WIRE_VERSION}
	mkdir -p bin/
	ln -s -f ${PWD}/bin/wire-${WIRE_VERSION}/wire $@

bin/mockery-${MOCKERY_VERSION}:
	mkdir $@
	GOBIN=${PWD}/$@ go install github.com/vektra/mockery/v2@${MOCKERY_VERSION}

CHECK_DEPENDENCIES += bin/mockery
bin/mockery: bin/mockery-${MOCKERY_VERSION}
	mkdir -p bin/
	ln -s -f ${PWD}/bin/mockery-${MOCKERY_VERSION}/mockery bin/mockery

bin/${GOLANGCI_LINT_ARCHIVE_NAME}/:
	mkdir -p bin
	curl -L --silent ${GOLANGCI_LINT_URL} | tar --directory bin/ --gzip --extract --verbose

CHECK_DEPENDENCIES += bin/golangci-lint
bin/golangci-lint: bin/${GOLANGCI_LINT_ARCHIVE_NAME}/
	ln -f -s $(PWD)/bin/${GOLANGCI_LINT_ARCHIVE_NAME}/golangci-lint $@
	touch $@

bin/${WATCH_EXEC_ARCHIVE_NAME}:
	curl -o $@  --silent -L ${WATCH_EXEC_URL}

bin/${WATCH_EXEC_DIR_NAME}: bin/${WATCH_EXEC_ARCHIVE_NAME}
	tar --directory bin/ --extract --xz --file $<

	touch $@

bin/watchexec: bin/${WATCH_EXEC_DIR_NAME}
	ln -s -f $(PWD)/bin/${WATCH_EXEC_DIR_NAME}/watchexec $@
	touch $@

source_files := $(shell find -L app/ -type f ! -name *_test.go ! -path *fixtures*)


cmd/pdf-server/wire_gen.go: bin/wire cmd/pdf-server/wire.go cmd/pdf-server/wire_bindings.go ${source_files}
	wire cmd/pdf-server/wire.go cmd/pdf-server/wire_bindings.go

bin/pdf-server: .gen $(SRC_FILES)
	mkdir -p bin/
	go build \
		-o $@ \
		-ldflags "-X $(VERSION_VARIABLE)=$(VERSION)" \
		./cmd/pdf-server

mocks: ${source_files}  bin/mockery
	./bin/mockery \
		--dir app \
		--output ./$@ \
		--all \
		--recursive \
		--with-expecter \
		--keeptree
	touch $@

.gen: ${GENERATE_TARGETS}
	mkdir -p $@

	touch $@

.PHONY: cache-dependencies
cache-dependencies: go.mod go.sum ${CHECK_DEPENDENCIES}
	go mod download

.PHONY: build
build: bin/pdf-server

.PHONy: dry-build
dry-build: bin/pdf-server
	rm bin/pdf-server

.PHONY: test
test: .gen mocks
	go test \
		-timeout=30s \
		-cover \
		./app/...

${GOLANGCI_LINT_CACHE_PATH}:
	mkdir -p $@

.PHONY: lint
lint: bin/golangci-lint .gen mocks ${GOLANGCI_LINT_CACHE_PATH}
	golangci-lint run ./...

.PHONY: test-integration
test-integration: bin/pdf-server
	./bin/pdf-server -t examples/templates/ --compositions-dir examples/compositions --templates-recursive validate

test-integration-fast: bin/pdf-server
	./bin/pdf-server \
		-t examples/templates/ \
		--compositions-dir examples/compositions \
		--templates-recursive \
		validate \
		composition:recursive/example-0 \
		examples_template/alice

.PHONY: clean
clean:
	git clean -f -d -X -- **

.PHONY: dev.server.run
dev.server.run: .gen $(SRC_FILES) bin/watchexec
	./bin/watchexec \
		--watch ./app \
		--watch ./cmd \
		--exts go \
		--shell=none \
		--stop-signal=SIGTERM \
		--restart \
		-- \
		go run \
			-ldflags=-X=${VERSION_VARIABLE}=${VERSION} \
			./cmd/pdf-server \
			--templates-dir ./examples/templates \
			--compositions-dir ./examples/compositions \
			--templates-reload-mode=always \
			--templates-recursive \
			server \
			--server-reuse-addr \
			--static-dir-enabled

.PHONY: dev.tester.watch
dev.tester.watch:
	cd tester && npm run watch

.PHONY: dev.run
dev.run: dev.server.run dev.tester.watch
